<master>
  <property name="doc(title)">@page_title;literal@</property>
  <property name="context">@context;literal@</property>
  <property name="main_navbar_label">admin</property>
  <property name="focus">@page_focus;literal@</property>
  <property name="admin_navbar_label">admin_categories</property>
  <property name="left_navbar">#cognovis-core.lt_left_navbar_htmlliter#</property>

<table width="100%">
<tr valign="top">
<td>
	<h1>@page_title@</h1>
	<%= [lang::message::lookup "" intranet-core.Categories_List_Help "
		<p>
		#cognovis-core.lt_This_page_allows_you_#
		</p>
		
	  <span style=\"color:red;font-weight:bold\">#cognovis-core.lt_Please_keep_the_follo#</span>
	  <ul>
	  <li>#cognovis-core.Never# <strong>#cognovis-core.DELETE#</strong> #cognovis-core.lt_categories_unless_you# <strong>#cognovis-core.DISBALE#</strong> #cognovis-core.lt_them_if_they_are_not_#</li>
	  <li>#cognovis-core.lt_Some_twenty_or_so_cor# <strong>#cognovis-core.DISABLING#</strong> #cognovis-core.lt_them_might_break_part#</li>
	  <li>#cognovis-core.lt_Never_change_CATEGORY#</li>
	  <li>#cognovis-core.lt_In_some_rare_cases_ch# <a href='/acs-admin/cache/'>#cognovis-core.flushing#</a> #cognovis-core.lt_the_cache_manually_or#</li>
	  </ul> 

	<ul>
	<li><a href='http://www.project-open.com/en/page-intranet-admin-categories-index'>#cognovis-core.Help_about_this_page#</a>
	<li><a href='http://www.project-open.com/en/list-categories'>#cognovis-core.lt_Help_about_the_meanin#</a>
"]%>
<if "All" ne @select_category_type@>
	<li><a href='@category_help_url;noquote@'>#cognovis-core.lt_Help_about_select_cat#</a>
</if>
	</ul>
<br><br>


</td>
</tr>
</table>


<if @show_add_new_category_p@>
	@category_list_html;noquote@
</if>
<else>
	<listtemplate name="categories"></listtemplate>
</else>


<script type="text/javascript">
	$(document).ready(function() { 
		$("#myTable").tablesorter(); 
	}); 
</script>

