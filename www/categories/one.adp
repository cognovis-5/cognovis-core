<master>
  <property name="doc(title)">@page_title;literal@</property>
<property name="admin_navbar_label">admin_categories</property>

<if "t" eq @constant_p@>
<p><font color=red>
<b>
#cognovis-core.lt_This_category_is_a_co#<br>
#cognovis-core.lt_Modifying_the_categor#<br>
#cognovis-core.However_it_is_OK_to#
<ul>
<li>#cognovis-core.lt_Change_the_Enabled_st#
<li>#cognovis-core.lt_Modify_the_Category_T#
<li>#cognovis-core.lt_Change_the_sort_order#
</ul>
</font></p>
<br>
</if>


<form @form_action_html;noquote@ method=POST>
<table border="0" cellpadding="0" cellspacing="1">
  <tr>
    <td class=rowtitle colspan="2" align="center">#intranet-core.Category#</td>
  </tr>
  <tr>
    <td>#intranet-core.Category_Type#</td>
    <td>@category_type_select;noquote@</td>
  </tr>
  <tr class=roweven>
    <td>#intranet-core.Category_Nr#</td>
    <td><input size="10" name="category_id" value="@category_id@"></td>
  </tr>
  <tr class=rowodd>
    <td>#intranet-core.Category_name#</td>
    <td><input size="40" name="category" value="@category@"></td>
  </tr>
  <tr class=rowodd>
    <td>#cognovis-core.Enabled#</td>
    <td>
	<input type="radio" name="enabled_p" value="t" @enabled_p_checked@>
	#cognovis-core.Enabled# 
	<input type="radio" name="enabled_p" value="f" @enabled_p_unchecked@>
	#cognovis-core.Not_Enabled# 
    </td>
  </tr>
  <tr class=rowodd>
    <td>#cognovis-core.Sort_Order#</td>
    <td><input size="5" name="sort_order" value="@sort_order@"></td>
  </tr>
  <tr class=roweven>
    <td>
      #intranet-core.Category_translation#<br>
      <%= [im_gif help "The English 'translation' should be identical with the category name"] %>
    </td>
    <td>
      @category_translation_component;noquote@
   </td>
  </tr>

  <tr class=rowodd>
    <td>#cognovis-core.Int1#</td>
    <td><input size="20" name="aux_int1" value="@aux_int1@"></td>
  </tr>
  <tr class=roweven>
    <td>#cognovis-core.Int2#</td>
    <td><input size="20" name="aux_int2" value="@aux_int2@"></td>
  </tr>
  <tr class=rowodd>
    <td>#cognovis-core.Num1#</td>
    <td><input size="20" name="aux_num1" value="@aux_num1@"></td>
  </tr>
  <tr class=roweven>
    <td>#cognovis-core.Num2#</td>
    <td><input size="20" name="aux_num2" value="@aux_num2@"></td>
  </tr>

  <tr class=rowodd>
    <td>#cognovis-core.String1_en_US#<br /><br />
@string1_translation_links;noquote@
</td>
    <td>
      <textarea name=aux_string1 rows="5" cols="50" wrap="<%=[im_html_textarea_wrap]%>">@aux_string1@</textarea>
    </td>
  </tr>
  <tr class=roweven>
    <td>#cognovis-core.String2_en_US#<br /><br />
@string2_translation_links;noquote@
    <td>
      <textarea name=aux_string2 rows="5" cols="50" wrap="<%=[im_html_textarea_wrap]%>">@aux_string2@</textarea>
    </td>
  </tr>
  <tr class=rowodd>
    <td>#cognovis-core.Html1#<br /><br />
@string1_translation_links;noquote@
</td>
    <td>
      <textarea name=aux_html1 rows=5 cols=50 wrap="<%=[im_html_textarea_wrap]%>">@aux_html1@</textarea>
    </td>
  </tr>
  <tr class=roweven>
    <td>#cognovis-core.Html2#<br /><br />
@string2_translation_links;noquote@
    <td>
      <textarea name=aux_html2 rows="5" cols="50" wrap="<%=[im_html_textarea_wrap]%>">@aux_html2@</textarea>
    </td>
  </tr>

  <tr class=rowodd>
    <td>#intranet-core.Visible_tcl#</td>
    <td>
      <textarea name=visible_tcl rows="5" cols="50" wrap="<%=[im_html_textarea_wrap]%>">@visible_tcl@</textarea>
    </td>
  </tr>

  <tr class=rowodd>
    <td>#intranet-core.Category_description#</td>
    <td>
      <textarea name=category_description rows=5 cols=50 wrap="<%=[im_html_textarea_wrap]%>">@descr@</textarea>
    </td>
  </tr>
<% if {"" != $hierarchy_component} { %>
  <tr class=roweven>
    <td>#intranet-core.Parent_Categories#</td>
    <td>
      <select name=parents size="20" multiple>
      @hierarchy_component;noquote@
      </select>
    </td>
  </tr>
<% } %>
<tr class=roweven>
  <td colspan="2">
	<input type="submit" name="submit" value="#intranet-core.Submit#" $input_form_html>
  </td>
</tr>

</table>

</form>
@delete_action_html;noquote@


