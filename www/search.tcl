# packages/intranet-search-pg/www/search.tcl
#
# Copyright (C) 1998-2004 various parties
# The code is based on ArsDigita ACS 3.4
#
# This program is free software. You can redistribute it
# and/or modify it under the terms of the GNU General
# Public License as published by the Free Software Foundation;
# either version 2 of the License, or (at your option)
# any later version. This program is distributed in the
# hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

ad_page_contract {
    @author Neophytos Demetriou <k2pts@cytanet.com.cy>
    @author Frank Bergmann <frank.bergmann@project-open.com>
    @creation-date May 20th, 2005
    @cvs-id $Id$

    This search page uses the "TSearch2" full text index (FTI)
    and the P/O permission system to locate suitable business
    objects for a search query.<p>

    The main problem of searching in P/O is it's relatively
    strict permission system with object specific permissions
    that can only be tested via a (relatively slow) TCL routine.
    For example: Project are readable for the "key account"
    managers of the project's customer.<p>

    So this search page contains several performance optimizations:
    <ul>
    <li>Rapid exclusion of non-allowed objects:<br>
	A search query can return millions of object_id's in
	the worst case. Testing each of these objects for permission
	would take minutes or even hours.
	However, we can (frequently!) discard a large number of
	these objects when they are located in projects (or 
	companies, offices, ...) that are outside of the permission
	scope of the current user. This is why the "im_search_objects"
	table explicitely carries the "business_object_id".

    <li>Explicit permissions for specific "profiles":<br>
	Explicit permissions are given for certain user groups,
	most notably "Registered Users". So documents in a project
	folder that are marked as publicly readable can be found
	even if the project may not be readable at all.
    </ul>

} {
    {q:trim ""}
    {t:trim ""}
    {offset:integer 0}
    {results_per_page:integer 0}
    {type:multiple "all"}
    {include_deleted_p 0}
	{max_results:integer 5}
} 

# -----------------------------------------------------------
# Default & Security
# -----------------------------------------------------------

set current_user_id [auth::require_login]
set page_title [lang::message::lookup "" intranet-search-pg.Search_Results_for_query "Search Results for '%q%'"]
set package_id [ad_conn package_id]
set package_url [ad_conn package_url]
set package_url_with_extras $package_url
set context [list]
set context_base_url $package_url

# Determine the user's group memberships
set user_is_employee_p [im_user_is_employee_p $current_user_id]
set user_is_customer_p [im_user_is_customer_p $current_user_id]
set user_is_wheel_p [im_profile::member_p -profile_id [im_wheel_group_id] -user_id $current_user_id]
set user_is_admin_p [im_is_user_site_wide_or_intranet_admin $current_user_id]
set user_is_admin_p [expr {$user_is_admin_p || $user_is_wheel_p}]

set query $q

# -----------------------------------------------------------
# Prepare the list of searchable object types
# -----------------------------------------------------------

set object_types [list "im_project" "im_company" "person" "im_invoice"]

set sql "
	select
		aot.object_type,
		aot.pretty_name as object_type_pretty_name
	from
		acs_object_types aot
	where
		object_type in ([ns_dbquotelist $object_types])
"

db_foreach object_type $sql {
    set checked ""
    if {$type eq "all" || [lsearch $type $object_type] >= 0} {
		set checked " checked"
    }
    regsub -all { } $object_type_pretty_name {_} object_type_pretty_name_sub

    if {"im_invoice" eq $object_type} {
		set object_type_pretty_name  [lang::message::lookup "" intranet-cost.FinancialDocument "Financial Document"]
    } else {
		set object_type_pretty_name [lang::message::lookup "" intranet-core.$object_type_pretty_name_sub $object_type_pretty_name]
    }

	set type_pretty_name($object_type) $object_type_pretty_name

    append objects_html "
		<tr>
		<td>
			<input type=checkbox name=type value='$object_type' id='type,$object_type' $checked>
		</td>
		<td>
			$object_type_pretty_name
		</td>
		</tr>
"
}

if {$query eq ""} {
	set result_html "No Results"
	set count 0
	set num_results 0
} else {
		if {"all" eq $type} {
		set search_results [cog::search::search -query $query -max_results $max_results]
		} else {
		set search_results [cog::search::search -query $query -object_types $type -max_results $max_results]
		}

		set count [dict size $search_results]
		set num_results $count

		# Iterate over the search results
		set prev_object_type ""
		dict for {object_id search_data} $search_results {
		# Extract the object type from the object data
		set object_type [dict get $search_data object_type]
		set object_name [dict get $search_data object_name]

		set object_url [im_biz_object_url $object_id]
		set headline [dict get $search_data search_text]
		set object_data [dict get $search_data object_data]

		if {$object_type ne $prev_object_type} {
			# Neue Überschrift für den neuen Objekttyp
			append result_html "<th>$type_pretty_name($object_type)</th>"
			set prev_object_type $object_type
		}

		if {"" != $object_url} {
			set object_html "<a href=\"$object_url\">$object_name</a>\n"
		} else {
			set object_html "$object_name\n"
		}

		switch $object_type {
			im_invoice {
				set cost_type_id [dict get $object_data cost_type_id]
				set l10n_key "intranet-cost.[im_cost_type_short_name $cost_type_id]"
				append result_html "
					<tr>
						<td>
						<font>[lang::message::lookup "" intranet-cost.FinancialDocument "Financial Document"]:
				([lang::message::lookup "" $l10n_key "[im_cost_type_short_name $cost_type_id]"]): $object_html</font><br>
						$headline
						<br>&nbsp;
						</td>
					</tr>
					"
			}
			default {
				append result_html "
					<tr>
						<td>
							$object_html<br>
							$headline
							<br>&nbsp;
						</td>
					</tr>
				"
			}
		}
	}
}
