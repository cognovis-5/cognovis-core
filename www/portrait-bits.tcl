ad_page_contract {
    spits out correctly MIME-typed bits for a user's portrait

    @author philg@mit.edu
    @creation-date 26 Sept 1999
    @cvs-id $Id: portrait-bits.tcl,v 1.10.2.7 2017/03/27 10:49:05 gustafn Exp $
} {
    {user_id ""}
    {item_id ""}
}


if {$user_id ne ""} {
    set item_id [cog::user::get_portrait_id_not_cached -user_id $user_id]
}

if { $item_id != 0} {
    cr_write_content -item_id $item_id
} else {
    ad_log warning "cannot show portrait with item_id $item_id for user $user_id item_id $item_id"
    ns_returnnotfound
}
