ad_page_contract {

    Delete a column from a report

    @author malte.sussdorff@cognovis.de
} {
    column_id
    report_id
}

db_dml delete_column "delete from im_report_columns where column_id = :column_id and report_id = :report_id"
ad_returnredirect [export_vars -base "new" -url {report_id}]
