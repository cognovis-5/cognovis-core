ad_page_contract {

    Generate columns based of SQL from a report

    @author malte.sussdorff@cognovis.de
} {
    { return_url "" }
    { report_id "" }
} 

if {$report_id eq ""} {
    ad_returnredirect "index"
}

set report_exists_p [db_0or1row report_info "select report_id, report_name, report_description, report_sql from im_reports where report_id = :report_id"]
if {!$report_exists_p} {
    ad_returnredirect "index"
}

db_with_handle db {
    if {[catch {set selection [db_exec select $db report_$report_id $report_sql]} err_msg]} {
        cog_log Error "Misconfigured report $report_id: $err_msg"
        ad_returnredirect "index"
    }

    # Append the columns based of the selection key
    set variable_names [ns_set keys $selection]
    set sort_order 0
    foreach variable_name $variable_names {

        # Check if the column already exists for this report
        set column_exists_p [db_string exists "select 1 from im_report_columns where variable_name = :variable_name and report_id = :report_id limit 1" -default 0]
        if {$column_exists_p} {
            set sort_order [expr $sort_order +10 ]
            continue
        }

        set column_type_id [cog::report::column::type_from_variable -variable_name $variable_name]

        set column_id [cog::report::column::new -report_id $report_id -column_name [cog_rest::helper::to_CamelCase -string $variable_name] \
            -column_type_id $column_type_id -variable_name $variable_name -sort_order $sort_order]
        set sort_order [expr $sort_order +10 ]            
    }
    ad_returnredirect [export_vars -base "new" -url {report_id return_url}]
}
