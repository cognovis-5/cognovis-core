
ad_page_contract {
    New / Edit column
    @author malte.sussdorff@cognovis.default
} {
    column_id:integer,optional
    report_id:integer
    {form_mode "display"}
}

if {![info exists column_id]} { set form_mode "edit" }
set page_title [lang::message::lookup "" cognovis-core.Edit_Report_column "Edit Report Column"]
set context [im_context_bar $page_title]

set form_id "form"
ad_form \
    -name $form_id \
    -mode $form_mode \
    -export {next_url user_id return_url} \
    -form {
	    column_id:key
        {report_id:integer(hidden) 
            {value $report_id}
        }
      	{column_name:text(text) {label "[lang::message::lookup {} cognovis-core.Column_Name {Column Name}]"} {html {size 60}}}
        {column_status_id:integer(im_category_tree)
            {label "[_ intranet-core.Status]"}
            {custom {category_type "Intranet Report Column Status" translate_p 1}}
        }
        {column_type_id:integer(im_category_tree)
            {label "[_ intranet-core.Type]"}
            {custom {category_type "Intranet Report Column Type" translate_p 1}}
        }
        {sort_order:integer
            {label "[_ intranet-core.Sort_Order]"}
            {html {size 5}}
        }
        {variable_name:text(text) {label "[lang::message::lookup {} cognovis-core.Variable_Name {Variable Name}]"} {html {size 60}}}
        {position:text(text),optional
            {label "[lang::message::lookup {} cognovis-core.Column_Position {Column Position}]"}
            {html {size 5}}
        }
    }

ad_form -extend -name $form_id \
-select_query {

	select * from im_report_columns where column_id = :column_id

} -new_data {
    set column_id [cog::report::column::new -report_id $report_id -column_name $column_name \
        -column_type_id $column_type_id -variable_name $variable_name -sort_order $sort_order]
} -edit_data {
    db_dml update_column "update im_report_columns set column_name = :column_name, column_type_id = :column_type_id, variable_name = :variable_name,
        column_status_id = :column_status_id, sort_order = :sort_order, position = :position where column_id = :column_id"
} -after_submit {
    ad_returnredirect [export_vars -base "new" -url {report_id return_url}]
}