<master src="/packages/intranet-core/www/master">
<property name="doc(title)">@page_title;literal@</property>
<property name="context">@context;literal@</property>

<script type="text/javascript" <if @::__csp_nonce@ not nil>nonce="@::__csp_nonce;literal@"</if>>
window.addEventListener('load', function() { 
     document.getElementById('list_check_all_search').addEventListener('click', function() { acs_ListCheckAll('type', this.checked) });
});
</script>

<br><form method=GET action=search>
  <center>
    <table>
      <tr>
        <td>
          <!--    <%= [im_logo] %>  -->
        </td>
        <td>
         <table>
         <tr>
         <td>
          <input type="text" name="q" size="31" maxlength="256" value="@query@">
          <input type="submit" value="#intranet-search-pg.Search#" name="t">
          </td>
          </tr>
                      <tr>
              <td colspan="2">
                <label for="max_results">Number of objects per type:</label>
                <select id="max_results" name="max_results">
                  <option value="1">1</option>
                  <option value="5" selected>5</option>
                  <option value="10">10</option>
                  <option value="15">15</option>
                  <option value="30">30</option>
                </select>
              </td>
            </tr>
            </table>
        </td>
        <td>
          <table>
            <tr>
              <td colspan="2">Search for specific object types:</td>
            </tr>
            <tr valign="top">
              <td align="center">
                <table cellspacing="0" cellpadding="0">
                  <tr class=rowtitle>
                    <td class=rowtitle><input id=list_check_all_search type="checkbox" name="_dummy" checked></td>
                    <td class=rowtitle><%= [lang::message::lookup "" intranet-search-pg.Search_Object_Type "Search Object Type"] %>
                  </tr>
                  @objects_html;noquote@
                </table>
              </td>
              <td>
                <table cellspacing="0" cellpadding="0">
                  <tr class=rowtitle>
                    <td class=rowtitle><input type="checkbox" name='include_deleted_p' value="1"></td>
                    <td class=rowtitle>#intranet-search-pg.Include_Deleted#</td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </center>
</form>


<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
  <td bgcolor="#fff" height=1 >
  </td>
</tr>
</table>

<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#e5ecf9">
<tr>
  <td bgcolor="#e5ecf9" nowrap>
  <font size=+1>&nbsp;<b>
    Intranet Results
  </b></font>&nbsp;
</td>
<td bgcolor="#e5ecf9" align="right" nowrap>
  <font size=-1>
    #intranet-search-pg.Results#
    <b>@num_results;noquote@</b>.
  </font>
</td>
</tr>
</table>

<br>

<if @query eq "">
  <font size="+1">#intranet-search-pg.lt_No_pages_were_found_c#: &quot;<b>Query was empty</b>&quot;</font>.
</if>
<if @count@ eq 0 and @query@ ne "">
  <font size="+1">#intranet-search-pg.lt_No_pages_were_found_c#: &quot;<b>@query@</b>&quot;</font>.
  <br><br>#intranet-search-pg.Suggestions#
  <ul>
    <li>#intranet-search-pg.lt_Make_sure_all_words_a#
    <li>#intranet-search-pg.lt_Try_different_keyword#
    <li>#intranet-search-pg.lt_Try_more_general_keyw#
  </ul>
</if>

<else>

  <table>
  @result_html;noquote@
  </table>

  <br clear=all>

</else>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
  <td bgcolor="#ffffff">
  </td>
</tr>
</table>

<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#e5ecf9">
<tr>
  <td bgcolor="#e5ecf9" colspan="99">&nbsp;</td>
</tr>
<tr>
  <td bgcolor="#e5ecf9" align="center">
  <form method=GET action=search>
  <table>
    <tr>
      <td>
        <input type="text" name="q" size="31" maxlength="256" value="@query@">
        <input type="submit" value="#intranet-search-pg.Search#" name="t">
      </td>
    </tr>
  </table>
  </form>
  </td>
</tr>
<tr>
  <td bgcolor="#e5ecf9" colspan="99">&nbsp;</td>
</tr>
</table>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
  <td bgcolor="#ffffff">
  </td>
</tr>
</table>
