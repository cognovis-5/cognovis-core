set read_p 1
if { ("" == $view_name || "standard" == $view_name) && $disable_view_standard_p } { set read_p 0 }
if { "finance" == $view_name && $disable_view_finance_p } { set read_p 0 }

# pre-filtering 
# permissions - beauty of code follows transparency and readability
  
set view_docs_1_p 0
set view_docs_2_p 0
set view_docs_3_p 0
set can_read_summary_p 0
    
set limit_to_freelancers ""
set limit_to_inco_customers ""
set limit_to_customers ""
    
# user is freelancer and can see purchase orders
if { [im_profile::member_p -profile_id [im_freelance_group_id] -user_id $user_id] && [im_permission $user_id fi_read_pos]  } {
	set view_docs_1_p 1
}
    
# user is interco client with privileges "Fi read quotes" AND  "Fi read invoices AND Fi read interco quotes" AND  "Fi read interco invoices" 
if { [im_profile::member_p -profile_id [im_inco_customer_group_id] -user_id $user_id] && ( [im_permission $user_id fi_read_invoices] || [im_permission $user_id fi_read_quotes] || [im_permission $user_id fi_read_interco_quotes] || [im_permission $user_id fi_read_interco_invoices] ) } {
	set view_docs_2_p 1
}
   
# user is client with privileges "Fi read quotes" AND  "Fi read invoices" 
if { [im_profile::member_p -profile_id [im_customer_group_id] -user_id $user_id] && ( [im_permission $user_id fi_read_invoices] || [im_permission $user_id fi_read_quotes] ) } {
	set view_docs_2_p 1
}
   
# user is employee and has has privilege  "view cost" 
if { ![im_user_is_customer_p $user_id] && ![im_user_is_freelance_p $user_id] && [im_permission $user_id view_costs] } {
set view_docs_3_p 1
	set can_read_summary_p 1
}
   
if { !( $view_docs_1_p || $view_docs_2_p || $view_docs_3_p ) && ![im_user_is_admin_p $user_id]} {
	set read_p 0
} 
    
    
set bgcolor(0) " class=roweven "
set bgcolor(1) " class=rowodd "
set colspan 9
set date_format "YYYY-MM-DD"
set num_format "9999999999.99"


#Round to two digits by default
set rounding_factor 100.0
   
# Locale for rendering 
set locale "en"
   
# Where to link when clicking on an object link? "edit" or "view"?
set view_mode "view"
    
# Default Currency
set default_currency [ad_parameter -package_id [im_package_cost_id] "DefaultCurrency" "" "EUR"]
   
# project_id may get overwritten by SQL query
im_security_alert_check_integer -location "im_costs_project_finance_component: project_id" -value $project_id
set org_project_id $project_id
    
# Get a hash array of subtotals per cost_type
array set subtotals [cog::project::update_cost_cache -project_id $project_id]
   
# ----------------- Compose Main SQL Query --------------------------------
   
set project_cost_ids_sql "
				select distinct cost_id
				from im_costs
				where project_id in (
			select	children.project_id
			from	im_projects parent,
				im_projects children
			where	children.tree_sortkey 
					between parent.tree_sortkey 
					and tree_right(parent.tree_sortkey)
				and parent.project_id = :project_id
		)
		UNION
		select distinct object_id_two as cost_id
		from acs_rels
		where object_id_one in (
			select	children.project_id
			from	im_projects parent,
				im_projects children
			where	children.tree_sortkey 
					between parent.tree_sortkey 
					and tree_right(parent.tree_sortkey)
				and parent.project_id = :project_id
		)
"
    
    
# If user = freelancer limit docs to PO
if { [im_profile::member_p -profile_id [im_freelance_group_id] -user_id $user_id] } {
	set limit_to_freelancers "and ci.cost_type_id = [im_cost_type_po] "
}

# If user = inco customer limit docs to Quotes & Invoices & InterCo Quotes & InterCo Invoices
if { [im_profile::member_p -profile_id [im_inco_customer_group_id] -user_id $user_id] } {
	set limit_to_inco_customers "and ci.cost_type_id in ( [im_cost_type_quote],[im_cost_type_invoice],[im_cost_type_interco_invoice],[im_cost_type_interco_quote] ) "
}

# If user = customer limit docs to Quotes & Invoices
if { [im_profile::member_p -profile_id [im_customer_group_id] -user_id $user_id] } {
	set limit_to_customers "and ci.cost_type_id in ( [im_cost_type_quote],[im_cost_type_invoice] ) "
}
    
set cost_type_excludes [list [im_cost_type_employee] [im_cost_type_repeating] [im_cost_type_expense_item]]
    
# Exclude intranet-planning planning types
lappend cost_type_excludes 73100
lappend cost_type_excludes 73102
      
if {$no_timesheet_p} {
	lappend cost_type_excludes [im_cost_type_timesheet]
}
   
set costs_sql "
 select
ci.*,
to_char(ci.paid_amount, :num_format) as payment_amount,
ci.paid_currency as payment_currency,
to_char(ci.amount, :num_format) as amount,
to_char(ci.amount * im_exchange_rate(ci.effective_date::date, ci.currency, :default_currency), :num_format) as amount_converted,
p.project_nr,
p.project_name,
cust.company_name as customer_name,
prov.company_name as provider_name,
url.url,
im_category_from_id(ci.cost_status_id) as cost_status,
im_category_from_id(ci.cost_type_id) as cost_type,
im_cost_center_code_from_id(ci.cost_center_id) as cost_center_code,
to_date(to_char(ci.effective_date,:date_format),:date_format) + ci.payment_days as calculated_due_date
 from
im_costs ci
	LEFT OUTER JOIN im_projects p ON (ci.project_id = p.project_id)
	LEFT OUTER JOIN im_companies cust on (ci.customer_id = cust.company_id)
	LEFT OUTER JOIN im_companies prov on (ci.provider_id = prov.company_id),
acs_objects o,
(select * from im_biz_object_urls where url_type=:view_mode) url
 where
ci.cost_id = o.object_id
and o.object_type = url.object_type
and ci.cost_id in (
	$project_cost_ids_sql
)
and ci.cost_type_id not in ([ns_dbquotelist $cost_type_excludes])
$limit_to_freelancers
$limit_to_inco_customers
$limit_to_customers
 order by
ci.cost_type_id,
ci.effective_date desc
 "
    
    
 set cost_html "
 <h1>[_ intranet-cost.Financial_Documents]</h1>
 <table border=0 class='table_list_page'>
   <!-- <tr class='no_hover'>
<td colspan=$colspan class=rowtitle>
  [_ intranet-cost.Financial_Documents]
</td>
   </tr>-->
   <thead>
   <tr>
<td>[_ intranet-cost.Document]</td>
<td>[lang::message::lookup "" intranet-cost.CostCenter_short "CC"]</td>
<td>[_ intranet-cost.Company]</td>
<td>[_ intranet-cost.Due]</td>
<td align='right'>[_ intranet-cost.Amount]</td>
<td align='right'>[lang::message::lookup "" intranet-cost.Org_Amount "Org"]</td>
<td align='right'>[_ intranet-cost.Paid]</td>
<td align='right'>[_ intranet-cost.Status]</td>
<td align='right'>[_ intranet-cost.Mail]</td>
   </tr>
   </thead>
   <tbody>
   <tr class='rowwhite'><td colspan='$colspan'>&nbsp;</td></tr>
       "
    
set ctr 1
set atleast_one_unreadable_p 0
set old_atleast_one_unreadable_p 0
set payment_amount ""
set payment_currency ""
    
set old_project_nr ""
set old_cost_type_id 0

set mail_installed_p [apm_package_installed_p "intranet-mail"]

db_foreach recent_costs $costs_sql {
    
	# Write the subtotal line of the last cost_type_id section
	if {$cost_type_id != $old_cost_type_id} {
		if {0 != $old_cost_type_id} {
  			if {!$atleast_one_unreadable_p} {
				append cost_html "
				<tr class=rowplain>
				  <td colspan=[expr $colspan-6]>&nbsp;</td>
				  <td align='right' colspan=2>
					<b><nobr>$subtotals($old_cost_type_id) $default_currency</nobr></b>
				  </td>
				  <td colspan='4'>&nbsp;</td>
				</tr>
				"
			} else {
				append cost_html "
				<tr class='rowwhite'>
				  <td colspan='$colspan'>&nbsp;</td>
				 SyntaxError</tr>\n"
			}
		}
    
		regsub -all " " $cost_type "_" cost_type_subs
		set cost_type [lang::message::lookup "" intranet-core.$cost_type_subs $cost_type]
		    
		append cost_html "
			<tr class='rowplain'>
			  <td colspan='$colspan'><span class='table_interim_title'>$cost_type</span></td>
			</tr>\n"
		    
		set old_cost_type_id $cost_type_id
		set old_atleast_one_unreadable_p $atleast_one_unreadable_p
		set atleast_one_unreadable_p 0
 	}
    
	# Avoid errors with strange cost_type_ids from planning etc
	if {![info exists subtotals($old_cost_type_id)]} { set subtotals($old_cost_type_id) 0 }
    
	# Check permissions - query is cached
	set cc_read_p [im_cost_center_read_p $cost_center_id $cost_type_id $user_id]
	if {!$cc_read_p} { 
		set atleast_one_unreadable_p 1 
		set can_read_summary_p 0
	}
    
	set company_name ""
	if {$cost_type_id == [im_cost_type_invoice] || $cost_type_id == [im_cost_type_quote] || $cost_type_id == [im_cost_type_delivery_note] || $cost_type_id == [im_cost_type_interco_invoice] || $cost_type_id == [im_cost_type_interco_quote]} {
		set company_name $customer_name
	} else {
		set company_name $provider_name
	}
    
	set cost_url "<A title=\"$cost_name\" href=\"$url$cost_id&return_url=[ns_urlencode $return_url]\">"
	set cost_url_end "</A>"
    
	set amount_unconverted "<nobr>([string trim $amount] $currency)</nobr>"
	if {[string equal $currency $default_currency]} { set amount_unconverted "" }
   
	set amount_paid "$payment_amount $default_currency"
	if {"" == $payment_amount} { set amount_paid "" }
   
	set default_currency_read_p $default_currency
	if {!$cc_read_p} {
		set cost_url ""
		set cost_url_end ""
		set amount_converted ""
		set amount_unconverted ""
		set amount_paid ""
		set default_currency_read_p ""
	}
   
   set mail_link ""
   if {$mail_installed_p} {
	   set mail_log_id [db_string mail "select log_id from acs_mail_log where context_id = :cost_id order by sent_date desc limit 1" -default ""]
	   if {$mail_log_id ne ""} {
	       set mail_link "<a href='[export_vars -base "/intranet-mail/one-message" -url {{log_id $mail_log_id}}]'>Mail</a>"
	   }
   }
	append cost_html "
<tr $bgcolor([expr $ctr % 2])>
  <td><nobr>$cost_url[string range $cost_name 0 30]</A></nobr></td>
  <td>$cost_center_code</td>
  <td>$company_name</td>
  <td><nobr>$calculated_due_date</nobr></td>
  <td align='right'><nobr>$amount_converted $default_currency_read_p</nobr></td>
  <td align='right'><nobr>$amount_unconverted</td>
  <td align='right'><nobr>$amount_paid</nobr></td>
  <td align='right'><nobr>$cost_status</nobr></td>
  <td align='right'><nobr>$mail_link</nobr></td>  
</tr>\n"
	incr ctr
	
}
    
# Write the subtotal line of the last cost_type_id section
if {$ctr > 1} {
	if {!$atleast_one_unreadable_p} {
		append cost_html "
			<tr class=rowplain>
			  <td colspan=[expr $colspan-6]>&nbsp;</td>
			  <td colspan='2' align='right'>
				<b>$subtotals($old_cost_type_id) $default_currency</b>
			  </td>
	      <td colspan='4'>&nbsp;</td>			  
			</tr>
				"
	}
	append cost_html "
		<tr class='rowwhite'>
		  <td colspan='$colspan'>&nbsp;</td>
		</tr>\n"
}
    
# Add a reasonable message if there are no documents
if {$ctr == 1} {
	 append cost_html "
	<tr$bgcolor([expr $ctr % 2])>
	  <td colspan=$colspan align=center>
		<I>[_ intranet-cost.lt_No_financial_document]</I>
	  </td>
	</tr>\n"
	 incr ctr
}
    
# Close the main table
append cost_html "</tbody></table>\n"
    
if {!$show_details_p} { set cost_html "" }
    
    
# ----------------- Hard Costs HTML -------------
# Hard "real" costs such as invoices, bills and timesheet
    
set hard_cost_html "
<table with=\"100%\">
  <tr class=rowtitle>
	<td class=rowtitle colspan=2 align=center>[_ intranet-cost.Real_Costs]</td>
  </tr>
  <tr>
	<td>[_ intranet-cost.Customer_Invoices]</td>\n"
set subtotal $subtotals([im_cost_type_invoice])
append hard_cost_html "<td align=right>$subtotal $default_currency</td>\n"
set grand_total $subtotal
    
append hard_cost_html "</tr>\n<tr>\n<td>[_ intranet-cost.Provider_Bills]</td>\n"
set subtotal $subtotals([im_cost_type_bill])
append hard_cost_html "<td align=right>- $subtotal $default_currency</td>\n"
set grand_total [expr $grand_total - $subtotal]
    
append hard_cost_html "</tr>\n<tr>\n<td>[_ intranet-cost.Timesheet_Costs]</td>\n"
set subtotal $subtotals([im_cost_type_timesheet])
append hard_cost_html "<td align=right>- $subtotal $default_currency</td>\n"
set grand_total [expr $grand_total - $subtotal]
    
append hard_cost_html "</tr>\n<tr>\n<td>[lang::message::lookup "" intranet-cost.Expenses "Expenses"]</td>\n"
set subtotal $subtotals([im_cost_type_expense_bundle])
append hard_cost_html "<td align=right>- $subtotal $default_currency</td>\n"
set grand_total [expr $grand_total - $subtotal]
    
set grand_total [expr round($rounding_factor * $grand_total) / $rounding_factor]
append hard_cost_html "</tr>\n<tr>\n<td><b>[_ intranet-cost.Grand_Total]</b></td>\n"
append hard_cost_html "<td align=right><b>$grand_total $default_currency</b></td>\n"
    
 #    append hard_cost_html "<td align=right><b>[lc_numeric $grand_total "" $locale] $default_currency</b></td>\n"
    
append hard_cost_html "</tr>\n</table>\n"
    
    
# ----------------- Prelim Costs HTML -------------
# Preliminary (planned) Costs such as Quotes and Purchase Orders
    
set prelim_cost_html "
 <table width=\"100%\">
   <tr class=rowtitle>
<td class=rowtitle colspan=2 align=center>[_ intranet-cost.Preliminary_Costs]</td>
   </tr>
   <tr>
<td>[_ intranet-cost.Quotes]</td>\n"
    
set subtotal $subtotals([im_cost_type_quote])
append prelim_cost_html "<td align=right>$subtotal $default_currency</td>\n"
set grand_total $subtotal
    
append prelim_cost_html "</tr>\n<tr>\n<td>[_ intranet-cost.Purchase_Orders]</td>\n"
set subtotal $subtotals([im_cost_type_po])
append prelim_cost_html "<td align=right>- $subtotal $default_currency</td>\n"
set grand_total [expr $grand_total - $subtotal]
    
append prelim_cost_html "</tr>\n<tr>\n<td>[lang::message::lookup "" intranet-cost.Timesheet_Budget "Timesheet Budget"]</td>\n"
set subtotal $subtotals([im_cost_type_timesheet_budget])
append prelim_cost_html "<td align=right>- $subtotal $default_currency</td>\n"
set grand_total [expr $grand_total - $subtotal]
    
append prelim_cost_html "</tr>\n<tr>\n<td>[lang::message::lookup "" intranet-cost.Expense_Budget "Expense Budget"]</td>\n"
set subtotal $subtotals([im_cost_type_expense_planned])
append prelim_cost_html "<td align=right>- $subtotal $default_currency</td>\n"
set grand_total [expr $grand_total - $subtotal]
    
set grand_total [expr round($rounding_factor * $grand_total) / $rounding_factor]
append prelim_cost_html "</tr>\n<tr>\n<td><b>[lang::message::lookup "" intranet-cost.Preliminary_Total "Preliminary Total"]</b></td>\n"
append prelim_cost_html "<td align=right><b>$grand_total $default_currency</b></td>\n"
append prelim_cost_html "</tr>\n</table>\n"
    
    
# ----------------- Check that the Exchange Rates are still valid  -------------
    
# See which currencies are to be added here...
set used_currencies [db_list currencies_used "
select distinct
	ci.currency
from	im_costs ci
where	ci.currency != :default_currency
	and cost_id in (
		$project_cost_ids_sql
	)
"]
    
set exchange_rates_outdated [im_exchange_rate_outdated_currencies]
set currency_outdated_warning ""
if {[llength $used_currencies] > 0 && [llength $exchange_rates_outdated] > 0} {
    
set currency_outdated_warning [lang::message::lookup "" intranet-cost.The_exchange_rates_are_outdated "The exchanges rates for the following currencies are outdated. <br>Please contact your System Administrator to update the following exchange rates:"]
    
append currency_outdated_warning "
	<ul>
	<li><a href='/intranet-exchange-rate/'>
		[lang::message::lookup "" intranet-cost.Update_Exchange_Rates "Update Exchange Rates"]
		</a>
	</ul>
"
    
set first_p 1
foreach entry $exchange_rates_outdated {
	set currency [lindex $entry 0]
	set days [lindex $entry 1]
	set outdated_msg [lang::message::lookup "" intranet-cost.Outdated_since_x_days "Outdated since %days% days"]
	if {!$first_p} { append currency_outdated_warning ", " }
	append currency_outdated_warning "$currency: $outdated_msg\n"
	set first_p 0
}
    
set currency_outdated_warning "
	<table>
	<tr class=rowtitle><td class=rowtitle>
	[lang::message::lookup "" intranet-cost.Outdated_Message "Outdated Exchange Rates Warning"]
	</td></tr>
	<tr><td>$currency_outdated_warning</td></tr>
	</table>
	"
    
}
    
    
# ----------------- Admin Links --------------------------------
    
# Restore value overwritten by SQL query
set project_id $org_project_id
    
# Add some links to create new financial documents
# if the intranet-invoices module is installed
set admin_html ""
if {$show_admin_links_p && [im_table_exists im_invoices]} {
# Customer invoices: customer = Project Customer, provider = Internal
set customer_id [util_memoize [list db_string project_customer "select company_id from im_projects where project_id = $org_project_id" -default ""]]
set provider_id [im_company_internal]
set bind_vars [list project_id $project_id customer_id $customer_id provider_id $provider_id]
    
# set html_customer_links [im_menu_ul_list -package_key intranet-invoices "invoices_customers" $bind_vars]
set html_customer_links [im_menu_ul_list "invoices_customers" $bind_vars]
    
# Provider invoices: customer = Internal, no provider yet defined
set customer_id [im_company_internal]
set bind_vars [list customer_id $customer_id project_id $project_id]	
    
# set html_provider_links [im_menu_ul_list -package_key intranet-invoices "invoices_providers" $bind_vars]
set html_provider_links [im_menu_ul_list "invoices_providers" $bind_vars]
    
if { ![empty_string_p $html_customer_links] || ![empty_string_p $html_provider_links] } {
	set admin_html "
	<h1>[_ intranet-core.Admin_Links]</h1>
	<table>
	<tr class=rowplain>
	<td>\n"
	if { ![empty_string_p $html_customer_links] } {
		append admin_html "<h2>"
			append admin_html [lang::message::lookup "" intranet-cost.Customer_Links "Customer Actions"]
			append admin_html "</h2>"	
	  			append admin_html $html_customer_links
	}
			if { ![empty_string_p $html_provider_links] } {
		append admin_html "<br><h2>"			
		append admin_html [lang::message::lookup "" intranet-cost.Provider_Links "Provider Actions"]
		append admin_html "</h2>"	
		append admin_html $html_provider_links
	}
	append admin_html "	
  </td>
</tr>
</table>
"
}
 }
    
# ----------------- Assemble the "Summary" component ---------------------------
# With preliminary and hard costs
    
    
set summary_html ""
if {$show_details_p} {
    
# Summary in broad format
set summary_html "
 <h1>[_ intranet-cost.Financial_Summary]</h1>
 <table cellspacing=0 cellpadding=0>
 <!--<tr><td class=rowtitle colspan=3>[_ intranet-cost.Financial_Summary]</td></tr>-->
 <tr valign=top>
   <td>$hard_cost_html</td>
   <td>&nbsp &nbsp;</td>
   <td>$prelim_cost_html</td>
 </tr>
 </table>\n"
    
} else {
    
# Summary in narrow format
set summary_html "
	<table cellspacing=0 cellpadding=0 width=\"100%\">
	<tr><td class=rowtitle>[_ intranet-cost.Financial_Summary]</td></tr>
	<tr valign=top><td>$hard_cost_html</td></tr>
	<tr><td>$prelim_cost_html</td></tr>
	</table>\n"
    
  	set summary_html "
$hard_cost_html
  		<br>
  	$prelim_cost_html
	"
}
    
if {!$show_summary_p} { set summary_html "" }
if {!$can_read_summary_p} { set summary_html "" }
