ad_library {

    Callbacks for cognovis-core

    @author malte.sussdorff@cognovis.de
}

ad_proc -callback im_invoice_after_update -impl cognovis_delete {
    -object_id:required
    -status_id:required
    -type_id:required
} {
    Delete linked invoices relationship
} {
    if {$status_id eq [im_cost_status_deleted] || $status_id eq [im_cost_status_rejected]} {
        set rel_ids [db_list rel_ids "select object_id_one from acs_rels where object_id_two = :object_id and rel_type = 'im_invoice_invoice_rel'
            UNION select object_id_two from acs_rels where object_id_one = :object_id and rel_type = 'im_invoice_invoice_rel'"]

        foreach rel_id $rel_ids {
            relation_remove $rel_id
        }
    }
}

ad_proc -public -callback intranet_chilkat::after_sending -impl cog_updating_bill_invoice {
    -message_id:required
    -email:required
    {-context_id ""}
    {-file_ids ""}
} {

    Update the status of the financial document if the mail went through
    
    @param mesasage_id the generated message_id for this mail
    @param email CkEMail Object of the mail send
    @param context_id The ID of the object that is responsible for sending the mail in the first place
    @param file_ids IDs of the cr_files attached as we don't store them in the email
} {
     # Don't update if we don't have an object_id or attached file
    if {$context_id eq "" || $file_ids eq ""} {
        return
    }

    set cost_type_id [db_string cost_type "select cost_type_id from im_costs where cost_id = :context_id" -default ""]
    switch $cost_type_id {
        3700 - 3702 - 3704 - 3706 - 3725 - 3735 - 3740 - 3741 {
            set cost_status_id [db_string cost_status "select cost_status_id from im_costs where cost_id = :context_id" -default ""]
            switch $cost_status_id {
                3802 - 3815 - 3816 { 
                    set from_addr [CkEmail_fromAddress $email]
                    set from_party_id [party::get_by_email -email $from_addr]
                    cog_rest::put::invoice -invoice_id $context_id -cost_status_id [im_cost_status_outstanding] -rest_user_id $from_party_id
                }
            }
        }
        default {
            # do nothing
        }
    }
}

ad_proc -public -callback im_invoice_after_update -impl update_last_modified {
    -object_id
    { -type_id "" }
    { -status_id "" }
} {
    Updates last_modified in acs_objects
} {
    # Make sure to update modification date
    db_dml update_modification_date "UPDATE acs_objects set last_modified = now() where object_id = :object_id"
}

ad_proc -public -callback im_invoice_after_create -impl cog_default_delivery_date {
    -object_id
    { -type_id "" }
    { -status_id "" }
} {
    Set the default for delivery_date if none was provided
} {
    db_1row cost_info "select project_id, cost_type_id, delivery_date from im_costs where cost_id = :object_id"

    if {$delivery_date eq ""} {
        switch $cost_type_id {
            3700 {
                if {$project_id ne ""} {
                    db_dml update_delivery_date "update im_costs set delivery_date = (select end_date from im_projects where project_id = :project_id) where cost_id = :object_id"
                    return "Updated delivery_date for invoice $object_id to project end_date"
                }
            }
            3704 {
                # Update to the deadline of the assignment.
                # db_dml update_delivery_date "update im_costs set delivery_date =  where cost_id = :object_id"
            }
        }
    } 
}

ad_proc -public -callback im_invoice_after_update -impl cog_update_amounts {
    -object_id
    { -type_id "" }
    { -status_id "" }
} {
    Updating VAT + Amount
} {
    return "Updated $object_id [cog::invoice::update_rounded_amount -invoice_id $object_id]"
}

ad_proc -public -callback im_invoice_after_create -impl cog_update_amounts {
    -object_id
    { -type_id "" }
    { -status_id "" }
} {
    Updating VAT + Amount
} {
    return "Updated $object_id [cog::invoice::update_rounded_amount -invoice_id $object_id]"
}

ad_proc -public -callback im_invoice_after_create -impl cog_permissions {
    -object_id
    { -type_id "" }
    { -status_id "" }
} {
    Grant permission to the recipient of the invoice
} {
    return "[callback::im_invoice_after_update::impl::cog_permissions -object_id $object_id]"
}

ad_proc -public -callback im_invoice_after_update -impl cog_permissions {
    -object_id
    { -type_id "" }
    { -status_id "" }
} {
    Grant permission to the recipient of the invoice
} {
    set invoice_recipient_id [db_string recipient "select company_contact_id from im_invoices where invoice_id = :object_id" -default ""]
    set project_contact_id [db_string recipient "select company_contact_id from im_projects p, im_costs c
        where c.project_id = p.project_id and c.cost_id = :object_id" -default ""]
    set recipient_ids [lsort -unique [list $invoice_recipient_id $project_contact_id]]
    foreach recipient_id $recipient_ids {
        if {$recipient_id ne ""} {
            set perm_proc [cog_rest::helper::object_permission_proc -object_type "im_invoice"]
            $perm_proc $recipient_id $object_id view read write admin
            if {!$read} {
                permission::grant -party_id $recipient_id -object_id $object_id -privilege "read"
                return "Granted read for $object_id to $recipient_id"
            }
        }
    }
    return ""
}

ad_proc -public -callback im_payment_after_create -impl cog_update_status {
    -payment_id:required
} {
    Set the status to paid / partially paid
} {

    if {![db_0or1row payment_info "select c.cost_id, cost_type_id, cost_status_id as old_cost_status_id 
        from im_payments p, im_costs c
        where c.cost_id = p.cost_id
        and p.payment_id = :payment_id"]} {
        return
    }
    
    db_1row payment "select amount - paid_amount as gap, paid_amount from im_costs where cost_id = :cost_id"
    set cost_status_id $old_cost_status_id
    if {$gap <=0} {
        set cost_status_id [im_cost_status_paid]
    } else {
	if {$paid_amount >0} {
	    set cost_status_id [im_cost_status_partially_paid]
	}
    } 

    if {$old_cost_status_id ne $cost_status_id} {        
        db_dml mark_invoice_as_paid "
            update im_costs set
                cost_status_id = :cost_status_id
                where cost_id = :cost_id
            "

        callback im_invoice_after_update -object_id $cost_id -status_id $cost_status_id -type_id $cost_type_id
    }
}

ad_proc -public -callback im_payment_after_update -impl cog_update_status {
    -payment_id:required
} {
    Set the status to paid / partially paid
} {

    cog_log Notice "Recoding paymnet $payment_id"
    if {![db_0or1row payment_info "select c.cost_id, cost_type_id, cost_status_id as old_cost_status_id 
        from im_payments p, im_costs c
        where c.cost_id = p.cost_id
        and p.payment_id = :payment_id"]} {
        return
    }
    
    im_cost_update_payments $cost_id 

    db_1row payment "select amount - paid_amount as gap, paid_amount from im_costs where cost_id = :cost_id"
    set cost_status_id $old_cost_status_id            
    if {$gap <=0} {
        set cost_status_id [im_cost_status_paid]
    } else {
	if {$paid_amount >0} {
	    set cost_status_id [im_cost_status_partially_paid]
	}
    } 

    if {$old_cost_status_id ne $cost_status_id} {        
        db_dml mark_invoice_as_paid "
            update im_costs set
                cost_status_id = :cost_status_id
                where cost_id = :cost_id
            "

        callback im_invoice_after_update -object_id $cost_id -status_id $cost_status_id -type_id $cost_type_id
    }
}
