ad_library {
    cognovis specific procedures for relationships
    
    @author malte.sussdorff@cognovis.de
}

namespace eval cog::rel {
    ad_proc -public remove {
        -object_id:required 
        { -current_user_id "" }
    } {
        Remove the relationships for an object
    } {
        if {$current_user_id eq ""} { set current_user_id [auth::get_user_id]}

        foreach rel_id [db_list rels "
            select rel_id 
            from acs_rels 
            where object_id_one = :object_id 
                or object_id_two = :object_id
        "] {
            db_dml del_rels "delete from group_element_index where rel_id = :rel_id"
    	    db_dml del_rel_skill_profile_ref "update im_biz_object_members set skill_profile_rel_id = null where skill_profile_rel_id = :rel_id"
            db_1row delete "select acs_rel__delete(:rel_id)"
        }
    }

    ad_proc -public add {
        -object_id_one:required
        -object_id_two:required
        {-rel_type "membership_rel"}
        {-context_id ""}
        {-creation_user ""}
    } {
        Add a new relationship

    } {
        set rel_id [relation::get_id -object_id_one $object_id_one -object_id_two $object_id_two -rel_type $rel_type]
        if {$rel_id eq ""} {
            if {$creation_user eq ""} {
                set creation_user [auth::get_user_id]
            }
            set rel_id [db_exec_plsql create_rel "
                select acs_rel__new (
                    null,             -- rel_id
                    :rel_type,   -- rel_type
                    :object_id_one,      -- object_id_one
                    :object_id_two,      -- object_id_two
                    :context_id,             -- context_id
                    :creation_user,             -- creation_user
                    null             -- creation_ip
                )
            "]
        }
    }
}

namespace eval cog::mail {
    ad_proc -public nuke_log {
        -object_id:required
        { -current_user_id "" }
    } {
        Delete the mail_log entry for an object_id, (either a context or sender)
    } {
        if {$current_user_id eq ""} { set current_user_id [auth::get_user_id]}
        if {![acs_user::site_wide_admin_p -user_id $current_user_id]} {
           return ""
        }
        set mail_log_ids [db_list mail_logs "select log_id from acs_mail_log where context_id = :object_id or sender_id = :object_id"]
        foreach log_id $mail_log_ids {
            db_1row delete "select acs_mail_log__delete(:log_id)"
        }
    }
}
