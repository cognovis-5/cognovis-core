ad_library {
    Procedures to handle translation
    @author malte.sussdorff@cognovis.de
}

namespace eval cog::trans::task {
    ad_proc -public new {
        -project_id:required
        -task_name:required
        { -task_uom_id ""}
        {-task_units ""}
        {-billable_units ""}
        { -task_type_id ""}
        { -target_language_ids ""}
        {-user_id ""}
        {-end_date ""}
        {-description ""}
    } {
        @param project_id Project in which we create the tasks
        @param task_name Name of the task to create (will duplicate per target language)
        @param task_uom_id category_id for the unit of measure. Defaults to im_uom_s_word
        @param task_type_id category_id for the task, derived from Intranet Project Types. Defaults to im_project_type
        @param user_id User generating the task, defaults to currently logged in user
        @param task_units Number of units for the task
        @param billable_units Number of billable units of the task
        @param end_date Task end_date

        @return List of created task_ids
    } {

        # Check user
        if {$user_id eq ""} {
            set user_id [auth::get_user_id]
        }

        if {$task_type_id eq ""} {
            set task_type_id [db_string task_type_id "select project_type_id from im_projects where project_id = :project_id"]
        }
    
        if {$target_language_ids eq ""} {
            set target_language_ids [im_target_languages $project_id]
        }

        if {$task_uom_id eq ""} {
            set task_uom_id [im_uom_s_word] 
        }

        # Clean up target_language_ids to make them unique
        set target_language_ids [lsearch -all -inline -not -exact [lsort -unique $target_language_ids] {}]
        
        set source_language_id [db_string source_language_id "select source_language_id from im_projects where project_id = :project_id"]
        
        # Created status id
        set task_status_id 340
        set ip_address [ns_conn peeraddr]        

        # Set default for billable units
        if {$billable_units eq ""} {set billable_units $task_units}
        
        set task_ids [list]
        
        if {[llength $target_language_ids] <1} {
            ns_log Error "$project_id does not have target Languages"
        }
        
        # Add a new task for every project target language
        foreach target_language_id $target_language_ids {
            # Check for duplicated task names
            if {![cog::trans::task::name_exists_p -project_id $project_id -task_name $task_name -target_language_id $target_language_id]} {
                # Keep track of the created tasks
                set new_task_id [im_exec_dml new_task "im_trans_task__new (
                    null,			-- task_id
                    'im_trans_task',	-- object_type
                    now(),			-- creation_date
                    :user_id,		-- creation_user
                    :ip_address,		-- creation_ip
                    null,			-- context_id
            
                    :project_id,		-- project_id
                    :task_type_id,		-- task_type_id
                    :task_status_id,	-- task_status_id
                    :source_language_id,	-- source_language_id
                    :target_language_id,	-- target_language_id
                    :task_uom_id		-- task_uom_id
                    
                    )"]

                db_dml update_task "
                    UPDATE im_trans_tasks SET
                        description=:description,
                        task_name = :task_name,
                        task_units = :task_units,
                        billable_units = :billable_units,
                        end_date = :end_date
                    WHERE
                        task_id = :new_task_id
                "
            
                # Successfully created translation task
                cog::callback::invoke -object_type "im_trans_task" -action after_create -object_id $new_task_id -status_id $task_status_id -type_id $task_type_id

                # If new target language was added, we also need to add it to project basic information
                set tl_in_project_p [db_string tl_count "select count(language_id) from im_target_languages where project_id =:project_id and language_id =:target_language_id" -default 0]
                if {!$tl_in_project_p} {
                    db_dml insert_im_target_language "insert into im_target_languages values ($project_id, $target_language_id)"
                }

            } else {
                #Get the existing task id back
                set new_task_id [db_string get_existing_task "select task_id
                from im_trans_tasks
                where
                    lower(task_name) = lower(:task_name)
                    and project_id = :project_id
                    and target_language_id = :target_language_id" ]
            }
            lappend task_ids $new_task_id
        }

        return $task_ids        
    }

    ad_proc -public update {
        -task_id:required
        -task_name:required
        {-task_units ""}
        {-billable_units ""}
        -task_uom_id:required
        -task_type_id:required
        -target_language_id:required
        {-end_date ""}
    } {
        @param task_id Task which we want to update
        @param task_name Name of the task to create (will duplicate per target language)
        @param task_uom_id category_id for the unit of measure
        @param task_type_id category_id for the task, derived from Intranet Project Types
        @param target_language_id category for the target language
        @param task_units Number of units for the task
        @param billable_units Number of billable units of the task
        @param end_date Task end_date

        @return List of created task_ids
    } {

        # Set default for billable units
        if {$billable_units eq ""} {set billable_units $task_units}

        # Check for duplicated task names
        if {[cog::trans::task::name_exists_p -task_id $task_id -task_name $task_name -target_language_id $target_language_id]} {
            cog_rest::error -http_status 400 -message "[_ intranet-translation.Database_Error] - [_ intranet-translation.lt_Did_you_enter_the_sam]"
        }
        
        db_dml update_task "
            UPDATE im_trans_tasks SET
                task_name = :task_name,
                task_units = :task_units,
                task_type_id = :task_type_id,
                task_uom_id = :task_uom_id,
                target_language_id = :target_language_id,
                billable_units = :billable_units,
                end_date = :end_date
            WHERE
                task_id = :task_id
        "
    
        cog::callback::invoke -object_type "im_trans_task" -action after_update -object_id $task_id
        
        return $task_id
        
    }

    ad_proc -public removeable_p {
        -task_id:required
    } {
        Procedure to determine if a task can be delete.

        You can't delete a task if it has active assignments

        @param task_id Id of the task

        @return boolean 0/1 depending if the task can be deleted
    } {

        # Check if the task is in packages at all
        set task_in_package_p [db_string package_p "select 1 from im_freelance_packages_trans_tasks where trans_task_id = :task_id limit 1" -default 0]

        if {$task_in_package_p} {
            # Assignments that are not Denied, Assignment Deleted, Assignment Closed
            set deleted_assignment_status_ids [list 4223 4230 4231]

            set task_not_removeable_p [db_string removable "select 1 from im_trans_tasks tt, im_freelance_assignments fa, im_freelance_packages_trans_tasks fp
                where tt.task_id = fp.trans_task_id
                and fp.freelance_package_id = fa.freelance_package_id
                and tt.task_id = :task_id
                and fa.assignment_status_id not in ([ns_dbquotelist $deleted_assignment_status_ids])
                limit 1
                " -default 0]
            
            if {$task_not_removeable_p} {
                return 0
            } else {
                return 1
            }
        } else {
            return 1
        }
    }

    ad_proc -public name_exists_p {
        -task_name:required
        {-task_id ""}
        {-project_id ""}
        {-target_language_id ""}
    } {

    } {
        if {$project_id eq "" && $task_id eq ""} {
            ns_log Error "task_name_exists_p: you need to provide at least project_id or task_id"
            return 0
        }

        if {$project_id eq ""} {
            set project_id [db_string project_from_task "select project_id from im_trans_tasks where task_id = :task_id" -default ""]
            if {$project_id eq ""} {
                ns_log Error "task_name_exists_p: task_id does not seem to exist. you need to provide at least project_id or task_id"
            }
        }

        if {$target_language_id eq ""} {
            # Need both project and task in this case
            if {$task_id ne ""} {
                set target_language_id [db_string target_language_from_task "select target_language_id from im_trans_tasks where task_id = :task_id" -default ""]
            }
        }


        if {$task_id ne ""} {
            set task_name_exists_p [db_string task_name_count "
                select 1
                from im_trans_tasks
                where
                    lower(task_name) = lower(:task_name)
                    and project_id = :project_id
                    and target_language_id = :target_language_id
                    and task_id != :task_id                    
            " -default 0]
        } else {
            
            set task_name_exists_p [db_string task_name_count "
                select 1
                from im_trans_tasks
                where
                    lower(task_name) = lower(:task_name)
                    and project_id = :project_id
                    and target_language_id = :target_language_id
            " -default 0]
        }

        ns_log Debug "Trans task exists  $task_name_exists_p $task_id ... $task_name $project_id $target_language_id"

        return $task_name_exists_p
    }


    ad_proc -public trados_tasks_new { 	
         -project_id:required
         -trados_analysis_xml:required
         -target_language_id:required
         -user_id:required
         -only_new_p:required
    } { 	
        Create translation tasks from trados analysis 
    } {     
        set created_task_ids ""
        
        # Setup the elements to search for and how to map them
        keylset element_keyl tag "perfect"
        keylset element_keyl variable "match_perf"
        set single_elements [list $element_keyl]
        
        keylset element_keyl tag "inContextExact"
        keylset element_keyl variable "match_x"
        lappend single_elements $element_keyl
        
        keylset element_keyl tag "exact"
        keylset element_keyl variable "match100"
        lappend single_elements $element_keyl

        keylset element_keyl tag "new"
        keylset element_keyl variable "match0"
        lappend single_elements $element_keyl	

        keylset element_keyl tag "locked"
        keylset element_keyl variable "locked"
        lappend single_elements $element_keyl
        
        keylset element_keyl tag "repeated"
        keylset element_keyl variable "match_rep"
        lappend single_elements $element_keyl
        
        keylset element_keyl tag "crossFileRepeated"
        keylset element_keyl variable "match_cfr"
        lappend single_elements $element_keyl
        
        # ---------------------------------------------------------------
        # Get the XML Analysis
        # ---------------------------------------------------------------

        set xml [new_CkXml]
        
        set success [CkXml_LoadXmlFile $xml $trados_analysis_xml]
        if {[expr $success != 1]} then {
            ns_log Error [CkXml_lastErrorText $xml]
            delete_CkXml $xml
            exit
        }
        CkXml_put_Encoding $xml "utf-8"
        CkXml_put_Utf8 $xml 1

        # ---------------------------------------------------------------
        # Loop through all files which were analysed
        # ---------------------------------------------------------------
        
        set numFiles [CkXml_NumChildrenHavingTag $xml "file"]
        set variables_for_array [list match_x match_rep match100 match95 match85 match75 match50 match0  match_perf match_cfr match_f95 match_f85 match_f75 match_f50 locked]
        set file_names [list]
        
        for {set i 0} {$i <= [expr $numFiles - 1]} {incr i} {
            set success [CkXml_GetNthChildWithTag2 $xml "file" $i]
            set file_name [CkXml_getAttrValue $xml name]
        
            # Skip already processed files
            if {[lsearch $file_names $file_name]<0} {
                lappend file_names $file_name
            } else {
                continue
            }
            
            # Go into the analysis
            CkXml_FindChild2 $xml "analyse"
            
            # Loop through all single elements to get the words
            foreach element_keyl $single_elements {
                CkXml_FindChild2 $xml [keylget element_keyl tag]
                set [keylget element_keyl variable] [CkXml_getAttrValue $xml words]
                CkXml_GetParent2 $xml ; # element
                
                set variable [keylget element_keyl variable]
            }
            
            # Loop through all the fuzzy elements and get the words
            set numFuzzy [CkXml_NumChildrenHavingTag $xml "fuzzy"]
            for {set k 0} {$k <= [expr $numFuzzy - 1]} {incr k} {
                CkXml_GetNthChildWithTag2 $xml "fuzzy" $k
                set min [CkXml_getAttrValue $xml min]
                set match$min [CkXml_getAttrValue $xml words]
                CkXml_GetParent2 $xml ; # fuzzy
            }

            # Loop through all the internalfuzzy elements and get the words
            # they are optional, set the variables just in case
            foreach percent [list 50 75 85 95] {
                set match_f$percent 0
            }
            set numFuzzy [CkXml_NumChildrenHavingTag $xml "internalfuzzy"]
            for {set k 0} {$k <= [expr $numFuzzy - 1]} {incr k} {
                CkXml_GetNthChildWithTag2 $xml "internalfuzzy" $k
                set min [CkXml_getAttrValue $xml min]
                set match_f$min [CkXml_getAttrValue $xml words]
                CkXml_GetParent2 $xml ; # fuzzy
            }
            
            CkXml_GetParent2 $xml ; # analyse
            CkXml_GetParent2 $xml ; # file
            
            # Set the variables in the array for later reuse
            foreach variable $variables_for_array {
                if {[info exists $variable]} {
                    set ${variable}_arr($file_name) [set $variable]
                } else {
                    set ${variable}_arr($file_name) ""
                }
            }
        }


        db_1row project_info "select company_id,project_type_id as task_type_id from im_projects where project_id = :project_id"

        foreach file_name $file_names {
            
            # Take the array variables back :-)
            foreach variable $variables_for_array {
                set $variable [set  ${variable}_arr($file_name)]
            }
            
            # ---------------------------------------------------------------
            # Insert / Update the task
            # ---------------------------------------------------------------

            # Remove the sdlxliff
            set file_name [string trimright $file_name "sdlxliff"]
            set file_name [string range $file_name 0 end-1]
            set task_name $file_name
            set task_type "trans"
            set task_units [im_trans_trados_matrix_calculate [im_company_freelance]  $match_x $match_rep $match100 $match95 $match85 $match75 $match50 $match0  $match_perf $match_cfr $match_f95 $match_f85 $match_f75 $match_f50 $locked $task_type]
            
            # Determine the "billable_units" form the project's customer:
            set billable_units [im_trans_trados_matrix_calculate $company_id  $match_x $match_rep $match100 $match95 $match85 $match75 $match50 $match0  $match_perf $match_cfr $match_f95 $match_f85 $match_f75 $match_f50 $locked $task_type]
                        
            set task_id [db_string task "select task_id from im_trans_tasks where task_name = :task_name and project_id = :project_id and target_language_id = :target_language_id" -default ""]
            
            if {$task_id eq ""} {
                
                set task_status_id 340
                
                set task_id [cog::trans::task::new \
                    -project_id $project_id \
                    -task_name $task_name \
                    -task_type_id $task_type_id \
                    -user_id $user_id \
                    -task_uom_id 324 \
                    -task_units $task_units \
                    -billable_units $billable_units \
                    -target_language_ids $target_language_id]
                
                set update_match_p 1
            } else {
                if {$only_new_p} {
                    set update_match_p 0
                } else {
                    set update_match_p 1
                    db_dml update_units "update im_trans_tasks set task_units = :task_units, billable_units = :billable_units where task_id = :task_id"
                    cog::callback::invoke -object_type "im_trans_task" -action after_update -object_id $task_id
                }
            }
        
            if {$update_match_p} {
                db_dml update_task "
                    UPDATE im_trans_tasks SET
                    tm_integration_type_id = [im_trans_tm_integration_type_external],
                    match_x = :match_x,
                    match_rep = :match_rep,
                    match100 = :match100, 
                    match95 = :match95,
                    match85 = :match85,
                    match75 = :match75, 
                    match50 = :match50,
                    match0 = :match0,
                    match_perf = :match_perf,
                    match_cfr = :match_cfr,
                    match_f95 = :match_f95,
                    match_f85 = :match_f85,
                    match_f75 = :match_f75,
                    match_f50 = :match_f50,
                    locked = :locked
                    WHERE 
                    task_id = :task_id
                    "
            }    

            lappend created_task_ids $task_id
        }

        # Generate batches for the provided task_ids
        cog::trans::packages::create -trans_task_ids $created_task_ids
        
        return $created_task_ids
    }


    ad_proc -public tasks_from_xls {
        -filename:required
        -path:required
        -project_id:required
        -rest_user_id
    } {
        Creates tasks from single uploaded CSV/XLS file

        @param filename string Name of the file (original filename)
        @param path string Path to the file as returned by upload_fs_file endpoint
        @param project_id object im_project::write Project in which to create the task - needs write permission

        @return trans_tasks json_array trans_task Task which were created
    } {

        set trans_tasks [list]
        set new_task_ids [list]

        set task_csv [new_CkCsv]
        # Put into UTF-8 mode
        CkCsv_put_Utf8 $task_csv 1

        CkCsv_put_HasColumnNames $task_csv 1

        # In here we previosly had a loop, but now we upload only single file so it wasn't needed anymore
        # We still might have loop here in future

        # Extract file exetension and proceed only if its not empty
        set extension [file extension $filename]
        if {$extension ne ""} {
           switch $extension {
                ".xls" - ".xlsx" {
                    # Konvert the XLS to CSV for impport
                    set xls_filename "[file rootname $path]$extension"
                    file rename $path $xls_filename
                    set csv_file [intranet_oo::convert_to -oo_file $xls_filename -convert_to "csv"]
                    file delete $xls_filename
                }
                ".csv" {
                    set csv_file $path
                }
            }

            set success [CkCsv_LoadFile $task_csv $csv_file]
            if {$success != 1} then {
                ns_log Error [CkCsv_lastErrorText $task_csv]
                delete_CkCsv $task_csv
                cog_rest::error -http_status 400 -message "[_ webix-portal.err_load_file_parsing]"
                return
            }
 
            set n [CkCsv_get_NumRows $task_csv]

            # Check the column names
            set numCols [CkCsv_get_NumColumns $task_csv]
            set column_names [list]
            for {set i 0} {$i < $numCols} {incr i} {
                set column_name [CkCsv_getColumnName $task_csv $i]
                lappend column_names $column_name
            }

            # Check we have the mandatory columns
            foreach mandatory_column [list task_name task_units task_uom] {
                if {[lsearch $column_names $mandatory_column]<0} {
                    cog_rest::error -http_status 400 -message "[_ webix-portal.err_miss_column]"
                }
            }

            set project_target_languages_names [im_target_languages $project_id]
            set project_target_language_ids [cog::trans::project_valid_language_ids -project_id $project_id -type "target"]
            set project_task_type_id [db_string project_task_type "select project_type_id from im_projects where project_id = :project_id"]
            set task_type_ids [cog::trans::project::trans_task_types -project_id $project_id]

            callback cog::trans::project::task_types -project_type_id $project_task_type_id -project_id $project_id -user_id $rest_user_id

            # Get a list of batch names to use later for batch generation
            set batch_names [list]

            for {set row 0} {$row <= [expr $n - 1]} {incr row} {
                set task_name [CkCsv_getCellByName $task_csv $row "task_name"]
                if {[string trim $task_name] eq ""} {
                    continue
                }

                set task_uom [CkCsv_getCellByName $task_csv $row "task_uom"]
                set task_uom_id [cog_category_id -category $task_uom -category_type "Intranet UoM"]
                if {$task_uom_id eq ""} {
                    set task_uom_id [cog_category_id -category "S-Word" -category_type "Intranet UoM"]
                }

                set task_units [CkCsv_getCellByName $task_csv $row "task_units"]

                set target_language [CkCsv_getCellByName $task_csv $row "target_language"]
                if {$target_language eq ""} {
                    set target_language_ids $project_target_language_ids
                } else {
                    if {[lsearch $project_target_languages_names $target_language] == -1} {
                        ns_log Error "$task_name: target language ($target_language) does not match the ones in project"
                        continue
                    } else {
                        set target_language_ids [cog_category_id -category "$target_language" -category_type "Intranet Translation Language"]
                    }
                }

                set task_type [CkCsv_getCellByName $task_csv $row "task_type"]
                if {$task_type eq ""} {
                    set task_type_id $project_task_type_id
                } else {
                    set task_type_id [cog_category_id -category "$task_type" -category_type "Intranet Project Type"]
                }            

                if {[lsearch $task_type_ids $task_type_id]<0} {
                    ns_log Error "$task_name: Task type $task_type is not valid in this project"
                }
                
                set task_deadline [CkCsv_getCellByName $task_csv $row "task_deadline"]


                set billable_units $task_units
                set new_task_ids [cog::trans::task::new \
                    -project_id $project_id \
                    -task_name $task_name \
                    -task_units $task_units \
                    -task_uom_id $task_uom_id \
                    -task_type_id $task_type_id \
                    -target_language_ids $target_language_ids \
                    -billable_units $billable_units \
                    -user_id $rest_user_id \
                    -end_date $task_deadline]

                set batch_name [CkCsv_getCellByName $task_csv $row "batch_name"]
                if {$batch_name eq ""} {
                    set batch_name "default"
                } 

                if {[lsearch $batch_names $batch_name]<0} {
                    lappend batch_names $batch_name
                }
                
                if {[info exists tasks_in_batch($batch_name)]} {
                    set tasks_in_batch($batch_name) [list {*}$tasks_in_batch($batch_name) {*}$new_task_ids]
                } else {
                    set tasks_in_batch($batch_name) $new_task_ids
                }
                callback cog_trans_task_after_create -trans_task_ids $new_task_ids -project_id $project_id
                
            }
        }

        if {[llength $new_task_ids] > 0} {

            # Create packages for the batches
            foreach package_name $batch_names {
                if {$package_name eq "default"} {
                    cog::packages::create -trans_task_ids $tasks_in_batch($package_name) -user_id $rest_user_id
                } else {
                    cog::packages::create -trans_task_ids $tasks_in_batch($package_name) -package_name $package_name -user_id $rest_user_id
                }
            }
        }

        return $new_task_ids
    }

    ad_proc -public best_price_id {
        -company_id:required
        -task_type_id:required
        { -subject_area_id "" }
        { -target_language_ids "" }
        { -source_language_id "" }
        { -currency "" }
        -uom_id:required
    } {
        Calculate the best rate for a company - defaults

        Default matching for source/target language:
            "de" <-> "de_DE" = + 1           
            "de_DE" <-> "de_DE" = +3 (same for de-DE)
            "es" <-> "de_DE" = -20


        @param company_id Company for which we are looking. If not provided, default to internal / freelancer_internal company
        @param task_type_id Project Type for which we want to calculate the price. This is also true for freelancers.
        @param subject_area_id Subject Area for which we try to get the price
        @param source_language_id Source Language which we look for. Relevancy assumes the "main" language to be in front with two characters, so does not match over category hierarchy.
        @param target_language_ids Target Languages which we look for. Will be matched as per source_language (e.g. de-CH will also look at de for a price, through with reduced matching)

        @return price_id of the best matched price for that company. Does not default to internal companies. Will return nothing if the relevancy is negative (e.g. wrong languages)
    } {
        foreach target_language_id $target_language_ids {
            set price_id [cog::price::best_match_price_id \
                -company_id $company_id \
                -material_uom_id $uom_id \
                -task_type_id $task_type_id \
                -subject_area_id $subject_area_id \
                -target_language_id $target_language_id \
                -source_language_id $source_language_id \
                -currency $currency]

            if {$price_id ne ""} { break }
        }

        if {$price_id eq ""} {
            # Check for internal / freelancer
            set provider_p [db_string provide_p "select 1 from im_companies where company_id = :company_id and company_type_id in ([ns_dbquotelist [im_sub_categories [im_company_type_provider]]]) limit 1" -default 0]
            if {$provider_p} {
                set internal_company_id [im_company_freelance] 
            } else {
                set internal_company_id [im_company_internal]
            }

            set price_id [cog::price::best_match_price_id -company_id $internal_company_id \
                -material_uom_id $uom_id \
                -task_type_id $task_type_id \
                -subject_area_id $subject_area_id \
                -target_language_id $target_language_id \
                -source_language_id $source_language_id \
                -currency $currency]
        }
        return $price_id
    }
}