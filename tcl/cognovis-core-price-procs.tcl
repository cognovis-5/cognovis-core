
ad_library {
    Procedures for supporting with prices
    @author malte.sussdorff@cognovis.de
}

namespace eval cog::price {
    ad_proc -public get_ids {
        -company_id:required
        -material_id:required
        {-price_status_id "3900"}
        {-complexity_type_id "4290" }
    } {
        Get a list of price_ids of a company for a material

        @param company_id Company for which we look for the price
        @param material_id Material for which we look
        @param price_status_id Price Status - defaults to active (to only get the active one)
    } {
        set where_clause_list [list]
        lappend where_clause_list "company_id = :company_id"
        lappend where_clause_list "material_id = :material_id"
        if {$price_status_id ne ""} {
            lappend where_clause_list "price_status_id = :price_status_id"
        }
        if {$complexity_type_id ne ""} {
            lappend where_clause_list "complexity_type_id = :complexity_type_id"
        }
        return [db_list price_ids "select price_id from im_prices where [join $where_clause_list " and "]"]
    }

    ad_proc -public create {
        -company_id:required
        -material_id:required
        -price:required
        {-min_price ""}
        {-price_type_id ""}
        {-price_status_id ""}
        {-valid_from ""}
        {-valid_through ""}
        {-note ""}
        {-currency ""}
        {-complexity_type_id ""}
        {-user_id ""}
    } {
        Creates a new price list entry for the company
    } {
        if {$material_id eq "" || $company_id eq "" || $price eq ""} {
            return ""
        }

        if {$valid_from eq ""} {set valid_from [db_string now "select now()::date from dual"]}
        if {$currency eq ""} {set currency [im_default_currency]}
        if {$user_id eq ""} {set user_id [auth::get_user_id]}
        if {$price_status_id eq ""} {set price_status_id [cog::price::status::active]}
        if {$price_type_id eq ""} {set price_type_id [cog::price::type::default]}
        if {$complexity_type_id eq ""} {set complexity_type_id [cog::price::complexity::default]}

        set price_id [db_string price "select price_id from im_prices where 
            price_type_id = :price_type_id and price_status_id = :price_status_id
            and material_id = :material_id and company_id = :company_id 
            and complexity_type_id = :complexity_type_id and valid_from::date = :valid_from" -default ""]
        if {$price_id eq ""} {
            set price_id [db_string price "select im_price__new(
                :price_type_id,:price_status_id,:material_id,:company_id,
                :complexity_type_id,:valid_from,:valid_through,:currency,
                :price,:min_price,:note,:user_id,'')"]
        }

        cog::callback::invoke cog::price::after_create -price_id $price_id -company_id $company_id -material_id $material_id

        return $price_id
    }

    ad_proc -public update {
        -price_id:required
        -price:required
        {-min_price ""}
        {-note ""}
        {-currency ""}
        {-valid_through ""}
        {-material_id ""}
        {-complexity_type_id ""}
        {-price_status_id ""}
        {-user_id ""}
    } {
        Updates the price of a material for a company.

        Will update the valid_from and valid through. Creates a copy of the "old" price upon the change.

        @param price_id Identifier of the price we want to update
        @param price Price per material (unit) 
        @param min_price Minimum price for that material
        @param note Note for this price (update) - defaults to the old note
        @param currency Currency of this price - defaults to the old currency
        @param valid_through How long is this price valid - defaults to the old valid_through
        @param material_id Material, just in case we want to ammend it.
        @param complexity_type_id Complexity of the work involved.
        @param price_status_id Used in case we want to reset the status of an existing price
    } {
        if {$user_id eq ""} {set user_id [auth::get_user_id]}

        # Check if we have a change at all
        set where_clause_list [list]
        lappend where_clause_list "price_id = :price_id"
        lappend where_clause_list "price = :price"
        if {$min_price ne ""} {
            lappend where_clause_list "min_price = :min_price"
        }
        if {$material_id ne ""} {
            lappend where_clause_list "material_id = :material_id"
        }
        if {$complexity_type_id ne ""} {
            lappend where_clause_list "complexity_type_id = :complexity_type_id"
        }
        if {$currency ne ""} {
            lappend where_clause_list "currency = :currency"
        }

        set changed_p [db_string changed "select 0 from im_prices where [join $where_clause_list " and "]" -default 1]

        if {$changed_p} {
            db_1row price_info "select complexity_type_id,currency, price_status_id, price_type_id, material_id, company_id, complexity_type_id, valid_through, currency,note from im_prices where price_id = :price_id" -column_array old_price
            db_dml update_old_price "update im_prices set price_status_id = [cog::price::status::inactive], valid_through = now() where price_id = :price_id"
            
            if {$valid_through eq ""} { set valid_through $old_price(valid_through)}
            if {$currency eq ""} { set currency $old_price(currency)}
            if {$note eq ""} { set note $old_price(note)}
            if {$complexity_type_id eq ""} { set complexity_type_id $old_price(complexity_type_id)}
            if {$material_id eq ""} { set material_id $old_price(material_id)}
            if {$price_status_id eq ""} { set price_status_id $old_price(price_status_id)}
            
            set old_price_id $price_id
            
            set price_id [cog::price::create -company_id $old_price(company_id) -material_id $material_id \
                -complexity_type_id $complexity_type_id -valid_through $valid_through \
                -currency $currency -note $note -price $price -min_price $min_price]
            
            permission::grant -party_id [im_profile_project_managers] -object_id $price_id -privilege "read"
            permission::grant -party_id [im_profile_project_managers] -object_id $price_id -privilege "write"

            db_dml update_object "update acs_objects set last_modified=now(), modifying_user=:user_id where object_id in (:old_price_id, :price_id)"
            cog::callback::invoke cog::price::after_update -old_price_id $old_price_id -price_id $price_id
        } else {
            set update_list [list]
            lappend update_list "valid_through = :valid_through"
            lappend update_list "note = :note"
            if {$price_status_id ne ""} {
                lappend update_list "price_status_id = :price_status_id"    
            } 
            db_dml update_price "update im_prices set [join $update_list " , "] where price_id = :price_id"
        }

        return $price_id
    }

    ad_proc -public export_price_list {
        -company_id:required
    } {
        Exports the current price list as a CSV

        @param company_id Company for whom we want to export the price list

        @return csv_path where to find the pricelist
    } {
        set csv [new_CkCsv]
        CkCsv_put_HasColumnNames $csv 1
        CkCsv_put_Delimiter $csv ";"
        CkCsv_put_Utf8 $csv 1

        set col_idx 0
        foreach var [list uom company_path task_type target_language source_language subject_area file_type complexity_type valid_from valid_through min_price price currency note] {
            CkCsv_SetColumnName $csv $col_idx $var
            incr col_idx
        }

        set row_idx 0
        db_foreach price {
            select im_category_from_id(m.material_uom_id) as uom, 
                c.company_path, im_category_from_id(m.task_type_id) as task_type, 
                im_category_from_id(m.target_language_id) as target_language, 
                im_category_from_id(m.source_language_id) as source_language, 
                im_category_from_id(m.subject_area_id) as subject_area, 
                im_category_from_id(m.file_type_id) as file_type, 
                im_category_from_id(p.complexity_type_id) as complexity_type, 
                p.valid_from, p.valid_through,
                replace(to_char(p.min_price, '99999.99'), '.', ',') as min_price, 
                replace(to_char(p.price, '99999.99'), '.', ',') as price, 
                p.currency, p.note 
            from im_prices p, im_materials m, im_companies c 
            where p.company_id = c.company_id 
                and p.material_id = m.material_id and p.company_id = :company_id
                and p.price_status_id = 3900 
            order by im_category_from_id(m.material_uom_id), c.company_path, 
            im_category_from_id(m.task_type_id), im_category_from_id(m.target_language_id), 
            im_category_from_id(m.source_language_id), im_category_from_id(m.subject_area_id), 
            im_category_from_id(m.file_type_id), im_category_from_id(p.complexity_type_id)
        } {
            set col_idx 0
            foreach var [list uom company_path task_type target_language source_language subject_area file_type complexity_type valid_from valid_through min_price price currency note] {
                set success [CkCsv_SetCell $csv $row_idx $col_idx [set $var]]
                incr col_idx
            }
            incr row_idx
        }
        
        set csv_file "[ns_mktemp].csv"
        
        set success [CkCsv_SaveFile $csv $csv_file]
        if {$success != 1} then {
            cog_log Error [CkCsv_lastErrorText $csv]
            delete_CkCsv $csv
            return ""
        } else {
            delete_CkCsv $csv
            return $csv_file
        }
    }

    ad_proc -public import_price_list {
        -csv_path:required
        -company_id:required
        {-user_id ""}
    } {
        Import the price list for a company

        @param csv_path path where to find the CSV file in the local file system
        @param company_id Company which we want to upload the prices into
        @param user_id User who is uploading the price list

        @return List of errors found
    } {
        if {$user_id eq ""} {set user_id [auth::get_user_id]}

        set errors [list]
        set csv [new_CkCsv]
        CkCsv_put_Utf8 $csv 1
        CkCsv_put_HasColumnNames $csv 1
        set success [CkCsv_LoadFile $csv "$csv_path"]
        if {$success != 1} then {
            lappend errors [CkCsv_lastErrorText $csv]
            delete_CkCsv $csv
            return $errors
        }

        set n [CkCsv_get_NumRows $csv]
        for {set row 0} {$row <= [expr $n -1]} {incr row} {

            # Preset values, defined by CSV sheet:
            foreach var [list uom company_path task_type target_language source_language subject_area file_type complexity_type valid_from valid_through min_price price currency note] {
                set $var [CkCsv_getCellByName $csv $row $var]
            }

            set errmsg ""
            if {![string equal "" $uom]} {
                set material_uom_id [db_string get_uom_id "select category_id from im_categories where category_type='Intranet UoM' and lower(category) = lower(:uom)" -default 0]
                if {$material_uom_id == 0} { append errmsg "<li>Didn't find UoM '$uom'\n" }
            } else {
                append errmsg "<li>Didn't find UoM '$uom'\n"
                set material_uom_id ""
            }

            set company_path [string trim $company_path]
            if {![string equal "" $company_path]} {
                set price_company_id [db_string get_company_id "select company_id from im_companies where lower(company_path) = lower(:company_path)" -default 0]
                if {$price_company_id == 0} {
                    set price_company_id [db_string get_company_id "select company_id from im_companies where lower(company_name) = lower(:company_path)" -default 0]
                }
                if {$price_company_id == 0} { append errmsg "<li>Didn't find Company '$company_path'\n" }
                if {$price_company_id != $company_id} {
                append errmsg "<li>Uploading prices for the wrong company ('$price_company_id' instead of '$company_id')"
                }
            }

            set task_type_id [db_string get_task_type "select category_id from im_categories where category_type='Intranet Project Type' and lower(category) = lower(trim(:task_type))"  -default ""]
            if {$task_type_id == "" && $task_type != ""} { 
                append errmsg "<li>Didn't find Task Type '$task_type'\n" 
            }

            set source_language_id [db_string get_source_language "select category_id from im_categories where category_type='Intranet Translation Language' and lower(category) = lower(trim(:source_language))"  -default ""]
            if {$source_language_id == "" && $source_language != ""} {
                append errmsg "<li>Didn't find Source Language '$source_language'\n"
            }

            set target_language_id [db_string target_langauge "select category_id from im_categories where category_type='Intranet Translation Language' and lower(category) = lower(trim(:target_language))"  -default ""]
            if {$target_language_id == "" && $target_language != ""} {
                append errmsg "<li>Didn't find Target Language '$target_language'\n"
            }


            set subject_area_id [db_string get_subject_area "select category_id from im_categories where category_type='Intranet Translation Subject Area' and lower(category) = lower(trim(:subject_area))"  -default ""]

            if {$subject_area_id == "" && $subject_area != ""} {
                append errmsg "<li>Didn't find Subject Area '$subject_area'\n"
            }

            set file_type_id [db_string get_file_type "select category_id from im_categories where category_type='Intranet Translation File Type' and lower(category) = lower(trim(:file_type))"  -default ""]
            if {$file_type_id == "" && $file_type != ""} {
                append errmsg "<li>Didn't find File Type '$file_type'\n"
            }

            set complexity_type_id [db_string get_complexity_type "select category_id from im_categories where category_type='Intranet Price Complexity' and lower(category) = lower(trim(:complexity_type))"  -default ""]
            if {$complexity_type_id == "" && $complexity_type != ""} {
                append errmsg "<li>Didn't find complexity Type '$complexity_type'\n"
            }

            # It doesn't matter whether prices are given in European "," or American "." decimals
            regsub {,} $price {.} price

            if {$errmsg ne ""} {
                lappend errors "Line #$row - $errmsg"
            } else {
                set material_id [cog::material::get_material_id -material_uom_id $material_uom_id -task_type_id $task_type_id \
        	    	-file_type_id $file_type_id -subject_area_id $subject_area_id \
		            -source_language_id $source_language_id -target_language_id $target_language_id]

                if {$material_id eq ""} {
                    set material_id [cog::material::create -material_uom_id $material_uom_id -task_type_id $task_type_id \
                    -file_type_id $file_type_id -subject_area_id $subject_area_id \
                    -source_language_id $source_language_id -target_language_id $target_language_id \
                    -description "Auto generated"]
                }
                set price_id [lindex [cog::price::get_ids -company_id $company_id -material_id $material_id -complexity_type_id $complexity_type_id] 0]

                regsub -all {,} $price {.} price
                regsub -all {,} $min_price {.} min_price
                if {$price_id eq ""} {
                    set price_id [cog::price::create -company_id $company_id -material_id $material_id \
		                -currency $currency -price $price \
		                -complexity_type_id $complexity_type_id -min_price $min_price -note $note]
                } else {
                    set price_id [cog::price::update -price_id $price_id -price $price -material_id $material_id \
                		-min_price $min_price -note $note -currency $currency -complexity_type_id $complexity_type_id]
                }

            }
        }
        return $errors
    }

    ad_proc -public nuke {
        -price_id:required
        -user_id:required
    } {
        Will purge a price from the database
        @param price_id Price we want to delete from the DB
    } {
        if {[acs_user::site_wide_admin_p -user_id $user_id]} {
            db_dml delete_price "delete from im_prices where price_id = :price_id"
            db_string delete_object "select acs_object__delete(:price_id) from dual"
        }
    }

    ad_proc -public rate {
        -company_id:required
        {-material_id ""}
        {-task_type_id ""}
        {-subject_area_id ""}
        {-target_language_id ""}
        {-source_language_id ""}
        {-file_type_id ""}
        {-complexity_type_id ""}
        {-currency "EUR"}
        {-material_uom_id ""}
        {-units ""}
        -ignore_min_price:boolean
    } {
        Calculate the best rate for a company

        @param task_type_id Type of work, needs to be provided if material_id is missing

        Supports looping up through the prices for parent categories
    } {
        if {$units eq ""} { set units 1 }
        
        if {$material_id eq ""} {
            if {$material_uom_id eq ""} {
                # We can't find the rate without task_type_id
                return ""
            }
            set material_id [cog::material::get_material_id -material_uom_id $material_uom_id -task_type_id $task_type_id \
                -file_type_id $file_type_id -subject_area_id $subject_area_id \
                -source_language_id $source_language_id -target_language_id $target_language_id]
        } else {
            set material_uom_id [cog::material::uom_id -material_id $material_id]
        }
        
        set price_id ""

        if {$material_id ne ""} {
            set price_id [lindex [cog::price::get_ids -company_id $company_id -material_id $material_id -complexity_type_id $complexity_type_id] 0]
        } 
        
        if {$price_id eq ""} {
            # do a price matching

            if {$material_uom_id eq ""} {
                # We can't find the rate without task_type_id
                return ""
            }

            set where_clause_list [list "p.company_id = :company_id" "p.material_id = m.material_id" "m.material_uom_id = :material_uom_id"]
            
            if {$source_language_id ne ""} {
                set source_language [string range [db_string category "select lower(category) from im_categories where category_id = :source_language_id"] 0 1]
                set source_language_ids [db_list categories "select category_id from im_categories 
                    where category_type = 'Intranet Translation Language'
                    and lower(category) like '${source_language}%'"]
                lappend where_clause_list "(m.source_language_id in ([ns_dbquotelist $source_language_ids]) or m.source_language_id is null)"
            }

            if {$target_language_id ne ""} {
                set target_language [string range [db_string category "select lower(category) from im_categories where category_id = :target_language_id"] 0 1]
                set target_language_ids [db_list categories "select category_id from im_categories 
                    where category_type = 'Intranet Translation Language'
                    and lower(category) like '${target_language}%'"]
                lappend where_clause_list "(m.target_language_id in ([ns_dbquotelist $target_language_ids]) or m.source_language_id is null)"
            }

            if {$currency ne ""} {
                lappend where_clause_list "currency = :currency"
            }

            if {$task_type_id ne ""} {
                lappend where_clause_list "task_type_id = :task_type_id"
            }

            # First check if we have actually prices for that customer
            set customer_pricing_exists_p [db_string trans_prices_p "select 1 from im_prices p, im_materials m
                where [join $where_clause_list " and "]
                limit 1" -default 0]

            if {!$customer_pricing_exists_p} {
                set company_type_id [db_string company_type "select company_type_id from im_companies where company_id = :company_id" -default ""]
                if {[lsearch [im_sub_categories [im_company_type_provider]] $company_type_id]<0} {
                    set company_id [im_company_internal]
                } else {
                    set company_id [im_company_freelance]
                }
            }

            set price_id [cog::price::best_match_price_id \
                -company_id $company_id \
                -task_type_id $task_type_id \
                -subject_area_id $subject_area_id \
                -target_language_id $target_language_id \
                -source_language_id $source_language_id \
                -currency $currency \
                -material_uom_id $material_uom_id \
                -file_type_id $file_type_id \
                -complexity_type_id $complexity_type_id]
        }

        if {$price_id ne ""} {
            set number_format "9999990.099"
            db_1row price_info "select
                p.price,
                p.min_price,
                m.material_uom_id          
            from im_prices p, im_materials m
            where price_id = :price_id
            and p.material_id = m.material_id"

            regsub -all {,} $units {.} units

            if {!$ignore_min_price_p} {
                if {[expr $price * $units] < $min_price} {
                    set units 1
                    set material_uom_id [im_uom_min_price]
                    set price $min_price
                }
            }
            return [list $units $material_uom_id $price]
        } else {
            return ""
        }
    }
 
    ad_proc -public best_match_price_id {
        -company_id:required
        -material_uom_id:required
        {-task_type_id ""}
        {-subject_area_id ""}
        {-target_language_id ""}
        {-source_language_id ""}
        {-file_type_id ""}
        {-complexity_type_id ""}
        {-currency ""}
    } {
        Calculate the best match price for a customer.
        Complicated undertaking, because the price depends on a number of variables, depending on client etc. As a solution, we act like a search engine, return all prices and rank them according to relevancy. We take only the first (=highest rank) line for the actual price proposal.
    } {
        set number_format "9999990.099"
        set where_clause_list [list]
        lappend where_clause_list "m.material_uom_id = :material_uom_id"
        lappend where_clause_list "p.material_id = m.material_id"
        lappend where_clause_list "p.price_status_id = [cog::price::status::active]"
        if {$currency ne ""} {
            lappend where_clause_list "p.currency = :currency"
        }
        if {$file_type_id eq ""} {
            set file_type_id 0
        }
        
        lappend where_clause_list "p.company_id = :company_id"

        set references_prices_sql "
            select
                p.price_id
            from
                (
                    (select
                        im_prices_calc_relevancy (
                            m.task_type_id, :task_type_id,
                            m.subject_area_id, :subject_area_id,
                            m.target_language_id, :target_language_id,
                            m.source_language_id, :source_language_id,
                            m.file_type_id, :file_type_id,
                            p.complexity_type_id, :complexity_type_id
                        ) as relevancy,
                        p.price_id
                    from im_prices p, im_materials m
                    where [join $where_clause_list " and "]
                    )
                ) p
            where
                relevancy>1
            order by
                p.relevancy desc
            limit 1
        "
        
        if {[db_0or1row reference_prices $references_prices_sql]} {
            return $price_id
        } else {

            # Still no file found, try with the subject_area
            set parent_subject_area_id [lindex [im_category_parents $subject_area_id] 0]
            if {$parent_subject_area_id ne ""} {
                set price_id [cog::price::best_match_price_id \
                    -company_id $company_id \
                    -task_type_id $task_type_id \
                    -subject_area_id $parent_subject_area_id \
                    -target_language_id $target_language_id \
                    -source_language_id $source_language_id \
                    -currency $currency \
                    -material_uom_id $material_uom_id \
                    -file_type_id $file_type_id \
                    -complexity_type_id $complexity_type_id]
            } else {
                set price_id ""
            }
            return $price_id
        }
    }
    
}

namespace eval cog::price::status {
    ad_proc -public active {} { Returns the active status } { return 3900 }
    ad_proc -public inactive {} { Returns the inactive status } { return 3902 }
}

namespace eval cog::price::type {
    ad_proc -public default {} {} { return 3910 }
    ad_proc -public timesheet {} {} { return 3911 }
    ad_proc -public translation {} {} { return 3912 }
}

namespace eval cog::price::complexity {
    ad_proc -public default {} {} { return 4290}
    ad_proc -public premium {} {} { return 4291}
}