ad_library {
    cognovis specific procedures for users
    
    @author malte.sussdorff@cognovis.de
}

namespace eval cog::user {
    ad_proc -public main_company_id {
        -user_id:required
    } {
        Return the main company id for given user_id. This should return single id or empty string 

        @param user_id User for whom we search the company
        @return company_id ID of the company
        
    } {
        set company_id [db_string company_id_sql "select company_id from im_companies where primary_contact_id =:user_id limit 1" -default ""]
        if {$company_id eq ""} {
            set company_id [db_string company_id_sql "select company_id from im_companies where accounting_contact_id =:user_id limit 1" -default ""]
        }
        if {$company_id eq ""} {
            set company_id [db_string company_id_sql "select object_id_one as company_id from acs_rels where object_id_two =:user_id and rel_type = 'im_company_employee_rel' limit 1" -default ""]
        }
        return $company_id
    }

    ad_proc -public nuke {
        -user_id:required
        -current_user_id:required
    } {
        Remove a user completely from the system (if possible)

        @param user_id User whom we want to remove
        @param current_user_id Who is doing the removing
    } {
        # Check for permissions
        im_user_permissions $current_user_id $user_id view read write admin
        if {!$admin} { return "User #$current_user_id isn't a system administrator" }

        # You can't delete an adminstrator
        set user_is_admin_p [im_is_user_site_wide_or_intranet_admin $user_id]
        if {$user_is_admin_p} {
	        return "User is an administrator - you can't nuke an administrator"
        }

        cog::callback::invoke -object_id $user_id -action before_nuke

        cog::fs::nuke -object_id $user_id
        cog::rel::remove -object_id $user_id -current_user_id $current_user_id
        cog::mail::nuke_log -object_id $user_id -current_user_id $current_user_id

        # Permissions
        db_dml perms "delete from acs_permissions where grantee_id = :user_id"
        db_dml perms "delete from acs_permissions where object_id = :user_id"

        set default_user [db_string default_user "
            select	min(person_id)
            from	persons
            where	person_id > 0
        "]

		# Reassign objects to a default user...
    	db_dml reassign_objects "update acs_objects set modifying_user = :default_user where modifying_user = :user_id"
	    db_dml reassign_projects "update acs_objects set creation_user = :default_user where creation_user = :user_id"
        if {[im_table_exists im_invoices]} {
            db_dml invoice_references "update im_invoices set company_contact_id = null where company_contact_id = :user_id"
        }
        db_dml cuase_objects "update im_costs set cause_object_id = :default_user where cause_object_id = :user_id"
        db_dml cost_providers "update im_costs set provider_id = :default_user where provider_id = :user_id"
        db_dml forum "update im_forum_topics set owner_id = :default_user where owner_id = :user_id"

        if {[im_table_exists im_hours]} {
            foreach hour_id [db_list delete_hours "select hour_id from im_hours where user_id = :user_id"] {
                cog::timesheet::entry::nuke -hour_id $hour_id -current_user_id $current_user_id
            }
        }

        set absence_ids [db_list old_absences "select absence_id from im_user_absences where owner_id = :user_id"] 
        foreach absence_id $absence_ids {
            cog::absence::nuke -absence_id $absence_id -current_user_id $user_id
        }

        if {[im_table_exists survsimp_responses]} {
            db_dml survsimp_responses_context "update survsimp_responses set related_context_id = :default_user where related_context_id = :user_id"
            db_dml survsimp_responses_rel "update survsimp_responses set related_object_id = :default_user where related_object_id = :user_id"
        }

        db_1row del_person "select person__delete(:user_id)"
        return ""
    }
}   