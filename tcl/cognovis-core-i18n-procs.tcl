
ad_library {
    Procedures for I18n 
    @author malte.sussdorff@cognovis.de
}

namespace eval cog::message {

    ad_proc -public lookup {
        { -locale ""}
        { -default ""}
        -user_id
        -key:required
    } {
        Looks up a message in the locale for a key. Will register the default if provided and key could not be found in the en_US locale

        @param key message key to look for
        @param locale locale we look for
        @param user_id User for whom we look this up
        @param default Default value to register in case the message is unknown
    } {
        if {$locale eq ""} {
            if {[info exists user_id]} {
                set locale [lang::user::locale -user_id $user_id]
            } else {
                set locale "en_US"
            }
        }
        
        if {$default ne ""} {
            # Check if the message exists
            if {![lang::message::message_exists_p en_US $key]} {
                set key_list [split $key "."]
                lang::message::register en_US [lindex $key_list 0] [lindex $key_list 1] $default
            }
        }
        
        return [lang::message::lookup $locale $key]
    }
}