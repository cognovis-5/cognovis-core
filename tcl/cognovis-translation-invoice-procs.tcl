ad_library {
    Procedures to handle translation projects
    @author malte.sussdorff@cognovis.de
}


namespace eval cog::trans::invoice {
    ad_proc -public create_from_tasks {
        {-cost_type_id ""}
        {-task_ids ""}
        {-cost_center_id ""}
        {-current_user_id ""}
        {-project_id ""}
    } {
        Create a quote / invoice from the tasks. This is CUSTOMER Facing and uses Customer Pricing.
    
        @param project_id Project ID for which to create the translation invoice
        @param task_ids List of TaskIDs which will be used to limit the invoice to only include these tasks. Helpful if you want to create a partial invoice
        @param cost_center_id Cost center to use
        @param cost_type_id Cost Type for the invoice. Defaults to quote, but could also be an actual customer invoice.

        @return invoice_id ID of the invoice we created.
    } {

        if {$cost_type_id eq ""} {set cost_type_id [im_cost_type_quote]}

        if {$project_id eq ""} {
            set first_task_id [lindex $task_ids 0]
            set project_id [db_string project_id_from_task_id "select project_id from im_trans_tasks where task_id =:first_task_id limit 1" -default ""]
        } 

        if {$project_id eq ""} {
            return
            ad_script_abort
        }

        # Get info about the project
        db_1row project_info "select company_contact_id,project_lead_id,company_id,subject_area_id,project_cost_center_id,complexity_type_id from im_projects where project_id = :project_id"
        if {$project_lead_id eq ""} {
            set project_lead_id $current_user_id
        }

        set invoice_id [cog::invoice::create -provider_contact_id $project_lead_id -provider_id [im_company_internal] -customer_contact_id $company_contact_id \
            -invoice_type_id $cost_type_id -project_id $project_id -cost_center_id $cost_center_id -customer_id $company_id]

        if {$invoice_id ne ""} {
            if {$task_ids ne ""} {
                cog::trans::invoice_items::create_from_tasks -invoice_id $invoice_id -task_ids $task_ids -current_user_id $current_user_id
            }

            cog::callback::invoke -object_type "im_invoice" -object_id $invoice_id -action after_create -status_id [im_cost_status_created] -type_id $cost_type_id
        }
        return $invoice_id
    }

    ad_proc -public update_from_tasks {
        -invoice_id:required
        -task_ids
        -cost_center_id
        {-current_user_id ""}
    } {
        @param invoice_id Invoice ID we want to UPDATE. In this case the line items will be removed and new line items generated for the tasks in the project
        @return invoice_id
    } {
        if {[info exists cost_center_id]} {
            db_dml update_cost_center "update im_costs set cost_center_id = :cost_center_id where cost_id = :invoice_id"
        }


        if {$task_ids ne ""} {
            # invoice exists, remove all line items
            db_dml delete_invoice_items "
                    DELETE from im_invoice_items
                    WHERE invoice_id=:invoice_id
			"
            cog::trans::invoice_items::create_from_tasks -invoice_id $invoice_id -task_ids $task_ids -current_user_id $current_user_id
        }

		db_1row invoice_info "select cost_type_id, cost_status_id from im_costs where cost_id = :invoice_id"

        cog::callback::invoke -object_type "im_invoice" -object_id $invoice_id -action after_update -status_id $cost_status_id -type_id $cost_type_id

        return $invoice_id
    }
}

namespace eval cog::trans::invoice_items {
    ad_proc -public create_from_tasks {
        -invoice_id:required
        -task_ids:required
        {-current_user_id ""}
    } {
        Create invoice items in the invoice from tasks

        @param invoice_id ID of the invoice we want to add the tasks to
        @param task_ids IDs of trans tasks to be turned into intoive_items
    } {
        set sort_order 1
        set currency [im_parameter -package_id [im_package_cost_id] "DefaultCurrency" "" "EUR"]
        if {$current_user_id eq ""} { set current_user_id [auth::get_user_id]}
        #---------------------------------------------------------------
        # First take a look at minimum price
        #---------------------------------------------------------------
        
        # Check if we have languages with the minimum rate.
        set min_price_languages [list]
        db_1row sql "select c.project_id,customer_id, subject_area_id, complexity_type_id 
            from im_costs c, im_projects p
            where c.project_id = p.project_id
            and cost_id = :invoice_id"

        set min_price_task_sql "
            select sum(billable_units) as sum_tasks, array_to_string(array_agg(task_name), ',') as task_name_agg, 
            target_language_id, task_uom_id, task_type_id, source_language_id, im_file_type_from_trans_task(task_id) as file_type_id 
            from im_trans_tasks 
            where task_id in ([ns_dbquotelist $task_ids])
            and billable_units >0"

            
        # Append the grouping
        append min_price_task_sql "\n group by target_language_id, task_uom_id, task_type_id, source_language_id, file_type_id"

        db_foreach tasks $min_price_task_sql {
            set rate_info [cog::price::rate \
                -company_id $customer_id \
                -task_type_id $task_type_id \
                -subject_area_id $subject_area_id \
                -target_language_id $target_language_id \
                -source_language_id $source_language_id \
                -complexity_type_id $complexity_type_id \
                -currency $currency \
                -material_uom_id $task_uom_id \
                -units $sum_tasks]

            # Split the rate information up into the components
            set billable_units [lindex $rate_info 0]
            set rate_uom_id [lindex $rate_info 1]
            set rate [lindex $rate_info 2]

            if {$billable_units eq 1 && [im_uom_min_price] eq $rate_uom_id && $rate ne ""} {
                # We found a language where the tasks together are below the
                # Minimum Price.
                lappend min_price_languages $target_language_id
                set material_id [cog::material::get_material_id -target_language_id $target_language_id -source_language_id $source_language_id -file_type_id $file_type_id -task_type_id $task_type_id -material_uom_id $task_uom_id]
                if {$material_id eq ""} {				
                    set material_id [cog::material::create -source_language_id $source_language_id -target_language_id $target_language_id -file_type_id $file_type_id -task_type_id $task_type_id -material_uom_id $task_uom_id] 
                }
                
                # Insert the min price
                
                set task_list [split $task_name_agg ","]
                if {[llength $task_list] >1} {
                    set task_name " [join $task_list "<br>"]"
                } else {
                    set task_name [lindex $task_list 0]
                }

                set newly_created_invoice_item_id [cog::invoice_item::create -invoice_id $invoice_id \
                    -item_name $task_name -item_material_id $material_id \
                    -item_units $billable_units -price_per_unit $rate \
                    -project_id $project_id -sort_order $sort_order \
                    -user_id $current_user_id -item_uom_id $rate_uom_id -currency $currency \
                    -item_type_id $task_type_id -item_status_id [im_invoice_item_status_active]]

                incr sort_order
            }
        }
        
        #---------------------------------------------------------------
        # Now work with normal prices
        #---------------------------------------------------------------

        set task_sql "select task_type_id,
            target_language_id,
            source_language_id,
            task_uom_id,
            task_name,
            billable_units,
            task_id,
            im_file_type_from_trans_task(task_id) as file_type_id
        from im_trans_tasks where task_id in ([ns_dbquotelist $task_ids]) and billable_units >0"
        
        # Order by filename and language
        append task_sql " order by task_name, im_name_from_id(target_language_id)"

        db_foreach tasks $task_sql {
        
            if {[lsearch $min_price_languages $target_language_id]<0 } {
                set material_id [cog::material::get_material_id -target_language_id $target_language_id -source_language_id $source_language_id -file_type_id $file_type_id -task_type_id $task_type_id -material_uom_id $task_uom_id]
                if {$material_id eq ""} {
                    set material_id [cog::material::create -material_uom_id $task_uom_id -task_type_id $task_type_id \
                        -source_language_id $source_language_id -target_language_id $target_language_id \
                        -description "Auto generated for customer [im_name_from_id $customer_id]."]
                }
                
                # Ignore the minimum rate
                set rate_info [cog::price::rate \
                        -company_id $customer_id \
                        -task_type_id $task_type_id \
                        -subject_area_id $subject_area_id \
                        -target_language_id $target_language_id \
                        -source_language_id $source_language_id \
                        -currency $currency \
                        -material_uom_id $task_uom_id \
                        -file_type_id $file_type_id \
                        -complexity_type_id $complexity_type_id \
                        -units $billable_units \
                        -ignore_min_price]
                    
                # Split the rate information up into the components
                if {$rate_info ne ""} {
                    set billable_units [lindex $rate_info 0]
                    set task_uom_id [lindex $rate_info 1]
                    set rate [lindex $rate_info 2]
                }

                set newly_created_invoice_item_id [cog::invoice_item::create -invoice_id $invoice_id \
                    -item_name $task_name -item_material_id $material_id \
                    -item_units $billable_units -price_per_unit $rate \
                    -project_id $project_id -sort_order $sort_order -task_id $task_id \
                    -user_id $current_user_id -item_uom_id $task_uom_id -currency $currency \
                    -item_type_id $task_type_id -item_status_id [im_invoice_item_status_active]]
                
                incr sort_order
            }
        }
        
        cog::callback::invoke -object_type "im_invoice" -object_id $invoice_id -action after_update

        db_release_unused_handles
    }
}