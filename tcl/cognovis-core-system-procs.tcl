ad_library {
    cognovis specific procedures for the whole system
    
    @author malte.sussdorff@cognovis.de
}

ad_proc -public cog::packages_to_upgrade {
	{ -package_key ""}
} {
	Returns a list of package_keys which can be upgraded

	If we provide a package_key we only try to upgrade that one
} {

	#
	# Retrieve all spec files
	#
	set packages_spec_files     [apm_scan_packages "$::acs::rootdir/packages"]
	set workspace_spec_files    [apm_scan_packages [apm_workspace_install_dir]]
	set workspace_filenames     [list]
	foreach spec_path $workspace_spec_files {
		lappend workspace_filenames [file tail $spec_path]
	}
	set all_spec_files $workspace_spec_files
	foreach spec_path $packages_spec_files {
		set spec_filename [file tail $spec_path]
		if {$spec_filename ni $workspace_filenames} {
			lappend all_spec_files $spec_path
		}
	}

	#
	# Parse the files and make a list of available packages to upgrade
	#
	set packages_to_upgrade [list]
	foreach spec_file $all_spec_files {
		array set version    [apm_read_package_info_file $spec_file]
		set this_version     $version(name)
		set this_package_key $version(package.key)
		#
		# Filter by package_key, if passed as an argument, and check for upgrades
		#
		if {($package_key eq "" || $package_key eq $this_package_key) &&
			[apm_package_supports_rdbms_p -package_key $this_package_key] &&
			[apm_package_registered_p $this_package_key] &&
			[apm_package_installed_p $this_package_key] &&
			[apm_higher_version_installed_p $this_package_key $this_version] eq 1
		} {
			#
			# Add the package to the list
			#
			lappend packages_to_upgrade $this_package_key
		}
	}

	return $packages_to_upgrade
}


ad_proc -public cog::run_update_scripts {
    {-package_key ""}
    -ignore_db_errors:boolean
} {
    Runs the upgrade scripts which havent run yet for a package

    @param package_key Package we want to run this for, defaults to all
    @return Returns a list of upgrade scripts that were executed successfully.
            In case of errors, the list is empty.
} {

    # Check if there are any potential errors 
    if {$ignore_db_errors_p} {
        cog_log Warning "cog::run_update_scripts: -ignore_db_errors is set to 1: Errors during DB upgrade will be ignored"
    }

    # Get the list of upgrade scripts in the FS
    set data_model_files [list]

    set where_clause_list [list "enabled_p = 't'"]
    if {$package_key eq ""} {
        # Only run with installed packages which follows the PO upgrade file name convention
        # Prevents ACS Packages from upgrading.

        set missing_modules [list]
        set core_dir "[acs_root_dir]/packages"

        set package_sql "
            select distinct
                    package_key
            from    apm_package_versions
            where   enabled_p = 't'
        "

        db_foreach packages $package_sql {
            set core_upgrade_dir "$core_dir/$package_key/sql/postgresql/upgrade"
            foreach dir [lsort [glob -type f -nocomplain "$core_upgrade_dir/upgrade-?.?.?.?.?-?.?.?.?.?.sql"]] {
                # Skip upgrade scripts from 3.0.x
                if {[regexp {upgrade-3\.0.*\.sql} $dir match path]} { continue }

                # Add the "/packages/..." part to hash-array for fast comparison.
                if {[regexp {(/packages.*)} $dir match path]} {
                    lappend data_model_files $path
                }
            }
        }

    } else {

        set package_sql "
            select distinct
                    package_key
            from    apm_package_versions
            where   enabled_p = 't'
            and     package_key = '$package_key'
        "

        set package_keys [db_list package_keys $package_sql]

        foreach package_key $package_keys {

            #
            # As we may have new packages included by the dependency check,
            # determine if we are upgrading or installing.
            #
            set spec_file       [apm_package_info_file_path $package_key]
            array set version   [apm_read_package_info_file $spec_file]
            set new_version     $version(name)
            set package_name $version(package-name)

            # Check on the installed version vs. new install (empty installed)
            if { [apm_package_upgrade_p $package_key $new_version] == 1} {
                set installed_version [apm_highest_version_name $package_key]
            } else {
                set installed_version ""
            }

            #
            # Select SQL scripts based of APM version
            #
            set apm_data_model_files [apm_data_model_scripts_find \
                                    -upgrade_from_version_name $installed_version \
                                    -upgrade_to_version_name $new_version \
                                    $package_key]
            foreach apm_data_model_file $apm_data_model_files {
                set data_model_file_name [lindex $apm_data_model_file 0]
                set log_key "/packages/$package_key/$data_model_file_name"
                if {![info exists db_files($log_key)]} {
                    # Log Key is also the filepath of  the SQL file we want to execute.
                    lappend data_model_files $log_key
                }
            }

            # 
            # Select SQL scripts in the FS based of Project-open Logic
            set core_upgrade_dir "[acs_root_dir]/packages/$package_key/sql/postgresql/upgrade"
            foreach sql_file [lsort [glob -type f -nocomplain "$core_upgrade_dir/upgrade-?.?.?.?.?-?.?.?.?.?.sql"]] {
                # Skip upgrade scripts from 3.0.x
                if {[regexp {upgrade-3\.0.*\.sql} $sql_file match path]} { continue }

                # Add the "/packages/..." part to hash-array for fast comparison.
                if {[regexp {(/packages.*)} $sql_file match path]} {
                    lappend data_model_files $path
                }
            }
        }
    }

    # --------------------------------------------------------------
    # Get the upgrade scripts that were executed
    set sql "
        select  distinct l.log_key
        from    acs_logs l
        order by log_key
    "
    db_foreach db_files $sql {
        # Add the "/packages/..." part to hash-array for fast comparison.
        if {[regexp {(/packages.*)} $log_key match path]} {
            set db_files($path) $path
        }
    }

    # --------------------------------------------------------------
    # Check if there are scripts that weren't executed:
    set upgrade_list [list]
    foreach file $data_model_files {
        if {![info exists db_files($file)]} {
            cog_log Notice "Running SQL upgrade script: [acs_root_dir]/$file"
            if {[catch {db_source_sql_file -callback apm_dummy_callback "[acs_root_dir]/$file"} err]} {
                cog_log Error "cog::update_package : Error loading file: $err"

                # Remove the entry from acs_logs
                db_dml delete_from_log "delete from acs_logs where log_key = '$file'"

                if {!$ignore_db_errors_p} {
                    # Abort the execution and return the list of files that were successfully executed
                    return $upgrade_list
                }
            } else {
                cog_log Notice "Database upgrade $file successful"
            }
            im_exec_dml log "acs_log__debug(:file,'')"
            lappend upgrade_list $file
        }
    }
    return $upgrade_list
}

ad_proc -public cog::install_package {
    -package_key:required
    -ignore_create_sql:boolean
} {
    Install a package
    
    @param package_key The package to install
    @return Returns the version_id if the package was installed successfully, otherwise 0.
} {

    set version_id 0
    set callback "apm_dummy_callback"
    set spec_file_path [apm_package_info_file_path $package_key]
    array set version [apm_read_package_info_file $spec_file_path]
    set version_name $version(name)

    set operations {Installing Installed}
    
    cog_log Notice "<h3>[lindex $operations 0] $version(package-name) $version(name)</h3>"

    set package_uri $version(package.url)
    set package_type $version(package.type)
    set package_name $version(package-name)
    set pretty_plural $version(pretty-plural)
    set initial_install_p $version(initial-install-p)
    set singleton_p $version(singleton-p)
    set implements_subsite_p $version(implements-subsite-p)
    set inherit_templates_p $version(inherit-templates-p)
    set auto_mount $version(auto-mount)
    set version_uri $version(url)
    set summary $version(summary)
    set description_format $version(description.format)
    set description $version(description)
    set release_date $version(release-date)
    set vendor $version(vendor)
    set vendor_uri $version(vendor.url)
    set split_path [split $spec_file_path /]
    set relative_path [join [lreplace $split_path 0 [lsearch -exact $package_key $split_path]] /] 

    # Register the package if it is not already registered.
    if { ![apm_package_registered_p $package_key] } {
        apm_package_register  -spec_file_path $relative_path  $package_key  $package_name  $pretty_plural  $package_uri  $package_type  $initial_install_p  $singleton_p  $implements_subsite_p  $inherit_templates_p
    }

    # Source Tcl procs and queries to be able
    # to invoke any Tcl callbacks after mounting and instantiation. Note that this reloading 
    # is only done in the Tcl interpreter of this particular request.
    # Note that acs-tcl is a special case as its procs are always sourced on startup from boostrap.tcl
    apm_load_libraries -procs -force_reload -packages $package_key
    apm_load_queries -packages $package_key

    # Get the callbacks in an array, since we can't rely on the 
    # before-upgrade being in the db (since it might have changed)
    # and the before-install definitely won't be there since 
    # it's not added until later here.

    array set callbacks $version(callbacks)

    # Run before-install
    if {[info exists callbacks(before-install)]} {
        apm_invoke_callback_proc  -proc_name $callbacks(before-install)  -version_id $version_id  -type before-install
    }

    # Install the data model
    if {!$ignore_create_sql_p} {
        apm_package_install_data_model -data_model_files 0 $spec_file_path
    }
    

    cog::run_update_scripts -package_key $package_key -ignore_db_errors

    # We are installing a new package

    set version_id [apm_package_install_version  -callback $callback  -array version  $package_key $version_name  $version_uri $summary $description $description_format $vendor $vendor_uri $auto_mount $release_date]

    if { !$version_id } {
        # There was an error.
        cog_log Error "apm_package_install: Package $package_key could not be installed. Received version_id $version_id"
    }

    apm_load_catalog_files $package_key
    apm_package_install_dependencies -callback $callback  $version(embeds) $version(extends) $version(provides) $version(requires) $version_id
    apm_build_one_package_relationships $package_key
    apm_copy_inherited_params $package_key [concat $version(embeds) $version(extends)]
        
    # Install the parameters for the version.
    apm_package_install_parameters -callback $callback $version(parameters) $package_key

    # Update all other package information.
    apm_package_install_owners -callback $callback $version(owners) $version_id
    apm_package_install_callbacks -callback $callback $version(callbacks) $version_id
    apm_build_subsite_packages_list
        
    cog_log Notice "<p>[lindex $operations 1] $version(package-name), version $version(name).</p>"

    nsv_set apm_enabled_package $package_key 1    

    apm_version_enable -callback $callback $version_id

    # After install Tcl proc callback
    apm_invoke_callback_proc -version_id $version_id -type after-install

    set priority_mount_path [ad_decode $version(auto-mount) "" $mount_path $version(auto-mount)]
    if { $priority_mount_path ne "" } {
        # This is a package that should be auto mounted

        set parent_id [site_node::get_node_id -url "/"]

        if { [catch {
            db_transaction {            
                set node_id [site_node::new -name $priority_mount_path -parent_id $parent_id]
            }
        } error] } {
            # There is already a node with that path, check if there is a package mounted there
            array set node [site_node::get -url "/${priority_mount_path}"]
            if { $node(object_id) eq "" } {
                # There is no package mounted there so go ahead and mount the new package
                set node_id $node(node_id)
            } else {
                # Don't unmount already mounted packages
                set node_id ""
            }
        }

        if { $node_id ne "" } {

            site_node::instantiate_and_mount  -node_id $node_id  -node_name $priority_mount_path  -package_name $version(package-name)  -package_key $package_key

            cog_log Notice "<p> Mounted an instance of the package at /${priority_mount_path} </p>"
        } {
            # Another package is mounted at the path so we cannot mount
            set error_text "Package $version(package-name) could not be mounted at /$version(auto-mount) , there may already be a package mounted there, the error is: $error"
            cog_log Error "apm_package_install: $error_text \n\n$::errorInfo"
        } 
    }

    # Flush the installed_p cache
    util_memoize_flush [list apm_package_installed_p_not_cached $package_key]
    return $version_id
}

ad_proc -public cog::update_package {
    -package_key:required
    -ignore_db_errors:boolean
    -ignore_dependencies:boolean
} {
    Update a package to the latest version

    @param package_key The package to upgrade
    @param ignore_db_errors Whether to ignore db upgrade errors
    @param ignore_dependencies Whether to ignore dependencies during the upgrade
    
    @return Returns a list of packages that were upgraded.
} {
    set packages_upgraded [list]

    if {!$ignore_dependencies_p} {
        # Are there packages to upgrade?
        set packages_to_upgrade [cog::packages_to_upgrade -package_key $package_key]
        
        # Dependency check
        apm_get_package_repository -array repository
        apm_get_installed_versions -array installed

        array set result [apm_dependency_check_new \
                            -repository_array repository \
                            -package_keys $packages_to_upgrade]

        set packages_upgraded [list]

        if {$result(status) eq "ok"} {
            set package_keys $result(install)
        } else {
            set package_keys [list]]
        }
    } else {
        set package_keys [list $package_key]
    }

    #
    # Do the upgrade
    #
    foreach package_key $package_keys {
        #
        # As we may have new packages included by the dependency check,
        # determine if we are upgrading or installing.
        #
        set spec_file       [apm_package_info_file_path $package_key]

        # Support to ignore errors in the data model files
        #
        # When ignore_errors_p is set to 1 we ignore any errors that
        # occur during the execution of the data model files.
        #
        if {$ignore_db_errors_p} {
            cog::run_update_scripts -package_key $package_key -ignore_db_errors
            
            # Upgrade the package
            if {[catch {
                set version_id [apm_package_install \
                        -enable=1 \
                        $spec_file]
            } errorMsg]} {
                cog_log Error "cog::update_package : $package_key $errorMsg\n [ns_quotehtml $::errorInfo]"
            } else {
                lappend packages_upgraded $package_key
            }
        } else {
            array set version   [apm_read_package_info_file $spec_file]
            set new_version     $version(name)
            set package_name $version(package-name)

            # Check on the installed version vs. new install (empty installed)
            if { [apm_package_upgrade_p $package_key $new_version] == 1} {
                set installed_version [apm_highest_version_name $package_key]
            } else {
                set installed_version ""
            }

            #
            # Select SQL scripts
            #
            set data_model_files [apm_data_model_scripts_find \
                                    -upgrade_from_version_name $installed_version \
                                    -upgrade_to_version_name $new_version \
                                    $package_key]
            # Upgrade the package
            if {[catch {
                set version_id [apm_package_install \
                        -enable=1 \
                        -load_data_model \
                        -data_model_files $data_model_files \
                        $spec_file]

            } errorMsg]} {
                cog_log Error "cog::update_package : $package_key $errorMsg\n [ns_quotehtml $::errorInfo]"
            } else {
                lappend packages_upgraded $package_key
            }
        }
    }
    return $packages_upgraded
}

ad_proc -public cog::reduce_data {
    { -interval "3 months"}
} {
    Deletes all project and invoice data which is older than the interval and any users / companies which are no longer needed because of that
} {

    set user_id [auth::get_user_id]
    if {![acs_user::site_wide_admin_p -user_id $user_id]} {
        return ""
    }

    db_dml disable_trigger "alter table im_costs disable trigger im_costs_project_cache_del_tr"

    # Delete all projects where the start date is less then three months ago, along with all associated data
    set project_ids [db_list old_projects "select project_id from im_projects where start_date < now()-interval :interval or start_date is null order by project_nr"]
    cog_log Notice "Deleting [llength $project_ids] projects"
    foreach project_id $project_ids {
	cog_log Notice "Deleting [im_name_from_id $project_id]"
        cog_rest::delete::trans_project -project_id $project_id -rest_user_id $user_id
    }

    # Delete all cost items without project connection
    set cost_ids [db_list old_costs "select cost_id from im_costs where effective_date < now() -interval :interval order by cost_name"]
    cog_log Notice "Deleting [llength $cost_ids] cost items"
    foreach cost_id $cost_ids {
	cog_log Notice "Nuking cost [im_name_from_id $cost_id]"
        cog::cost::nuke -cost_id $cost_id
    }

    
    set absence_ids [db_list old_absences "select absence_id from im_user_absences where start_date < now() -interval :interval"] 
    cog_log Notice "Nuking Absences [llength $absence_ids] absences"
    foreach absence_id $absence_ids {
        cog::absence::nuke -absence_id $absence_id -current_user_id $user_id
    }

    set internal_company_id [im_company_internal]
    set freelance_company_id [im_company_freelance]

    set unused_company_ids [db_list companies "select company_id from im_companies where company_id not in (select distinct company_id from 
        (
            select provider_id as company_id from im_costs UNION
            select customer_id as company_id from im_costs UNION
            select company_id as company_id from im_projects UNION
            select final_company_id as company_id from im_projects
        ) c where company_id is not null)
        and company_id != :internal_company_id
        and company_id != :freelance_company_id"]
    cog_log Notice "Nuking [llength $unused_company_ids] companies"    
    foreach company_id $unused_company_ids {
	cog_log Notice "Nuking company $company_id"
        cog::company::nuke -company_id $company_id -current_user_id $user_id
    }

    set unused_user_ids [db_list users "select user_id from users where user_id not in (select distinct user_id from
        (
            select company_contact_id as user_id from im_projects UNION
            select project_lead_id as user_id from im_projects UNION
            select modifying_user as user_id from acs_objects UNION
            select accounting_contact_id as user_id from im_companies UNION
            select primary_contact_id as user_id from im_companies UNION
            select manager_id as user_id from im_companies UNION
            select manager_id as user_id from im_cost_centers UNION  
            select freelancer_id as user_id from im_trans_main_freelancer UNION
            select owner_id as user_id from im_user_absences                      
        ) u where user_id is not null
    )"]

    foreach unused_user_id $unused_user_ids {
	cog_log Notice "Nuking user $unused_user_id"
        cog::user::nuke -user_id $unused_user_id -current_user_id $user_id
    }

    cog_log Notice "Nuking Notes"
    set object_notes [db_list object_notes "select object_id from acs_objects where object_id not in (select note_id from im_notes) and object_type = 'im_note'"]
    foreach note_id $object_notes {
        im_note_nuke -current_user_id $user_id $note_id
    }


    db_dml disable_trigger "alter table im_costs enable trigger im_costs_project_cache_del_tr"

    set num_projects [db_string projects "select count(*) from im_projects where project_type_id != 100"]
    set num_costs [db_string projects "select count(*) from im_invoices"]
    set num_companies [db_string projects "select count(*) from im_companies"]
    set num_users [db_string projects "select count(*) from users"]

    cog::cleanup_objects
    
    return "$num_projects Projekte, $num_costs Invoices, $num_companies companies, $num_users users remaining"
}

ad_proc -public cog::cleanup_objects {

} {
    Cleans up any dangling objects
} {
    db_foreach object_type "select object_type, table_name, id_column from acs_object_types where supertype != 'relationship' and object_type != 'relationship'" {
        if {[im_table_exists $table_name]} {
            
            set dangling_ids [db_list dangling "select object_id from acs_objects where object_type = :object_type and object_id not in (select $id_column from $table_name)"]
            if {[llength $dangling_ids]>0} {
                foreach object_id $dangling_ids {
                    catch {db_1row delete_object "select acs_object__delete(:object_id)"}
                }
            }
        }
    }
}

ad_proc -public cog::recreate_folders {

} {
    Recreate the projects, users, company folders in case we migrated the system or anonymized it
} {
    # Filestorage folders
    set fs_dir_create_proc ""
    if {[apm_package_installed_p intranet-translation]} {
        set fs_dir_create_proc [parameter::get -package_id [im_package_translation_id] -parameter "FilestorageCreateFolderProc"]
    } 

    if {$fs_dir_create_proc ne ""} {
        foreach project_id [db_list projects "select project_id from im_projects where project_type_id != 100"] {
            $fs_dir_create_proc $project_id
        }
    }
}

ad_proc -public im_sysconfig_change_logo {
    { -logo_url ""}
    { -logo_link_url ""}
    { -portal_url ""}
} {
    Sets the logo e.g. for a new system

} {
    if {$logo_url ne ""} {
        parameter::set_from_package_key -package_key intranet-core -parameter "SystemLogo" -value $logo_url
    }
    
    if {$logo_link_url ne ""} {
        parameter::set_from_package_key -package_key intranet-core -parameter "SystemLogoLink" -value $logo_link_url
    }

    if {[apm_package_installed_p webix-portal]} {
        parameter::set_from_package_key -package_key webix-portal -parameter "LogoURL" -value $logo_url
        if {$portal_url ne ""} {
            parameter::set_from_package_key -package_key webix-portal -parameter "WebixPortalUrl" -value $portal_url   
        }
    }
}

ad_proc -public cog::sysconfig_change_server { 
    -server_path
    -server_url
    { -system_owner "" }
    -develop:boolean
    -no_docker:boolean
} {
    Allows moving a server to a different location. This will change the most typical parameters when you copy e.g. the database from production to staging
} {

    parameter::set_from_package_key -package_key acs-kernel -parameter "SystemURL" -value $server_url
    parameter::set_from_package_key -package_key intranet-core -parameter "UtilCurrentLocationRedirect" -value $server_url
	
    if {[string match "https://*" $server_url]} {
        parameter::set_from_package_key -package_key acs-kernel -parameter "RestrictLoginToSSLP" -value 1
    } else {
        parameter::set_from_package_key -package_key acs-kernel -parameter "RestrictLoginToSSLP" -value 0
    }

    if {[string match "*://*:*" $server_url]} {
        parameter::set_from_package_key -package_key acs-tcl -parameter "SuppressHttpPort" -value 0
    } else {
        parameter::set_from_package_key -package_key acs-tcl -parameter "SuppressHttpPort" -value 0
    }

    if {$system_owner ne ""} {
        parameter::set_from_package_key -package_key acs-kernel -parameter "SystemOwner" -value $system_owner
        parameter::set_from_package_key -package_key acs-kernel -parameter "OutgoingSender" -value $system_owner
        parameter::set_from_package_key -package_key acs-kernel -parameter "AdminOwner" -value $system_owner
        parameter::set_from_package_key -package_key acs-kernel -parameter "HostAdministrator" -value $system_owner
        parameter::set_from_package_key -package_key acs-mail-lite -parameter "NotificationSender" -value $system_owner
        parameter::set_from_package_key -package_key acs-mail-lite -parameter "FixedSenderEmail" -value $system_owner
        parameter::set_from_package_key -package_key acs-mail-lite -parameter "BounceDomain" -value [lindex [split $system_owner "@"] 1]
        set main_site_package_id [subsite::main_site_id]
        parameter::set_value -package_id $main_site_package_id -parameter "NewRegistrationEmailAddress" -value $system_owner
        
        if {[apm_package_installed_p intranet-helpdesk]} {
            parameter::set_from_package_key -package_key intranet-helpdesk -parameter "HelpdeskOwner" -value $system_owner
        }
    
        if {[apm_package_installed_p webix-portal]} {
            catch {parameter::set_from_package_key -package_key webix-portal -parameter "GeneralFreelancerContact" -value $system_owner}
        }

    }

    parameter::set_from_package_key -package_key intranet-core -parameter "BackupBasePathUnix" -value "${server_path}/filestorage/backup"
    parameter::set_from_package_key -package_key intranet-filestorage -parameter "BugBasePathUnix" -value "${server_path}/filestorage/bugs"
    parameter::set_from_package_key -package_key intranet-filestorage -parameter "CompanyBasePathUnix" -value "${server_path}/filestorage/companies"
    parameter::set_from_package_key -package_key intranet-filestorage -parameter "HomeBasePathUnix" -value "${server_path}/filestorage/home"
    parameter::set_from_package_key -package_key intranet-filestorage -parameter "ProjectBasePathUnix" -value "${server_path}/filestorage/projects"
    parameter::set_from_package_key -package_key intranet-filestorage -parameter "ProjectSalesBasePathUnix" -value "${server_path}/filestorage/project_sales"
    parameter::set_from_package_key -package_key intranet-filestorage -parameter "TicketBasePathUnix" -value "${server_path}/filestorage/tickets"
    parameter::set_from_package_key -package_key intranet-filestorage -parameter "UserBasePathUnix" -value "${server_path}/filestorage/users"
    parameter::set_from_package_key -package_key intranet-filestorage -parameter "CostBasePathUnix" -value "${server_path}/filestorage/costs"
    parameter::set_from_package_key -package_key intranet-filestorage -parameter "EventBasePathUnix" -value "${server_path}/filestorage/events"
    parameter::set_from_package_key -package_key intranet-invoices -parameter "InvoiceTemplatePathUnix" -value "${server_path}/filestorage/templates"
    if {[apm_package_installed_p intranet-mail-import]} {
        parameter::set_from_package_key -package_key intranet-mail-import -parameter "MailDir" -value "${server_path}/maildir"
    }

    if {!$no_docker_p} {
        parameter::set_from_package_key -package_key acs-mail-lite -parameter "SMTPHost" -value "postfix"
        parameter::set_from_package_key -package_key acs-mail-lite -parameter "SMTPPassword" -value ""
        parameter::set_from_package_key -package_key acs-mail-lite -parameter "SMTPPort" -value "25"
        parameter::set_from_package_key -package_key acs-mail-lite -parameter "SMTPUser" -value ""
    }    

    # Set parameters for redirecting mail
    if {$develop_p} {
		parameter::set_from_package_key -package_key acs-mail-lite -parameter "EmailDeliveryMode" -value "redirect"
        if {$system_owner eq ""} {
            set system_owner [parameter::get_from_package_key -package_key acs-mail-lite -parameter "EmailRedirectTo"]
            if {$system_owner eq ""} {
                set system_owner [party::email -party_id [auth::get_user_id]]
            }
        }
		parameter::set_from_package_key -package_key acs-mail-lite -parameter "EmailRedirectTo" -value "$system_owner"
		parameter::set_from_package_key -package_key intranet-core -parameter "TestDemoDevServer" -value "1"
        if {[apm_package_installed_p xotcl-core]} {
            parameter::set_from_package_key -package_key xotcl-core -parameter "NslogRedirector" -value "1"
        }
        parameter::set_from_package_key -package_key intranet-core -parameter "TestDemoDevServer" -value "1"
        
        # catch {parameter::set_from_package_key -package_key cognovis-core -parameter "ErrorChannel" -value "development"}

    }
    
    if {[apm_package_installed_p intranet-collmex]} {
        parameter::set_from_package_key -package_key intranet-collmex -parameter "ActiveP" -value "0"	    
		parameter::set_from_package_key -package_key intranet-collmex -parameter "Login" -value ""	    
    }

    if {[apm_package_installed_p intranet-trans-memoq]} {
        parameter::set_from_package_key -package_key intranet-trans-memoq -parameter "APIKey" -value ""
        parameter::set_from_package_key -package_key intranet-trans-memoq -parameter "MemoQServer" -value ""
        parameter::set_from_package_key -package_key intranet-trans-memoq -parameter "MemoQAPIServer" -value ""
        parameter::set_from_package_key -package_key intranet-trans-memoq -parameter "MemoQDomainAttribute" -value ""
    }
}
