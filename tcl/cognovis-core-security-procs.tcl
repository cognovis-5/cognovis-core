ad_library {
    security copy from oacs-5-10 to make host issues go away    
    
    @author malte.sussdorff@cognovis.de
}

ad_proc -public security::configured_driver_info {} {
	
	Return a list of dicts containing type, driver, location and port
	of all configured drivers
	
	@see util_driver_info
	
} {
    set protos {http 80 https 433}
    set result {}
    foreach i [ns_driver info] {
        set type     [dict get $i type]
        set location [dict get $i location]
        set proto    [dict get $i protocol]
        if {$location ne ""} {
            set li [ns_parseurl $location]

            if {[dict exists $li port]} {
                set port [dict get $li port]
                set suffix ":$port"
            } else {
                set port [dict get $protos $proto]
                set suffix ""
            }
        } else {
            #
            # In case we have no "location" defined (e.g. virtual
            # hosting), get "port" and suffix directly from the
            # driver.
            #
            if {[dict exists $i port]} {
                set port [lindex [dict get $i port] 0]
                set defaultport [dict get $i defaultport]
            } else {
                set driver_section [ns_driversection -driver [dict exists $i module]]
                set port [ns_config -int $driver_section port]
                set defaultport [dict get $protos $proto]
            }
            #
            # Newer versions of NaviServer support multiple ports
            # per driver. For now, take the first one (similar with "address" below).
            #
            set port [lindex [dict get $i port] 0]
            if {$port eq $defaultport} {
                set suffix ""
            } else {
                set suffix ":$port"
            }
        }
        lappend result [list  proto $proto  driver [dict get $i module]  host [lindex [dict get $i address] 0]  location $location port $port suffix $suffix]
    }
    return $result
}

ad_proc -public security::validated_host_header {} {
    @return validated host header field or empty
    @author Gustaf Neumann

    Protect against faked or invalid host header fields. Host header
    attacks can lead to web-cache poisoning and password reset attacks
    (for more details, see e.g.
        http://www.skeletonscribe.net/2013/05/practical-http-host-header-attacks.html)
} {
    #
    # Check, if we have a host header field
    #
    set host [ns_set iget [ns_conn headers] Host]
    if {$host eq ""} {
        return ""
    }
    #
    # Domain names are case insensitive. So convert it to lower to
    # avoid surprises.
    #
    set host [string tolower $host]

    #
    # Check, if we have validated it before, or it belongs to the
    # predefined accepted host header fields.
    #
    set key ::acs::validated($host)
    if {[info exists $key]} {
        return $host
    }

    if {![string match *//* $host]} {
        set splithost [ns_conn protocol]://$host
    } else {
        set splithost $host
    }
    if {![util::split_location $splithost .proto hostName hostPort]} {
        return ""
    }

    #
    # Remove trailing dot, as this is allowed in fully qualified DNS
    # names (see e.g. §3.2.2 of RFC 3976).
    #
    set hostName [string trimright $hostName .]

    #
    # Check, if the provided host is the same as the configured host
    # name for the current driver or one of its IP addresses. Should
    # be true in most cases.
    #
    set driverInfo [util_driver_info]
    set driverHostName [dict get $driverInfo hostname]

    #
    # The port is currently ignored for determining the validated host
    # header field.
    #
    # Validation is OK, when the provided host-header content is
    # either the same as configured hostname in the driver
    # configuration or one of its IP addresses.
    #
    set validationOk 0
    if {$hostName eq $driverHostName} {
        set validationOk 1
    } else {
        try {
            ns_addrbyhost -all $driverHostName
        } on error {errorMsg} {
            #
            # Name resolution of the hostname configured for this
            # driver failed, we cannot validate incoming IP addresses.
            #
            ns_log error "security::validated_host_header: configuration error:" \
                "name resolution for configured hostname '$driverHostName'" \
                "of driver '[ad_conn driver]' failed"
        } on ok {result} {
            set validationOk [expr {$hostName in $result}]
        }
    }

    #
    # Check, if the provided host is the same in [ns_conn location]
    # (will be used as default, but we do not want a warning in such
    # cases).
    #
    if {$validationOk == 0 && [util::split_location [ns_conn location] proto locationHost locationPort]} {
        set validationOk [expr {$hostName eq $locationHost}]
    }

    #
    # Check, if the provided host is the same as in the configured
    # SystemURL.
    #
    if {$validationOk == 0 && [util::split_location [ad_url] .proto systemHost systemPort]} {
        set validationOk [expr {$hostName eq $systemHost
                                && ($hostPort eq $systemPort || $hostPort eq "") }]
    }

    if {$validationOk == 0 && [ns_info name] eq "NaviServer"} {
        #
        # Check against the virtual server configuration of NaviServer.
        #
        set s [ns_info server]
        set driverInfo [security::configured_driver_info]
        set drivers [lmap d $driverInfo {dict get $d driver}]

        foreach driver $drivers {
            #
            # Check global "servers" configuration for virtual servers for the driver
            #
            set ns [ns_configsection ns/module/$driver/servers]
            if {$ns ne ""} {
                #
                # We have a global "servers" configuration for the driver
                #
                set names [lmap {key value} [ns_set array $ns] {
                    if {$key ne $s} continue
                    set value
                }]
                if {$host in $names} {
                    ns_log notice "security::validated_host_header: found $host" \
                        "in global virtual server configuration for $driver"
                    set validationOk 1
                    break
                }
            }
        }
    }

    if {$validationOk == 0} {
        #
        # Check against host node map. Here we need as well protection
        # against invalid utf-8 characters.
        #
        if {![security::provided_host_valid $hostName]} {
            return ""
        }

        set validationOk [db_0or1row host_header_field_mapped {select 1 from host_node_map where host = :hostName}]
    }

    if {$validationOk == 0} {
        #
        # This is not an attempt, where someone tries to lure us to a
        # different host via redirect. "localhost" is always safe.
        #
        set validationOk [expr {$hostName eq "localhost"}]
    }

    #
    # When any of the validation attempts above were successful, we
    # are done. We keep the logic for successful lookups
    # centralized. Performance of the individual tests are not
    # critical, since the lookups are cache per thread.
    #
    if {$validationOk} {
        set $key 1
        return $host
    }

    #
    # We could/should check as well against a white-list of additional
    # hostnames (maybe via ::acs::validated, or via config file, or
    # via additional package parameter). Probably the best way is to
    # get alternate (alias) names from the driver section of the
    # current driver [ns_conn driver] (maybe check global and local).
    #
    #ns_set array [ns_configsection ns/module/nssock/servers]

    #
    # Now we give up
    #
    ns_log warning "ignore untrusted host header field: '$host'"

    return ""
}

ad_proc -private security::provided_host_valid {host} {
    Check, if the provided host contains just valid characters.
    Spit warning message out only once per request.
    @param host host from host header field.
} {
    #
    # The per-request cache takes care of outputting error message only
    # once per request.
    #
        set result 1
        if {$host ne ""} {
            if {![regexp {^[\w.:@+/=$%!*~\[\]-]+$} $host]} {
                #
                # Don't use "ad_log", since this might leed to a recursive loop.
                #
                binary scan [encoding convertto utf-8 $host] H* hex
                ns_log warning "provided host <$host> (hex $hex) contains invalid characters\n\
                        URL: [ns_conn url]\npeer addr:[ad_conn peeraddr]"
                set result 0
            }
        }
        set result
}

