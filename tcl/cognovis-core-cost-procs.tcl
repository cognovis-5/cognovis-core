
ad_library {
    Procedures for supporting with cost entries
    @author malte.sussdorff@cognovis.de
}

namespace eval cog::cost {

    ad_proc -public nuke {
        -cost_id:required
        {-current_user_id ""}
    } {
        Nukes a cost record from the database
    } {

        set current_user_id [auth::get_user_id]
        if {![acs_user::site_wide_admin_p -user_id $current_user_id]} {
            return "0"
        }

        db_0or1row cost_infos "
            select cost_id, object_type 
            from im_costs, acs_objects 
            where cost_id = object_id and cost_id = :cost_id"
        
        # Delete Multiple-Values associated to this cost item ("Canned Notes")
        db_dml del_multi_values "delete from im_dynfield_attr_multi_value where object_id = :cost_id"

        # Delete references from im_hours to im_costs.
        db_dml hours_costs_link "update im_hours set cost_id = null where cost_id = :cost_id"

        # ToDo: Remove this.
        # Instead, the referencing im_expense_bundles (data type doesn't exist yet)
        # should be deleted with the appropriate destructor method
        db_dml expense_cost_link "delete from im_expenses where bundle_id = :cost_id"

        cog::rel::remove -object_id $cost_id -current_user_id $current_user_id
        cog::mail::nuke_log -object_id $cost_id -current_user_id $current_user_id
 
        if {[im_column_exists im_invoice_items created_from_item_id]} {
            db_dml created_from_item_id "
            update im_invoice_items 
            set created_from_item_id = null 
            where created_from_item_id in (
                select	item_id
                from	im_invoice_items
                where	invoice_id = :cost_id
            )
                "
        }
        im_exec_dml del_cost "${object_type}__delete($cost_id)"
    }

    ad_proc -public permissions {user_id cost_id view_var read_var write_var admin_var} {
        Fill the permissions on the cost item.
    } {
        upvar $view_var view
        upvar $read_var read
        upvar $write_var write
        upvar $admin_var admin

        set user_is_freelance_p [im_user_is_freelance_p $user_id]
        set user_is_inco_customer_p [im_user_is_inco_customer_p $user_id]
        set user_is_customer_p [im_user_is_customer_p $user_id]

        # -----------------------------------------------------
        # Get Cost information
        set customer_id 0
        set provider_id 0
        set cost_center_id 0
        set cost_type_id 0
        db_0or1row get_companies "
            select	c.customer_id,
                c.provider_id,
                (select o.object_type from acs_objects o where o.object_id = c.provider_id) as provider_otype,
                c.cost_center_id,
                c.cost_type_id,
                c.cost_status_id
            from	im_costs c
            where	c.cost_id = :cost_id
        "

        # -----------------------------------------------------
        # Cost Center permissions - check if the user has read permissions
        # for this particular cost center
        if {[apm_package_installed_p "intranet-cost-center"]} {
            db_1row cost_type_privileges "select read_privilege, write_privilege from im_cost_types where cost_type_id = :cost_type_id"
            set cc_read [permission::permission_p -no_login -object_id $cost_center_id -party_id $user_id -privilege $read_privilege]
            set cc_write [permission::permission_p -no_login -object_id $cost_center_id -party_id $user_id -privilege $write_privilege]
        } else {
            set cc_read 1
            set cc_write 1
        }

        set can_read [expr [im_permission $user_id view_costs] || [im_permission $user_id view_invoices]]
        set can_write [expr [im_permission $user_id add_costs] || [im_permission $user_id add_invoices]]

        # Disable ability to write costs for filed costs
        if {$cost_status_id eq [im_cost_status_filed]} {
            set can_write 0
        }

        # AND-connection with add/view - costs/invoices
        if {!$can_read} { set cc_read 0 }
        if {!$can_write} { set cc_write 0 }

        # Set the other two variables
        set cc_admin $cc_write
        set cc_view $cc_read

        # -----------------------------------------------------
        # Customers get the right to see _their_ invoices
        set cust_view 0
        set cust_read 0
        set cust_write 0
        set cust_admin 0
        set incust_view 0
        set incust_read 0
        set incust_write 0
        set incust_admin 0

        if {$user_is_inco_customer_p && $customer_id && $customer_id != [im_company_internal]} {
            im_company_permissions $user_id $customer_id incust_view incust_read incust_write incust_admin
        }
        if {$user_is_customer_p && $customer_id && $customer_id != [im_company_internal]} {
            im_company_permissions $user_id $customer_id cust_view cust_read cust_write cust_admin
        }

        # -----------------------------------------------------
        # Providers get the right to see _their_ invoices
        # This leads to the fact that FreelanceManagers (the guys
        # who can convert themselves into freelancers) can also
        # see the freelancer's permissions. Is this desired?
        # I guess yes, even if they don't usually have the permission
        # to see finance.
        set prov_view 0
        set prov_read 0
        set prov_write 0
        set prov_admin 0

        switch $provider_otype {
            im_company {
                if {$user_is_freelance_p && $provider_id && $provider_id != [im_company_internal]} {
                    im_company_permissions $user_id $provider_id prov_view prov_read prov_write prov_admin
                }
            }
            user {
                # This is an expense or an expense bundle, probably.
                if {$provider_id eq $user_id} {
                    set prov_view 1
                    set prov_read 1
                }
            }
        }


        # -----------------------------------------------------
        # Set the permission as the OR-conjunction of provider and customer
        set view [expr {$incust_view || $cust_view || $prov_view || $cc_view}]
        set read [expr {$incust_read || $cust_read || $prov_read || $cc_read}]
        set write [expr {$incust_write || $cust_write || $prov_write || $cc_write}]
        set admin [expr {$incust_admin || $cust_admin || $prov_admin || $cc_admin}]

        # Limit rights of all users to view & read if they dont
        # have the expressive permission to "add_costs or add_invoices".
        if {!$can_write} {
            set write 0
            set admin 0
        }
    }
}