ad_library {

    Initialization for cognovis-core module
    
    @author malte.sussdorff@cognovis.de
}

ns_logctl severity Error -color red

# Initialize the search "semaphore" to 0.
# There should be only one thread indexing files at a time...
nsv_set intranet_timesheet2 timesheet_synchronizer_p 0

# Check for imports of external im_hours entries every every X minutes
ad_schedule_proc -thread t 3600 im_timesheet2_sync_timesheet_costs

# Schedule the reminders
set remind_employees_p [parameter::get_from_package_key -package_key cognovis-core -parameter RemindEmployeesToLogHoursP -default 0 ]

if {$remind_employees_p} {
    ad_schedule_proc -thread t -schedule_proc ns_schedule_weekly [list 1 7 0] im_timesheet_remind_employees
}


ad_proc -public -callback im_payment_after_create {
	{-payment_id:required}
	{-payment_method_id ""}
} {
    This callback allows you to execute action before and after every
    important change of object. Examples:
    - Copy preset values into the object
    - Integrate with external applications via Web services etc.
    
    @param payment_id ID of the payment
    @param payment_method_id ID of the Payment Method
} -

ad_proc -public -callback im_payment_after_update {
	{-payment_id:required}
} {
    Allow the recording of a payment
    
    @param payment_id ID of the payment
} -

ad_proc -public -callback cog::invoice_item::after_create {
    -invoice_id:required
    -invoice_item_id:required
} {
    Callback run after creation of an invoice_item for an invoice

    this will not trigger an update of the invoice callback
} - 

ad_proc -public -callback cog::invoice_item::after_update {
    -invoice_item_id:required
} {
    Callback run after update of an invoice_item for an invoice

    This will not trigger an update of the invoice callback.
} -



# Callbacks 
ad_proc -public -callback absence_on_change {
    {-absence_id:required}
    {-absence_type_id:required}
    {-user_id:required}
    {-start_date:required}
    {-end_date:required}
    {-duration_days:required}
    {-transaction_type:required}
} {
    Callback to be executed after an absence has been created
} -

ad_proc -public -callback im_user_absence_new_button_pressed {
    {-button_pressed:required}
} {
    This callback is executed after we checked the pressed buttons but before the normal delete / cancel check is executed. 
    
    This allows you to add additional activities based on the actions defined e.g. in the im_user_absence_new_actions. As it is called before delete / cancel you can
    have more actions defined.

} - 


ad_proc -public -callback im_user_absence_new_actions {
} {
    This callback is executed after we build the actions for the new absence form
    
    This allows you to extend in the uplevel the form with any additional actions you might want to add.

} - 

ad_proc -public -callback im_user_absence_info_actions {
} {
    This callback is executed after we build the actions for the absence info page
    
    This allows you to extend in the uplevel the form with any additional actions you might want to add.

} - 

ad_proc -public -callback im_user_absence_perm_check {
    {-absence_id:required}
} {
    This callback is executed first time we determine that we have an absence_id
    
    This allows you to add additional permission checks, especially against ID guessing.

} - 

ad_proc -public -callback im_trace_column_change {
    {-user_id:required}
    {-object_id:required}
    {-table:required}
    {-table:required}
    {-column_name:required}
    {-pretty_name:required}
    {-old_value:required}
    {-new_value:required}
} {
    Callback to be executed after a change in a record
} -

ad_proc -public -callback im_category_after_create {
    {-object_id:required}
    {-type ""}
    {-status ""}
    {-category_id ""}
    {-category_type ""}
} {
    This is a callback to map attributes and categories using respectively attribute_id and category_id

    @param category_id ID of the category
    @param category_type Type of the category
} -

ad_proc -public -callback im_category_after_update {
    {-object_id:required}
    {-type ""}
    {-status ""}
    {-category_id ""}
    {-category_type ""}
} {
    This is a callback to map attributes and categories using respectively attribute_id and category_id

    @param category_id ID of the category
    @param category_type Type of the category
} -

ad_proc -public -callback im_projects_index_before_render {
    {-view_name:required}
    {-view_type:required}
    {-sql:required}
    {-table_header ""}
    {-variable_set ""}
} {
    This callback is executed before /projects/index is rendered / the sql command actually executed.

    The callback implementation needs to run ad_script_abort in the uplevel, so you don't execute the SQL statement and try to render the component.

    @param view_name view_name used to render the columns.
    @param view_type The view_type. This can be anything, empty string usually means you want to render the component
    @param sql The SQL string which im_timesheet_task_list_component prepares
    @param table_header Name of the table in the spreadsheet (e.g. in Excel).
    @param variable_set A set of variables to pass through
} -

ad_proc -public -callback im_projects_csv1_before_render {
    {-view_name:required}
    {-view_type:required}
    {-sql:required}
    {-table_header ""}
} {
    This callback is executed before im_projects_csv1 is rendered / the sql command actually executed.

    The callback implementation needs to run ad_script_abort in the uplevel, so you don't execute the SQL statement and try to return a CSV file.

    @param view_name view_name used to render the columns.
    @param view_type The view_type. This can be anything, empty string usually means you want to render the component
    @param sql The SQL string which im_timesheet_task_list_component prepares
    @param table_header Name of the table in the spreadsheet (e.g. in Excel).
} -

ad_proc -public -callback im_projects_index_filter {
    {-form_id:required}
} {
    This callback is executed after we generated the filter ad_form

    This allows you to extend in the uplevel the form with any additional filters you might want to add.

    @param form_id ID of the form to which we want to append filter elements
} -

ad_proc -public -callback im_invoices_index_before_render {
    {-view_name:required}
    {-view_type:required}
    {-sql:required}
    {-table_header ""}
    {-variable_set ""}
} {
    This callback is executed before /projects/index is rendered / the sql command actually executed.

    The callback implementation needs to run ad_script_abort in the uplevel, so you don't execute the SQL statement and try to render the component.

    @param view_name view_name used to render the columns.
    @param view_type The view_type. This can be anything, empty string usually means you want to render the component
    @param sql The SQL string which im_timesheet_task_list_component prepares
    @param table_header Name of the table in the spreadsheet (e.g. in Excel).
    @param variable_set A set of variables to pass through
} -


ad_proc -callback im_trace_table_change {
    {-object_id:required}
    {-table:required}
    {-message:required}
} {
    Callback to be executed after a set of changes in a table record
} -

ad_proc -callback cog::price::after_create {
    -price_id:required
    -company_id:required 
    -material_id:required
} {
    After adding a new price for a company

    @param price_id New Price id
    @param company_id Company for which we have this price
    @param material_id Material for this price
} -

ad_proc -callback cog::price::after_update {
    -old_price_id:required
    -price_id:required
} {
    After created a copy of the old_price with the new values

    @param old_price_id Old price we changed
    @param price_id Newly created price_id
} -

namespace eval cog::cost {
    variable STATUS_PAID 3810
    variable STATUS_DELETED 3812
    variable STATUS_FILED 3814
    variable STATUS_PARTIALLY_PAID 3808
    variable STATUS_WRITE_OFF 11000503
    variable STATUS_CREATED 3802
    variable STATUS_OUTSTANDING 3804
}

namespace eval cog::invoice {
    variable STATUS_CREATED 10124
    variable STATUS_OUTSTANDING 10127
    variable STATUS_PAST_DUE 10128
    variable STATUS_DELETED 10131
    variable STATUS_IN_PROCESS 10123
    variable STATUS_PAID 10130
    variable STATUS_FILED 10132
    variable STATUS_PARTIALLY_PAID 10129
}

namespace eval cog::partner {
    variable STATUS_TARGETED 60
    variable STATUS_IN_DISCUSSION 61
    variable STATUS_ACTIVE 62
    variable STATUS_DEAD 65
}

namespace eval cog::topic {
    variable STATUS_ASSIGNED 1202
    variable STATUS_ACCEPTED 1204
    variable STATUS_REJECTED 1206
    variable STATUS_NEEDS_CLARIFY 1208
    variable STATUS_CLOSED 1210
    variable STATUS_OPEN 1200
}

namespace eval cog::company {
    variable STATUS_INACTIVE 48
    variable STATUS_DECLINED 47
    variable STATUS_ACTIVE 46
    variable STATUS_QUALIFYING 43
}

namespace eval cog::cost_center {
    variable STATUS_ACTIVE 3101
}

namespace eval cog::investment {
    variable STATUS_DELETED 3503
    variable STATUS_AMORTIZED 3505
    variable STATUS_ACTIVE 3501
}

namespace eval cog::office {
    variable STATUS_ACTIVE 160
    variable STATUS_INACTIVE 161
}

namespace eval cog::trans::task {
    variable STATUS_FOR_EDIT 346
    variable STATUS_DELETED 372
    variable STATUS_PAYED 370
    variable STATUS_FOR_TRANS 342
    variable STATUS_EDITING 348
    variable STATUS_FOR_PROOF 350
    variable STATUS_FOR_QCING 354
    variable STATUS_QCING 356
    variable STATUS_FOR_DELIV 358
    variable STATUS_DELIVERED 360
    variable STATUS_INVOICED 365
    variable STATUS_PROOFING 352
    variable STATUS_CREATED 340
    variable STATUS_TRANS_ING 344
}

namespace eval cog::timesheet::task {
    variable STATUS_INACTIVE 9602
    variable STATUS_ACTIVE 9600
}

namespace eval cog::material {
    variable STATUS_ACTIVE 9100
    variable STATUS_INACTIVE 9102
}

namespace eval cog::trans::rfq {
    variable STATUS_OPEN 4450
    variable STATUS_CLOSED 4455
    variable STATUS_CANCELED 4460
    variable STATUS_DELETED 4465
    variable OVERALL_STATUS_OK 4480
    variable OVERALL_STATUS_NOT_OK 4485
}