ad_library {
    Procedures to handle translation
    @author malte.sussdorff@cognovis.de
}


#---------------------------------------------------------------
# Helper procs for translation
#---------------------------------------------------------------

namespace eval cog::trans {

    ad_proc quality_rating {
        {-freelancer_id ""}
        {-assignment_id ""}
        {-subject_area_id ""}
        {-quality_type_id ""}
    } {
        Returns the quality rating for a freelancer

        @param freelancer_id Freelancer in whos quality we are interested
        @param assignment_id If we only want the rating for a specific assignment
        @param subject_area_id All ratings for this subject_area
        @param quality_type_id All ratings for a specific quality type
    } {
        set where_clause_list [list]
        if {$freelancer_id ne ""} {
            lappend where_clause_list "f.assignee_id = :freelancer_id"
        }
        if {$assignment_id ne ""} {
             lappend where_clause_list "f.assignment_id = :assignment_id"
        }

        if {$subject_area_id ne ""} {
            lappend where_clause_list "r.subject_area_id = :subject_area_id"
        }

        if {$quality_type_id ne ""} {
            lappend where_clause_list "r.quality_type_id = :quality_type_id"
        }

        if {[llength $where_clause_list]>0} {
        
            set query_sql "select round(avg(c.aux_int1),2) 
                from im_categories c, im_assignment_quality_reports r, im_assignment_quality_report_ratings rr, im_freelance_assignments f 
                where rr.report_id = r.report_id 
                    and c.category_id = rr.quality_level_id 
                    and r.assignment_id = f.assignment_id 
                    and [join $where_clause_list " and "]"


            set rating [db_string rating $query_sql -default ""]
        } else {
            set rating ""
        }

        return $rating
    }

    ad_proc -public project_valid_language_ids {
        -project_id:required
        { -language_ids "" }
        { -type "source" }
    } {
        Returns the list of filtered Languages from the given language_ids checked against valid ones for the project

        Also checks for parent language groups and dialects (so fuzzy match)

        @param project_id Project for which we want to check languages
        @param language_ids List of languages which we want to check if they are valid for the project
        @param type Type of language we check against, might be "source" or "target"
    } {
        switch $type {
            "source" {
                db_1row source_langs "select  source_language_id, ch.parent_id
                    from    im_projects left outer join im_category_hierarchy ch on (child_id = source_language_id)
                    where   project_id = :project_id"

                if {[info exists parent_id] && $parent_id ne ""} {
                    set project_language_ids [cog_sub_categories $parent_id]
                } else {
                    set project_language_ids [cog_sub_categories $source_language_id]
                }
            }
            "target" {
                set project_language_ids [list]
                db_foreach target_langs "
                        select  language_id as target_language_id, parent_id
                        from	im_target_languages left outer join im_category_hierarchy ch on (child_id = language_id)
                        where	project_id = :project_id
                " {
                    if {[info exists parent_id] && $parent_id ne ""} {
                        set target_ids [cog_sub_categories $parent_id]
                    } else {
                        set target_ids [cog_sub_categories $target_language_id]
                    }
                    foreach target_id $target_ids {
                        if {[lsearch $project_language_ids $target_id]<0} {
                            lappend project_language_ids $target_id
                        }
                    }
                }
            }
        }


        if {$language_ids ne ""} {
            # Check that they are valid
            foreach language_id $language_ids {
                if {[lsearch $project_language_ids $language_id]<0} {
                    set language_ids [lsearch -inline -all -not -exact $language_ids $language_id]
                }
            }
        } else {
            set language_ids $project_language_ids
        }

        return [cog_sub_categories -include_disabled_p 0 $language_ids]
    }

    ad_proc -public main_translator_ids {
        -project_id:required
    } {
        Return the  list of main translators for a specific project. 

        Will look at final_company before company_id for the customer

        @param project_id Project for which we look up main translators

        @return main_translator_ids List of main translators for the customer.
    } {
    	return [util_memoize [list cog::trans::main_translator_ids_helper -project_id $project_id] 600]
    }

    ad_proc -public main_translator_ids_helper {
        -project_id:required
    } {
        Return the  list of main translators for a specific project. 

        Will look at final_company before company_id for the customer

        @param project_id Project for which we look up main translators

        @return main_translator_ids List of main translators for the customer.
    } {
        set source_language_ids [cog::trans::project_valid_language_ids -project_id $project_id]
        set target_language_ids [cog::trans::project_valid_language_ids -project_id $project_id -type "target"]
        set customer_id [db_string final_company_or_customer "select coalesce(final_company_id,company_id) from im_projects where project_id = :project_id"]

        set main_translator_ids [db_list main_translator_ids "select distinct freelancer_id 
            from im_trans_main_freelancer 
            where customer_id = :customer_id 
            and source_language_id in ([ns_dbquotelist $source_language_ids]) 
            and target_language_id in ([ns_dbquotelist $target_language_ids])"]

        return $main_translator_ids
    }
}