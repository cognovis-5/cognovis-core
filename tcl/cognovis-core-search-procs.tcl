namespace eval cog::search {

    ad_proc search {
        {-user_id ""}
        -query:required
        {-object_types ""}
        {-max_results ""}
    } {
        Search for objects based on the given query string.

        @param user_id The ID of the user performing the search.
        @param query The search query string.
        @param object_types (optional) List of object types to search for. If not provided, all object types will be searched.
        @param max_results (optional) The maximum number of results to return.
        @return A dictionary of search results, where the keys are object IDs and the values are dictionaries containing object details.
    } {
        set query [string tolower $query]
        regsub -all {["']} $query {} query

        if {$user_id eq ""} {
            set user_id [auth::get_user_id]
        }

        set search_results [dict create]
        
        if {$object_types eq ""} {
            set object_types [list "im_project" "im_company" "person" "im_invoice"]
        }

        if {$max_results eq ""} {
            set max_results [parameter::get_from_package_key -package_key "cognovis-core" -parameter "MaxSearchResults" -default 5]
        }

        foreach object_type $object_types {
            set object_results [cog::search::${object_type} -query $query -user_id $user_id -max_results $max_results]
            dict for {object_id object_data} $object_results {
                dict set search_results $object_id $object_data
            }
        }

        return $search_results
    }

    ad_proc -public im_project {
        -user_id:required
        -query:required
        -max_results:required
    } {
        Search for projects based on the given query string.

        @param user_id The ID of the user performing the search.
        @param query The search query string.

        @return A dictionary of search results, where the keys are project IDs and the values are dictionaries containing project details.
    } {
        # Initialize an empty dictionary to store the search results
        set search_results [dict create]

        # Check if the user has the "view_projects_all" permission
        if {[im_permission $user_id "view_projects_all"]} {
            # If the user has the permission, no additional project permission SQL is needed
            set project_perm_sql ""
        } else {
            # If the user doesn't have the permission, build the project permission SQL
            set project_perm_sql "
                AND p.project_id IN (
                    SELECT p.project_id
                    FROM im_projects p
                    JOIN acs_rels r ON r.object_id_one = p.project_id
                    WHERE r.object_id_two = :user_id
                )
            "
        }

        # Definiere den Schwellenwert für den ts_rank
        set ts_rank_threshold [parameter::get_from_package_key -package_key "cognovis-core" -parameter "SearchTsRankThreshold" -default 0.03]


        # Build the SQL query to search for projects
        set project_sql "
            SELECT DISTINCT
                p.project_id,
                p.project_nr,
                p.project_name,
                c.company_name,
                p.company_id,
                p.end_date,
                ts_rank(to_tsvector('english', p.project_nr || ' ' || p.project_name || ' ' || c.company_name), to_tsquery('english', replace(:query, ' ', ':* | ') || ':*')) AS ts_rank
            FROM im_projects p
            JOIN im_companies c ON p.company_id = c.company_id
            WHERE to_tsvector('english', p.project_nr || ' ' || p.project_name || ' ' || c.company_name) @@ to_tsquery('english', replace(:query, ' ', ':* | ') || ':*')
                AND p.project_type_id NOT IN (100, 101)
                AND ts_rank(to_tsvector('english', p.project_nr || ' ' || p.project_name || ' ' || c.company_name), to_tsquery('english', replace(:query, ' ', ':* | ') || ':*')) > :ts_rank_threshold
                $project_perm_sql
            ORDER BY ts_rank DESC
            LIMIT :max_results            
        "

        # Execute the SQL query and process the results
        db_foreach project $project_sql {
            # Build the search text for the project
            set search_text "${project_nr}: $project_name (${company_name})"

            # Build the project data dictionary
            set project_data [dict create \
                project_nr $project_nr \
                company_name $company_name \
                end_date $end_date \
            ]

            # Add the project to the search results dictionary
            dict set search_results $project_id [dict create \
                object_name $project_name \
                object_type "im_project" \
                search_text $search_text \
                ts_rank $ts_rank \
                object_data $project_data \
            ]
        }

        return $search_results
    }

    ad_proc -public im_company {
        -user_id:required
        -query:required
        -max_results:required
    } {
        Search for companies based on the given query string.

        @param user_id The ID of the user performing the search.
        @param query The search query string.

        @return A dictionary of search results, where the keys are company IDs and the values are dictionaries containing company details.
    } {
        # Initialize an empty dictionary to store the search results
        set search_results [dict create]

        # Check if the user has the "view_companies_all" permission
        if {[im_permission $user_id "view_companies_all"]} {
            # If the user has the permission, only exclude deleted companies
            set company_perm_sql "
                AND c.company_status_id NOT IN ([im_company_status_deleted])
            "
        } else {
            # If the user doesn't have the permission, build the company permission SQL
            set company_perm_sql "
                AND c.company_id IN (
                    SELECT c.company_id
                    FROM im_companies c
                    JOIN acs_rels r ON r.object_id_one = c.company_id
                    WHERE r.object_id_two = :user_id
                        AND c.company_status_id NOT IN ([im_company_status_deleted])
                )
            "
        }

        # Definiere den Schwellenwert für den ts_rank
        set ts_rank_threshold [parameter::get_from_package_key -package_key "cognovis-core" -parameter "SearchTsRankThreshold" -default 0.03]          

        # Baue die SQL-Abfrage zur Suche nach Firmen
        set company_sql "
            SELECT DISTINCT
                c.company_id,
                c.company_name,
                o.address_city,
                o.address_postal_code,
                o.address_country_code,
                ts_rank(
                    setweight(to_tsvector('english', c.company_name), 'A') ||
                    coalesce(setweight(to_tsvector('english', o.address_city || ' ' || o.address_postal_code || ' ' || o.address_country_code), 'B'), to_tsvector('')),
                    to_tsquery('english', replace(:query, ' ', ':* | ') || ':*')
                ) AS ts_rank
            FROM im_companies c
            LEFT JOIN im_offices o ON c.main_office_id = o.office_id
            WHERE (
                    to_tsvector('english', c.company_name) @@ to_tsquery('english', replace(:query, ' ', ':* | ') || ':*')
                    OR
                    to_tsvector('english', coalesce(o.address_city, '') || ' ' || coalesce(o.address_postal_code, '') || ' ' || coalesce(o.address_country_code, '')) @@ to_tsquery('english', replace(:query, ' ', ':* | ') || ':*')
                )
                AND ts_rank(
                    setweight(to_tsvector('english', c.company_name), 'A') ||
                    coalesce(setweight(to_tsvector('english', o.address_city || ' ' || o.address_postal_code || ' ' || o.address_country_code), 'B'), to_tsvector('')),
                    to_tsquery('english', replace(:query, ' ', ':* | ') || ':*')
                ) >= :ts_rank_threshold
                $company_perm_sql
            ORDER BY ts_rank DESC
            LIMIT :max_results          
        "

        # Execute the SQL query and process the results
        db_foreach company $company_sql {
            # Build the search text for the company
            set search_text "${company_name} (${address_city}, ${address_postal_code}, ${address_country_code})"

            # Build the company data dictionary
            set company_data [dict create \
                address_city $address_city \
                address_postal_code $address_postal_code \
                address_country_code $address_country_code \
            ]

            # Add the company to the search results dictionary
            dict set search_results $company_id [dict create \
                object_name $company_name \
                object_type "im_company" \
                search_text $search_text \
                ts_rank $ts_rank \
                object_data $company_data \
            ]
        }

        return $search_results
    }

    ad_proc -public person {
        -user_id:required
        -query:required
        -max_results:required
    } {
        Search for persons based on the given query string.

        @param user_id The ID of the user performing the search.
        @param query The search query string.
        @param ts_rank_threshold The minimum ts_rank threshold for search results.

        @return A dictionary of search results, where the keys are person IDs and the values are dictionaries containing person details.
    } {
        # Initialize an empty dictionary to store the search results
        set search_results [dict create]

        # Definiere den Schwellenwert für den ts_rank
        set ts_rank_threshold [parameter::get_from_package_key -package_key "cognovis-core" -parameter "SearchTsRankThreshold" -default 0.03]

        # Build the SQL query to search for persons
        set person_sql "
            SELECT DISTINCT
                person_id,
                first_names,
                last_name,
                email,
                ts_rank(setweight(to_tsvector('english', first_names || ' ' || last_name), 'A') ||
                        setweight(to_tsvector('english', email), 'B'),
                        to_tsquery('english', replace(:query, ' ', ':* | ') || ':*')) AS ts_rank
            FROM cc_users
            WHERE to_tsvector('english', first_names || ' ' || last_name || ' ' || email) @@ 
                to_tsquery('english', replace(:query, ' ', ':* | ') || ':*')
                AND ts_rank(setweight(to_tsvector('english', first_names || ' ' || last_name), 'A') ||
                        setweight(to_tsvector('english', email), 'B'),
                        to_tsquery('english', replace(:query, ' ', ':* | ') || ':*')) >= :ts_rank_threshold
            ORDER BY ts_rank DESC
            LIMIT :max_results
        "

        # Execute the SQL query and process the results
        db_foreach person $person_sql {
            # Check if the user has read permission for the person
            im_user_permissions $user_id $person_id view read write admin
            if {$read} {
                # Build the search text for the person
                set search_text "$first_names $last_name (${email})"

                # Build the person data dictionary
                set person_data [dict create \
                    email $email \
                ]

                # Add the person to the search results dictionary
                dict set search_results $person_id [dict create \
                    object_name "$first_names $last_name" \
                    object_type "person" \
                    search_text $search_text \
                    ts_rank $ts_rank \
                    object_data $person_data \
                ]
            }
        }

        return $search_results
    }
    ad_proc -public im_invoice {
        -user_id:required
        -query:required
        -max_results:required
    } {
        Search for invoices based on the given query string.

        @param user_id The ID of the user performing the search.
        @param query The search query string.

        @return A dictionary of search results, where the keys are invoice IDs and the values are dictionaries containing invoice details.
    } {
        set search_results [dict create]

        set ts_rank_threshold [parameter::get_from_package_key -package_key "cognovis-core" -parameter "SearchTsRankThreshold" -default 0.03]

        set invoice_sql "
            SELECT DISTINCT
                i.invoice_id,
                i.invoice_nr,
                c.cost_type_id,
                im_name_from_id(i.company_contact_id) AS company_contact_name,
                im_category_from_id(c.cost_type_id) AS cost_type,
                im_name_from_id(c.customer_id) AS customer_name,
                im_name_from_id(c.provider_id) AS provider_name,
                ts_rank(to_tsvector('english', i.invoice_nr), to_tsquery('english', replace(:query, ' ', ':* | ') || ':*')) AS ts_rank
            FROM im_invoices i
            JOIN im_costs c ON i.invoice_id = c.cost_id
            WHERE to_tsvector('english', i.invoice_nr) @@ to_tsquery('english', replace(:query, ' ', ':* | ') || ':*')
                AND ts_rank(to_tsvector('english', i.invoice_nr), to_tsquery('english', replace(:query, ' ', ':* | ') || ':*')) >= :ts_rank_threshold
            ORDER BY ts_rank DESC
            LIMIT :max_results
        "

        db_foreach invoice $invoice_sql {
            cog::cost::permissions $user_id $invoice_id view read write admin
            if {$read} {
                if {[im_cost_type_is_invoice_or_quote_p $cost_type_id]} {
                    set company_name $customer_name
                } else {
                    set company_name $provider_name
                }
                set search_text "$invoice_nr ($company_name)"
                
                set invoice_data [dict create \
                    company_contact_name $company_contact_name \
                    cost_type $cost_type \
                    cost_type_id $cost_type_id \
                    customer_name $customer_name \
                    provider_name $provider_name \
                ]
                
                dict set search_results $invoice_id [dict create \
                    object_name $invoice_nr \
                    object_type "im_invoice" \
                    search_text $search_text \
                    ts_rank $ts_rank \
                    object_data $invoice_data \
                ]
            }
        }

        return $search_results
    }
}