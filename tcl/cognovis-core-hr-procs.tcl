ad_proc -public im_hr_or_supervisor_p {
    {-employee_id:required ""}
} {

    Checks permissions on visibility of HR sensitive data of a user. Usually HR department and the supervisors
    (from your direct manager up to the chief, following the supervisor_id hierarchy) are allowed to see HR sensitive
    data. This proc checks whether current_user_id is either in the HR group or in the hierarchy of supervisors
    from the employee to the CEO.

    @author Neophytos Demetriou

} {

    set current_user_id [auth::get_user_id]

    # check if current user in HR group

    if {[im_user_is_hr_p $current_user_id]} {return 1}

    # check if in hierarchy of supervisors
    # from the employee to the CEO

    set sql "
        with recursive nodes(parent_id, child_id) as (
            select supervisor_id,employee_id
            from im_employees
            where employee_id=:employee_id
            union all
            select supervisor_id,employee_id
            from im_employees e, nodes n
            where e.employee_id=n.parent_id
        ) select 1 from nodes where parent_id=:current_user_id
    "

    set supervisor_p [db_string check_hierarchy_p $sql -default 0]

    return $supervisor_p

}

ad_proc -public im_user_reports_component {
    {-user_id:required ""}
} {
    @author Neophytos Demetriou
} {

    set view_p [im_hr_or_supervisor_p -employee_id $user_id]

    if { !$view_p } {
        # we do not show the component if the current user
        # is not in the HR group (or the supervisor hierarchy)
        return ""
    }

    set params [list \
                    [list employee_id $user_id]]

    set result [ad_parse_template -params $params "/packages/intranet-hr/lib/user-reports"]
    return [string trim $result]

}

namespace eval cog::hr {
    ad_proc -public employee_info_component {
        -employee_id:required
        -return_url:required
        {-current_user_id ""}
        {-view_name ""}
    } {
        Returns the info_component for employees
    } {
        if {"" == $view_name} { set view_name "employees_view" }
        if {$current_user_id eq ""} {
            set current_user_id [auth::get_user_id]
        }
        
        set date_format "YYYY-MM-DD"
        set number_format "9999990D99"

        set department_url "/intranet-cost/cost-centers/new?cost_center_id="
        set user_url "/intranet/users/view?user_id="

        set td_class(0) "class=roweven"
        set td_class(1) "class=rowodd"

        # employee_id gets modified by the SQl ... :-(
        set org_employee_id $employee_id    

        # --------------- Security --------------------------

        set view 0
        set read 0
        set write 0
        set admin 0
        im_user_permissions $current_user_id $employee_id view read write admin
    
        if {!$read} { return "" }

        # Check if the current_user is a HR manager
        if {![im_permission $current_user_id view_hr]} { return "" }

        # Finally: Show this component only for employees
        # Fraber 130328: Now showing the portlet for all users,
        # because we need it to set the supervisor for freelancers (Emre)
        # if {![im_user_is_employee_p $employee_id] && ![im_profile::member_p -profile_id [im_profile_skill_profile] -user_id $employee_id]} {
        # cog_log Debug "im_employee_info_component: user is not an employee..."
        # return "" 
        # }

        # --------------- Select all values --------------------------

        if {[catch {db_1row employee_info "
            select	
                im_name_from_user_id(pe.person_id) as user_name,
                p.email,
                e.*,
                ci.*,
                to_char(ci.start_date,:date_format) as start_date_formatted,
                to_char(ci.end_date,:date_format) as end_date_formatted,
                to_char(e.birthdate,:date_format) as birthdate_formatted,
                to_char(salary, :number_format) as salary_formatted,
                to_char(hourly_cost, :number_format) as hourly_cost_formatted,
                to_char(other_costs, :number_format) as other_costs_formatted,
                to_char(insurance, :number_format) as insurance_formatted,
                to_char(social_security, :number_format) as social_security_formatted,
                u.user_id,
                cc.cost_center_name as department_name,
                im_name_from_user_id(e.supervisor_id) as supervisor_name
            from	
                users u
                LEFT OUTER JOIN im_employees e ON (u.user_id = e.employee_id)
                LEFT OUTER JOIN im_cost_centers cc ON (e.department_id = cc.cost_center_id)
                LEFT OUTER JOIN (
                    select	* 
                    from	im_costs c, im_repeating_costs rc
                    where	c.cost_id = rc.rep_cost_id
                        and c.cost_type_id in ([join [im_sub_categories [im_cost_type_employee]] ","])
                        and c.cause_object_id = :employee_id
                ) ci ON (u.user_id = ci.cause_object_id),
                parties p,
                persons pe
            where	
                pe.person_id = u.user_id
                and p.party_id = u.user_id
                and u.user_id = :employee_id

            "
        } err_msg]} {
            return "<b>Multiple Salary Items per User</b>:<br>
                This error is probably due to an incomplete update to version V3.2.<br>
                Please notify your System Administrator and tell him (or her) to<br>
                execute the script /packages/intranet-hr/sql/postgresql/update/upgrade-3.2.6.0.0-3.2.7.0.0.sql.
                <p><pre>$err_msg</pre>"
        } 

        regsub -all { } $salary_period {_} salary_period_key
        set salary_period [lang::message::lookup "" intranet-hr.Salary_period_${salary_period_key} $salary_period]
        
        set employee_info_exists 1

        set view_id [db_string get_view "select view_id from im_views where view_name=:view_name" -default 0]

        set column_sql "
            select	c.column_name,
                c.column_render_tcl,
                c.visible_for
            from	im_view_columns c
            where	c.view_id=:view_id
            order by sort_order"

        set employee_id $org_employee_id

        set employee_html "
            <form method=GET action=/intranet-hr/new>
            [export_vars -form {employee_id return_url}]
            <table cellpadding=1 cellspacing=1 border=0>
            <tr> 
            <td colspan=2 class=rowtitle align=center>[_ intranet-hr.Employee_Information]</td>
            </tr>\n"

        set ctr 1
        if {$employee_info_exists} {
            # if the row makes references to "private Note" and the user isn't
            # adminstrator, this row don't appear in the browser.
            db_foreach column_list_sql $column_sql {
                if {"" == $visible_for || [eval $visible_for]} {
                append employee_html "
                        <tr $td_class([expr {$ctr % 2}])>
                <td>[lang::message::lookup "" "intranet-hr.[lang::util::suggest_key $column_name]" $column_name] &nbsp;</td><td>"
                set cmd "append employee_html $column_render_tcl"
                eval $cmd
                append employee_html "</td></tr>\n"
                incr ctr
                }
            }
        } else {
            append employee_html "<tr><td colspan=2><i>[_ intranet-hr.Nothing_defined_yet]</i></tr></td>\n"
        }

        if {$write} {
            append employee_html "
            <tr $td_class([expr {$ctr % 2}])>
            <td></td><td><input type=submit value='[_ intranet-hr.Edit]'></td></tr>\n"
        }
        append employee_html "</table></form>\n"

        return $employee_html
    }
}

namespace eval cog::user {

    ad_proc -public get_portrait_id {
        -user_id:required
    } {
        Return the image_id of the portrait of a user, if it does not exist, return 0 

        @param user_id user_id of the user for whom we need the portrait
    } {
        return [util_memoize [list cog::user::get_portrait_id_not_cached -user_id $user_id] 600]
    }

    ad_proc get_portrait_id_not_cached {
        -user_id:required
    } {
        set item_id [db_string get_item_id {
            select max(c.item_id)
            from acs_rels a, cr_items c
            where a.object_id_two = c.item_id
            and a.object_id_one = :user_id
            and a.rel_type = 'user_portrait_rel'
        } -default 0]

        if {$item_id eq ""} { set item_id 0 }

        if {$item_id eq 0} {

            set develop_p [parameter::get_from_package_key -package_key "intranet-core" -parameter "TestDemoDevServer"]
            if {$develop_p && $item_id == 0} {
                set item_id [cog::anonymize::portrait -user_id $user_id]
            }
        }
        return $item_id
    }
}