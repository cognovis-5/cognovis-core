
ad_library {
    Procedures for supporting with timesheets
    @author malte.sussdorff@cognovis.de
}

namespace eval cog::timesheet::entry {
    ad_proc -public nuke {
        -hour_id:required
        -current_user_id:required
    } {
        Nukes a timesheet entry from the database
        
        Removes corresponding cost item as well

        @param hour_id HourId for the timesheet entry
    } {
        set valid_p [db_0or1row hours "select user_id, cost_id from im_hours where hour_id = :hour_id"]
        if {$valid_p} {
            if {$current_user_id ne $user_id && ![acs_user::site_wide_admin_p -user_id $current_user_id]} {
                if {![im_permission $current_user_id "add_hours_all"]} {
                    return
                }
            }
            db_dml hours_delete "
                delete  from im_hours
                where   hour_id = :hour_id
            "
            if {$cost_id ne ""} {
                im_exec_dml del_cost "im_cost__delete($cost_id)"
            }
        } else {
            return ""
        }
    }
}