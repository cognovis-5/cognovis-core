ad_library {
    cognovis specific procedures for reports
    
    @author malte.sussdorff@cognovis.de
}

namespace eval cog::report {
    ad_proc -public new {
        -report_name:required
        -report_sql:required
        {-report_type_id 15100}
        {-report_status_id 15000}
        {-report_sort_order 100}
        {-report_description ""}
        {-report_code ""}
        {-parent_menu_id ""}
        {-user_id ""}
        -overwrite:boolean
    } {
        Generate a new report of a certain name
    } {

		set package_name "cognovis-core"
		set label [im_mangle_user_group_name $report_name]
		set url [export_vars -base "/intranet-reporting/view" -url {report_id}]
        
        if {$parent_menu_id eq ""} {
            set parent_menu_id [db_string parent "select menu_id from im_menus m where m.label = 'reporting-other' limit 1"]
        }

        if {$report_code eq ""} {
            set report_code [cog_rest::helper::to_CamelCase -string $report_name]
        }

        set report_id [db_string report "select report_id from im_reports where report_code = :report_code" -default ""] 

        if {$overwrite_p && $report_id ne ""} {
            set menu_id [db_string menu_id "select report_menu_id from im_reports where report_id = :report_id" -default 0]
			db_string del_report "select im_report__delete(:report_id)"
			db_string del_report "select im_menu__delete(:menu_id)"
            set report_id ""
        }

        if {$report_id eq ""} {
            set report_id [db_nextval "acs_object_id_seq"]
            set report_menu_id [db_exec_plsql menu_new "
                    SELECT im_menu__new (
                            null,                   -- p_menu_id
                            'im_menu',              -- object_type
                            now(),                  -- creation_date
                            null,                   -- creation_user
                            null,                   -- creation_ip
                            null,                   -- context_id

                            :package_name,          -- package_name
                            :label,                 -- label
                            :report_name,           -- name
                            :url,                   -- url
                            :report_sort_order,	-- sort_order
                            :parent_menu_id,	-- parent_menu_id
                            null                    -- p_visible_tcl
                    )
            "]

            if {$user_id eq ""} {
                set user_id [auth::get_user_id]
            }

            db_exec_plsql create_report "
                SELECT im_report__new(
                    :report_id,
                    'im_report',
                    now(),
                    :user_id,
                    '[ad_conn peeraddr]',
                    null,

                    :report_name,
                    :report_code,
                    :report_type_id,
                    :report_status_id,
                    :report_menu_id,
                    :report_sql::text
                )
                "
        }

		db_dml edit_report "
			update im_reports set 
				report_sort_order = :report_sort_order,
				report_status_id = :report_status_id,
				report_type_id = :report_type_id,
				report_description = :report_description
			where report_id = :report_id
		"

		im_menu_update_hierarchy
        return $report_id
    }
}


namespace eval cog::report::column {
    ad_proc -public new {
        -report_id:required
        { -column_name ""}
        { -column_status_id 15050}
        { -column_type_id 15151}
        -variable_name:required
        { -sort_order 0}
        { -column_description ""}
    } {
        Creates a new report column

        @param report_id Report for which we define the column
        @param column_name Name of the column (Displayed header) - defaults to variable_name
        @param column_status_id Typically active 
        @param column_type_id Type of the column, defaults to string
        @param variable_name Name of the variable with the data
        @param sort_order Place of the column in the report table
        @param column_description Additional description for this column
    } {
        set creation_user [auth::get_user_id]
        if {$column_name eq ""} {
            set column_name $variable_name
        }
        set column_id [db_string create_column "select im_report_column__new(:report_id,:column_name,:column_status_id,
            :column_type_id,:sort_order,:variable_name,:column_description,:creation_user,'') from dual" -default ""]
        return $column_id
    }

    ad_proc -public type_from_variable {
        -variable_name
    } {
        Returns the column_type_id based of the variable_name
    } {

        # Check if this might be an array function
        if {[regexp "(.*)_ids" $variable_name match]} {
            set attribute_name [string range $variable_name 0 end-1]
            set array_p 1
        } else {
            set attribute_name $variable_name
            set array_p 0
        }

        
        if {![db_0or1row deref "select deref_plpgsql_function, a.datatype from acs_attributes a, im_dynfield_attributes da, im_dynfield_widgets dw
            where a.attribute_id = da.acs_attribute_id
            and da.widget_name = dw.widget_name
            and a.attribute_name = :attribute_name limit 1"]} {
            set deref_plpgsql_function ""
            set datatype [db_string datatype "select datatype from acs_attributes where attribute_name = :attribute_name" -default ""]
        }

        if {$deref_plpgsql_function ne ""} {
            switch $datatype {
                date - timestamp {
                    set column_type_id 15153
                }
                string - text - keyword {
                    set column_type_id 15151
                }
                float - number {
                    set column_type_id 15155
                }
                boolean {
                    set column_type_id 15158
                }
                default {
                    set column_type_sql "select category_id from im_categories where category_type = 'Intranet Report Column Type' and aux_string1 = :deref_plpgsql_function"
                    if {$array_p} {
                        append column_type_sql " and category like '%Array'"
                    } else {
                        append column_type_sql " and category not like '%Array'"
                    }
                    append column_type_sql "limit 1"
                    
                    set column_type_id [db_string column_type $column_type_sql -default ""]
                    if {$column_type_id eq ""} {
                        # NamedID
                        if {$array_p} {
                            set column_type_id 15160
                        } else {
                            set column_type_id 15150
                        }
                    }
                }
            }
        } else {
            switch $datatype {
                date - timestamp {
                    set column_type_id 15153
                }
                float - number {
                    set column_type_id 15155
                }
                boolean {
                    set column_type_id 15158
                }
                default {
                    set column_type_id 15151
                    if {[regexp "(.*)_amount" $variable_name match]} {
                        set column_type_id 15154
                    }
                    if {[regexp "(.*)_date" $variable_name match]} {
                        set column_type_id 15153
                    }                    
                    if {[regexp "(.*)_id" $variable_name match]} {
                        if {$array_p} {
                            set column_type_id 15160
                        } else {
                            set column_type_id 15150
                        }
                    }
                }
            }
        }
        
        return $column_type_id
    }

    
    ad_proc -public display_value {
        -value:required
        -column_type_id:required
        {-user_id ""}
    } {
        Returns the CACHED display value for a given value based of the column_type

        @param value value for which we need the display value
        @param column_type_id How do we get to the value
    } {
        if {$user_id eq ""} {
            set user_id [auth::get_user_id]
        }
        return [util_memoize [list cog::report::column::display_value_helper -value $value -column_type_id $column_type_id -user_id $user_id] 60]
    }

    ad_proc -public display_value_helper {
        -value:required
        -column_type_id:required
        -user_id:required
    } {
        Returns the display value for a given value based of the column_type

        @param value value for which we need the display value
        @param column_type_id How do we get to the value
    } {
        if {$value eq ""} {
            return ""
        }
        switch $column_type_id {
            15150 - 15160 {
                set is_category [db_string category_exsits "select 1 from im_categories where category_id = :value" -default 0]
                if {$is_category} {
                    set display_value [im_category_from_id $value]
                } else {
                    set display_value [im_name_from_id $value]
                }
            }
            15152 - 15162 {
                set display_value [im_category_from_id $value]
            }
            15156 - 15163 {
                set display_value [cog_rest::get::cognovis_object -rest_user_id $user_id -object_id $value]
            }
            default {
                set display_value $value
            }
        }

        return $display_value
    }
}