
ad_library {
    Rest Procedures for the cognovis-core package
    @author malte.sussdorff@cognovis.de
}

ad_proc -public cog_salutation {
    -person_id:required
} {
    Return the proper I18N of the salutation for the user
} {
    if {[db_0or1row person_info "select first_names, last_name, salutation_id from persons where person_id = :person_id"]} {
        set aux_string1 [db_string aux "select aux_string1 from im_categories where category_id= :salutation_id" -default ""]
        if {$aux_string1 eq ""} {
            # Default to last name
            set aux_string1 "Dear \$first_names,"
        }
        
        set salutation [im_category_from_id -current_user_id $person_id -locale [lang::user::site_wide_locale -user_id $person_id] $salutation_id]
        
        eval "set salutation_pretty \"$salutation $aux_string1\""
        return $salutation_pretty
    } else {
    	return ""
    }
}

ad_proc -public cog_project_managers {
    -project_id:required
} {
    Returns a list of project manager ids for the project. 

    @param project_id ProjectId for which we do the lookup

    @return pm_ids List of project manager ids
} {
    
    set pm_ids [db_list pm_s "select object_id_two from im_biz_object_members bom, acs_rels r where bom.rel_id = r.rel_id and object_id_one = :project_id and rel_type ='im_biz_object_member' and object_role_id = 1301"]
    lappend pm_ids [db_string project_lead "select project_lead_id from im_projects where project_id = :project_id"]
    return [lsort -unique $pm_ids]
}



# ---------------------------------------------------------------------
# Helper procs
# ---------------------------------------------------------------------

ad_proc get_value_if {someVar {default_value ""}} {
    @author Neophytos Demetriou (neophytos@azet.sk)
} {
    upvar $someVar var
    if {[info exists var]} { 
        return $var
    }
    return $default_value
}

ad_proc im_seconds_from_date {date} {
    @author Neophytos Demetriou (neophytos@azet.sk)
} {
    return [db_string seconds "select EXTRACT(EPOCH FROM TIMESTAMP WITH TIME ZONE :date)"]
}

ad_proc im_year_from_date {date} {
    @author Neophytos Demetriou (neophytos@azet.sk)
} {

    if { $date eq {} } {
        return
    }

    return [clock format [im_seconds_from_date $date] -format "%Y"]
}

ad_proc incr_if {varName expr} {
    @author Neophytos Demetriou (neophytos@azet.sk)
} {
    upvar $varName var
    if { [uplevel [list expr $expr]] } {
        incr var
    }
}

ad_proc im_coalesce {args} {
    @author Neophytos Demetriou (neophytos@azet.sk)
} {
    return [lsearch -inline -not $args {}]
}

ad_proc im_intersect3 {a b} {
    from tcl wiki (http://wiki.tcl.tk/283)
} {

    if {[llength $a] == 0} {
        return [list {} {} $b]
    }
    if {[llength $b] == 0} {
        return [list {} $a {}]
    }

    set res_i  {}
    set res_ab {}
    set res_ba {}

    foreach e $b {
        set ba($e) .
    }

    foreach e $a {
        set aa($e) .
    }

    foreach e $a {
        if {![info exists ba($e)]} {
            lappend res_ab $e
        } else {
            lappend res_i $e
        }
    }

    foreach e $b {
        if {![info exists aa($e)]} {
            lappend res_ba $e
        } else {
            lappend res_i $e
        }
    }

    list $res_i $res_ab $res_ba

}

ad_proc im_name_from_id {
    -cache:boolean
    object_id
} {
    Returns the name for an object
    
    @param cache Cache the result for 10 minutes
} {

    # we check to make sure the given id is an integer
    # as we don't db quote the value, if it's quoted
    # it just returns the provided value as is
    # if substituted, pl/pgsql figures out it's an 
    # integer and picks the right function (the one
    # that accepts an integer)
    if { ![string is integer -strict $object_id] } {
        error "object_id must be an integer value"
    }

    if {$cache_p} {
        set name [util_memoize [list db_string get_name_from_id "select im_name_from_id($object_id) as name" -default ""] 600]
    } else {
        set name [db_string get_name_from_id "select im_name_from_id($object_id) as name" -default ""]
        if {$name eq ""} {
            set object_type [acs_object_type $object_id]
            switch $object_type {
                content_item {
                    set name [content::item::get_title -item_id $object_id]
                    if {$name eq ""} {
                        set name [db_string item_name "select name from cr_items where item_id = :object_id" -default ""]
                    }
                }
            }
        }
    }

    return $name

}

ad_proc acs_object::update {
    -object_id:required
    { -user_id "" }
    { -context_id ""}
    { -title ""}
} {
    Update and objects information

    @param object_id Object to update
    @param user_id Set the last modified user. If not provided, default to currently logged in user
    @param context_id Context for the object 
    @param title Title of the object
} {
    if {$user_id eq ""} {
        set modifying_user [auth::get_user_id]
    } else {
        set modifying_user $user_id
    }
    set update_sql_list [list "last_modified = now()"]
    foreach key [list modifying_user context_id title] {
        set value [set $key]
        if {$value ne ""} {
            lappend update_sql_list "$key = :$key"
        }
    }

    db_dml update_object "update acs_objects set [join $update_sql_list ", "] where object_id = :object_id"
}

ad_proc -public im_biz_object_memberships {
    {-member_id:required}
} {
    Get the biz_object_groups a user is member of.
} {
    return [util_memoize [list im_biz_object_memberships_helper -member_id $member_id] 60]
}

ad_proc -public im_biz_object_memberships_helper {
    {-member_id:required}
} {
    Get the biz_object_groups a user is member of.
} {

    # Get the groups the owner belongs to
    set group_ids [db_list group_options "
        select  g.group_id
        from    groups g,
                    acs_objects o,
                acs_rels r
        where   g.group_id = o.object_id and
                o.object_type in ('im_profile', 'im_biz_object_group') and
                r.object_id_one = g.group_id and
                r.object_id_two = :member_id
        order by g.group_name
     "]
}

ad_proc -private im_where_from_criteria {
    criteria 
    {keyword "and"}
} {
    @last-modified 2014-11-24
    @last-modified-by Neophytos Demetriou (neophytos@azet.sk)
} {
    set where_clause ""
    if { $criteria ne {} } {
        set where_clause "\n\t${keyword} [join $criteria "\n\tand "]"
    }
    return $where_clause
}

ad_proc -public cog_log {
    { -object_id ""}
    -user_id
    severity
    message
} {
    Logs an error using the cognovis error handling
} {
    if {$message ne ""} {
        # Allow user_id to be empty
        if {![info exists user_id]} { set user_id [auth::get_user_id]}

        set message_pretty ""

        if {$object_id ne ""} {
            append message_pretty " - [im_name_from_id $object_id] ($object_id)" 
        }

        if {$message_pretty eq ""} {
            set message_pretty $message
        } else {
            append message_pretty ": $message"
        }

        if { [info commands cog::log::${severity}] ne "" } {
            cog::log::${severity} -object_id $object_id -user_id $user_id -message $message_pretty
        } else {
            cog::log::ns_log -severity $severity -message $message_pretty
        }
    }
}


namespace eval cog::log {
    ad_proc -public Error {
        -message:required
        {-object_id ""}
        {-user_id ""}
    } {
        Logs an error along with the object and user_id if possible
    } {

        cog::log::ns_log -severity "Error" -message $message -user_id $user_id

        # Send message to slack if enabled
        cog::slack::post_error -message $message -user_id $user_id

        # Send message to notifications in webix for the sysadmin


    }

    ad_proc -public ns_log {
        {-severity "Notice"}
        {-user_id ""}
        -message:required
    } {
        Wrapper around ns_log
        Adds an entry to the server log

        @severity Severity of the message
        @message Actual message
    } {
        if {[lsearch [ns_logctl severities] $severity]<0} {
            ::ns_log Error "Severity $severity is not supported"
            set severity "Error"
        }

        if {$user_id ne ""} {
            set message "[im_name_from_id $user_id] ($user_id): $message"
        }

        ::ns_log $severity "$message"
    }
}

namespace eval cog {
    ad_proc -public package_id {} {
        Returns the package_id of the cognovis core package
    } {
        set package_id [db_list select_package_id "
	        select package_id from apm_packages where package_key = 'cognovis-core'
        "]
        return $package_id
    }

    ad_proc -public url_with_query {
        {-url ""}
    } {
        Returns the url with the query appended
    } {
        if { $url eq "" } {
            set url [ns_conn url]
        }
        set query [export_vars -entire_form]
        if { $query ne "" } {
            append url "?$query"
        }
        return $url
    }

    ad_proc -public ad_hoc_query {
        {-format plain}
        {-report_name ""}
        {-border 0}
        {-col_titles {} }
        {-col_td_attributes {} }
        {-translate_p 1 }
        {-subtotals_p 0 }
        {-subtotals_rounding_factor 100.0 }
        {-package_key "intranet-core" }
        {-locale ""}
        {-skip_if_no_rows 0}
        sql
    } {
        Ad-hoc execution of SQL-Queries.
        @format "plain", "hmtl", "cvs", "json" or "xml" - select the output format. Default is "plain".
        @border Table border for HTML output
        @col_titles Optional titles for columns. Normally, columns are taken directly
        from the SQL query and passed through the localization subsystem.
        @translate_p Should the columns be translated?
        @package_key Default package for translated columns
    } {
        set result ""
        set bgcolor(0) " class=roweven "
        set bgcolor(1) " class=rowodd "
        
        regsub -all {[^0-9a-zA-Z]} $report_name "_" report_name_key

        set thousand_separator [lc_get -locale $locale "thousands_sep"]
        # ad_return_complaint 1 $thousand_separator

        # ---------------------------------------------------------------
        # Execute the report. As a result we get:
        #       - bind_rows with list of columns returned and
        #       - lol with the result set
        
        set bind_rows ""
        set err [catch {
            db_with_handle db {
                set selection [db_exec select $db query $sql]
                set lol [list]
                while { [db_getrow $db $selection] } {
                    set bind_rows [ns_set keys $selection]
                    set this_result [list]
                    for { set i 0 } { $i < [ns_set size $selection] } { incr i } {
                        lappend this_result [ns_set value $selection $i]
                    }
                    lappend lol $this_result
                }
            }
            db_release_unused_handles
        } err_msg]
        
        if {$err} {
            ad_return_complaint 1 "<b>Error executing sql statement</b>:
            <pre>$sql</pre>
            <pre>$err_msg</pre>\n"
            ad_script_abort
        }

        if {"" == $col_titles} { set col_titles $bind_rows }
        set header ""
        foreach title $col_titles {
            if {$translate_p} {
                regsub -all " " $title "_" title_key
            set key "$package_key.Ad_hoc"
            if {"" != $report_name} { set key "${key}_${report_name_key}" }
            set key "${key}_$title_key"
                set title [lang::message::lookup $locale $key $title]
            }
            switch $format {
                plain { append header "$title\t" }
                html { append header "<th>$title</th>" }
                csv { append header "\"$title\";" }
                xml { append header "<column>$title</column>\n" }
                json { append header "\"$title\"\n" }
            }
        }
        switch $format {
            plain { set header $header }
            html { set header "<tr class=rowtitle>\n$header\n</tr>\n" }
            csv { set header $header }
            xml { set header "" }
            json { set header "" }
        }
        
        set row_count 0
        foreach row $lol {

        set col_count 0
        set row_content ""
            foreach col $row {
            set col_name [lindex $col_titles $col_count]
            set col [string trim $col]
                switch $format {
                    plain { append result "$col\t" }
                    html {
                        if {"" == $col} { set col "&nbsp;" }
                set td_attributes [lindex $col_td_attributes $col_count]
                        append result "<td $td_attributes>$col</td>"
                    }
                    csv { append result "\"$col\";" }
                    xml { 
                append row_content "<$col_name>[ns_quotehtml $col]</$col_name>\n" 
            }
                    json {
                if {0 == $col_count} { set komma "" } else { set komma "," }
                regexp -all {\n} $col {\n} col
                regexp -all {\r} $col {} col
                append row_content "$komma\"$col_name\": \"[ns_quotehtml $col]\"" 
            }
                }

            if {$subtotals_p} {
            set sum 0
            if {[info exists subtotals($col_name)]} { set sum $subtotals($col_name) }
            set col [regsub -all {\&nbsp;} $col ""]
            set col [regsub -all {\,} $col ""]
            if {"" ne $col} {
                set col [regsub -all {\</[a-zA-Z]+\>} $col ""]
                set col [regsub -all {\<.?div.*?\>} $col ""]
                ns_log Notice "im_ad_hoc_query: col_name=$col_name,		col=$col"
                if {"" ne $sum && [regexp {^[0-9\,\.\-]+$} $col] && [string is double $col]} {
                set col [regsub -all $thousand_separator $col ""]
                if {"" eq $col} { set col 0.0 }
                set sum [expr $sum + $col]
                } else {
                set sum ""
                }
            }
            set subtotals($col_name) $sum
            }

            incr col_count
            }
        
            # Change to next line
            switch $format {
                plain { append result "\n" }
                html { append result "</tr>\n<tr $bgcolor([expr {$row_count % 2}])>" }
                csv { append result "\n" }
                xml { append result "<row>\n$row_content</row>\n" }
                json { 
            if {0 == $row_count} { set komma "" } else { set komma "," }
            append result "$komma\n{$row_content}" 
            }
            }
            incr row_count
        }

        set footer ""
        if {$subtotals_p} {
        set col_count 0
        foreach col_name $bind_rows {
            set subtotal ""
            if {[info exists subtotals($col_name)]} { set subtotal $subtotals($col_name) }
            set td_attributes [lindex $col_td_attributes $col_count]
            set subtotal_value ""
            if {"" ne [string trim $subtotal] && [string is double $subtotal]} { 
            set subtotal_value [expr round($subtotals_rounding_factor * $subtotal) / $subtotals_rounding_factor] 
            }
            append footer "<td $td_attributes><b>[lc_numeric $subtotal_value "" $locale]</b></td>"
            incr col_count
        }
        set footer "<tr>$footer</tr>\n"
        }

        if {$skip_if_no_rows && $row_count == 0} { return "" }
        switch $format {
            plain { 
            return "$header\n$result"  
        }
            html { 
            return "
                    <table border=$border>
                    $header
                    <tr $bgcolor(0)>
                    $result
                    </tr>
                    $footer
                    </table>
                "
            }
            csv { 
            return "$header\n$result"  
        }
            xml { 
            return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<result>\n$result\n</result>\n" 
        }
            json { 
            return $result 
        }
        }
    }

}