
ad_library {
    Procedures for supporting with materials 
    @author malte.sussdorff@cognovis.de
}
ad_proc -public im_uom_min_price {} {return 331}

namespace eval cog::material {
    ad_proc -public get_material_id {
        { -task_type_id "" }
        { -material_uom_id "" }
        { -file_type_id ""}
        { -material_type_id ""}
        { -material_status_id ""}
        { -source_language_id ""}
        { -target_language_id ""}
        { -subject_area_id ""}
    } {
        Returns the material_id 
        Will use im_trans_material in case we have source + target_language_id
        @param task_type_id
        @param task_uom_id
        @param file_type_id 
        @param source_language_id
        @param target_language_id Language we want to translate into
    } {
        set where_clause_list [list]
        foreach val [list task_type_id material_uom_id file_type_id source_language_id target_language_id subject_area_id] {
            if {[set $val] ne ""} {
                lappend where_clause_list "$val = :$val"
            } else {
                lappend where_clause_list "$val is null"
            }
        }

        foreach optional_val [list material_type_id material_status_id] {
            if {[set $optional_val] ne ""} {
                lappend where_clause_list "$optional_val = :$optional_val"
            }     
        }
    
        set material_id [db_string material "select material_id from im_materials
            where [join $where_clause_list " and "]
            order by material_id desc
            limit 1" -default ""]

        return $material_id
    }

    ad_proc -public create {
        -material_uom_id:required
        {-material_name ""}
        {-material_nr ""}
        {-material_type_id ""}
        {-material_status_id ""}
        {-description ""}
        {-material_billable_p "t"}
        {-source_language_id ""}
        {-target_language_id ""}
        {-subject_area_id ""}
        {-task_type_id ""}
        {-file_type_id ""}        
    } {
        Create a new material if it does not exist

        @param material_uom_id UOM for the material
        @param material_name Auto generated if not provided
        @param material_nr Auto generated material nr if not provided
        @param material_type_id Type of material. Will be set to auto generated if not provided
    } {
        
        if {$material_type_id eq ""} {
            if {$source_language_id ne "" || $target_language_id ne ""} {
                set material_type_id [im_material_type_translation]
            } else {
                set material_type_id [im_material_type_auto_gen]
            }
        }

        if {$material_status_id eq ""} {
            set material_status_id [im_material_status_active]   
        }

        if {$material_name eq ""} {
            set material_name_list [list]
            foreach var [list source_language_id target_language_id task_type_id subject_area_id file_type_id material_uom_id] {
                if {[set $var] ne ""} {
                    lappend material_name_list [im_name_from_id [set $var]]
                }
            }
            set material_name [join $material_name_list ", "]
        }
        if {$material_nr eq ""} {
            regsub -all { } [string trim [string tolower $material_name]] "" material_nr
        }
        
        set material_id [db_string existing_material "select material_id from im_materials where material_nr = :material_nr" -default ""]

    	if {$material_id eq ""} {
            if {[catch {set material_id [db_string material_create "  select im_material__new (  [db_nextval acs_object_id_seq], 'im_material', now(), [ad_conn user_id], '[ns_conn peeraddr]', null,  :material_name, :material_nr, :material_type_id, :material_status_id,  :material_uom_id, :description  )"]}]} {
                set material_id [db_string existing_material "select material_id from im_materials where material_nr = :material_nr" -default ""]
            }

        	db_dml update_material "update im_materials 
			    set source_language_id = :source_language_id,
				    target_language_id = :target_language_id,
				    task_type_id = :task_type_id,
				    subject_area_id = :subject_area_id,
                    material_billable_p = :material_billable_p,
                    file_type_id = :file_type_id
			    where material_id = :material_id"
        }
        return $material_id
    }

    ad_proc -public uom_id {
        -material_id:required
    } {
        Returns the unit of measure for a material

        @param material_id Material we look at
        @return material_uom_id 
    } {
        return [db_string get_uom_from_material "select material_uom_id from im_materials where material_id = :material_id" -default "[im_uom_unit]"]
    }
}
