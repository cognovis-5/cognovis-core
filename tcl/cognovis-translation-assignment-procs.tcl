ad_library {
    Procedures to handle translation assignments and packages
    @author malte.sussdorff@cognovis.de
}

namespace eval cog::trans::assignment {
    variable STATUS_CREATED 4220
    variable STATUS_REQUESTED 4221
    variable STATUS_ACCEPTED 4222
    variable STATUS_DENIED 4223
    variable STATUS_IN_PROGRESS 4224
    variable STATUS_DELIVERED 4225
    variable STATUS_DELIVERY_ACCEPTED 4226
    variable STATUS_DELIVERY_REJECTED 4227
    variable STATUS_ASSIGNED_OTHER 4228
    variable STATUS_PACKAGE_REQUESTED 4229
    variable STATUS_ASSIGNMENT_DELETED 4230
    variable STATUS_ASSIGNMENT_CLOSED 4231
}

#---------------------------------------------------------------
# Translation packages helper procs
#---------------------------------------------------------------


namespace eval cog::trans::packages {

    ad_proc -public previous_packages {
        -freelance_package_id:required
    } {
        Returns a list of directly previous freelance_package_ids. Who have the same tasks.

        @param freelance_package_id
    } {
        db_1row package_info "select project_id, package_type_id from im_freelance_packages where freelance_package_id = :freelance_package_id"
        set trans_task_ids [db_list tasks "select task_id from im_trans_tasks where project_id = :project_id"]
        set trans_task_type_ids [webix::assignments::trans_task_type_ids -trans_task_ids $trans_task_ids]
        
        set workflow_step_nr [lsearch $trans_task_type_ids $package_type_id]
        if {$workflow_step_nr eq 0} {
            # First step, no need to check for previous packages
            return ""
        } else {
            set previous_type_id [lindex $trans_task_type_ids [expr $workflow_step_nr -1]]
            set package_task_ids [db_list package_tasks "select trans_task_id from im_freelance_packages_trans_tasks where freelance_package_id = :freelance_package_id"]
            if {[llength $package_task_ids] > 0} {
                set previous_package_ids [db_list previous_packages "select distinct fp.freelance_package_id 
                    from im_freelance_packages fp, im_freelance_packages_trans_tasks fptt
                    where fp.freelance_package_id = fptt.freelance_package_id
                    and fp.freelance_package_id != :freelance_package_id
                    and fptt.trans_task_id in ([ns_dbquotelist $package_task_ids])
                    and fp.package_type_id = :previous_type_id"]
                return $previous_package_ids
            } else {
                return ""
            }
        }
    }


    ad_proc -public next_packages {
        -freelance_package_id:required
    } {
        Returns a list of directly next freelance_package_ids which have the same tasks.

        @param freelance_package_id Freelance package for which we need the next packages

        @return next_package_ids List of directly following packages.
    } {
        db_1row package_info "select project_id, package_type_id from im_freelance_packages where freelance_package_id = :freelance_package_id"
        set trans_task_ids [db_list tasks "select task_id from im_trans_tasks where project_id = :project_id"]
        set trans_task_type_ids [webix::assignments::trans_task_type_ids -trans_task_ids $trans_task_ids]
        
        set workflow_step_nr [lsearch $trans_task_type_ids $package_type_id]
        set next_type_id [lindex $trans_task_type_ids [expr $workflow_step_nr +1]]
        set package_task_ids [db_list package_tasks "select trans_task_id from im_freelance_packages_trans_tasks where freelance_package_id = :freelance_package_id"]
        if {[llength $package_task_ids] > 0} {
            set next_package_ids [db_list next_packages "select distinct fp.freelance_package_id 
                from im_freelance_packages fp, im_freelance_packages_trans_tasks fptt
                where fp.freelance_package_id = fptt.freelance_package_id
                and fp.freelance_package_id != :freelance_package_id
                and fptt.trans_task_id in ([ns_dbquotelist $package_task_ids])
                and fp.package_type_id = :next_type_id"]
            return $next_package_ids
        } else {
            return [list]
        }
    }

    ad_proc -public calculate_deadline {
        -freelance_package_id:required
        {-start_timestamp ""}
    } {
        Returns the deadline for a freelance_package_id. If it has assignments, use the closest deadline of the assignments.

        @param freelance_package_id PackageID for which to calculate the deadline
        @param start_timestamp Timestamp which we use to calculate the deadline from. Use previous steps end date or project start date if not provided
    } {
        set has_assignments [db_string assignments_p "select 1 from im_freelance_assignments where freelance_package_id = :freelance_package_id and assignment_status_id not in (4230,4223,4227,4228) limit 1" -default 0]
        if {$has_assignments} {
            set package_deadline [db_string min_deadline "select min(end_date) from im_freelance_assignments where freelance_package_id = :freelance_package_id" -default ""]
        } else {
            # Need to calculate the package deadline
            
            db_1row package_info "select project_id, package_type_id from im_freelance_packages where freelance_package_id = :freelance_package_id"
            set previous_package_ids [cog::trans::packages::previous_packages -freelance_package_id $freelance_package_id]
            if {$start_timestamp eq ""} {
                
                set start_timestamp [db_string start_date "select coalesce(start_date, now()) from im_projects where project_id = :project_id"]

                foreach previous_package_id $previous_package_ids {
                    # Deadline is calculated based of the previous deadlines if they are later than the project start. which they should :)
                    set previous_deadline [cog::trans::packages::calculate_deadline -freelance_package_id $previous_package_id]
                    if {$previous_deadline > $start_timestamp} {
                        set minutes_between_steps [parameter::get_from_package_key -package_key "webix-portal" -parameter "MinutesBetweenTransSteps" -default 0]
                        if {$minutes_between_steps eq 0} {
                            set start_timestamp $previous_deadline
                        } else {
                            set start_timestamp [db_string add_minutes "select timestamp '$previous_deadline' + interval '$minutes_between_steps minutes' from dual" -default $previous_deadline]
                        }
                    }
                }
            }
            
            # Now we know when to start, calculate the days and add them.
            # Then do additional logic.
            set processing_days [cog::trans::packages::processing_days -freelance_package_id $freelance_package_id]
      		set package_deadline "[im_translation_processing_deadline -start_timestamp $start_timestamp -days $processing_days]"
        }

        cog::callback::invoke cog::trans::packages::after_deadline_calculation -freelance_package_id $freelance_package_id -package_deadline $package_deadline
        return $package_deadline
    }



    ad_proc -public processing_days {
        -freelance_package_id:required
    } {
	    Returns the calculated duration of working days needed to finish the freelance_package_id

        @param freelance_package_id Package we want to get the uom for
        @return working days Number of working days
    } {
        set uom_id [cog::trans::packages::uom_id -freelance_package_id $freelance_package_id]
        set trans_task_ids [db_list trans_tasks "select trans_task_id from im_freelance_packages_trans_tasks where freelance_package_id = :freelance_package_id"]
        if {[llength $trans_task_ids] eq 0} {
            # No tasks => no processing_days
            return ""
        }

        switch $uom_id {
            320 {
                # Hour
                set units_per_day [parameter::get_from_package_key -package_key "intranet-timesheet2" -parameter "TimesheetHoursPerDay"]
            }
            321 {
                # Day
                set units_per_day 1
            }
            324 - 325 {
                # Words
                set units_per_day [db_string package_type "select aux_int1 from im_freelance_packages fp, im_categories c where fp.freelance_package_id = :freelance_package_id and fp.package_type_id = c.category_id"]
            }
            328 {
                # Week
                set working_days [parameter::get_from_package_key -package_key "intranet-timesheet2" -parameter "TimesheetWeeklyLoggingDays"]
                set units_per_day [expr 1 / [llength $working_days]]
            }
            330 {
                # Text
                set package_type [db_string package_type "select category from im_freelance_packages fp, im_categories c where fp.freelance_package_id = :freelance_package_id and fp.package_type_id = c.category_id"]
                switch $package_type {
                    trans - copy {
                        set units_per_day 1 
                    }
                    proof - edit {
                        set units_per_day 3
                    }
                    default {
                        set units_per_day ""
                    }
                }
            }
            default {
                # Can't compute
                return ""
            }
        }

        if {$units_per_day ne ""} {
            set days [db_string tu "select ceil(coalesce(sum(task_units),0)/:units_per_day) from im_trans_tasks where task_id in ([ns_dbquotelist $trans_task_ids])" -default 0]
        } else {
            set days "0"
        }

        return [format "%.0f" $days]
    }

    ad_proc -public uom_id {
        -freelance_package_id:required
    } {
        Returns the uom_id for a freelance_package_id based of the trans_tasks

        @param freelance_package_id Package we want to get the uom for
        @return uom_id Unit of Measure category

        @error Returns nothing if we can't find a task or if we have multiple task_uom_id (which should not be the case)
    } {
        set trans_task_ids [db_list trans_tasks "select trans_task_id from im_freelance_packages_trans_tasks where freelance_package_id = :freelance_package_id"]

        if {[llength $trans_task_ids] eq 0} {
            return ""
        }

        set task_uom_ids [db_list task_uoms "select distinct task_uom_id from im_trans_tasks where task_id in ([ns_dbquotelist $trans_task_ids])"]

        if {[llength $task_uom_ids] eq 1} {
            return $task_uom_ids
        } else {
            return ""
        }

    }
    
    ad_proc -public trans_task_units {
        { -freelance_package_id "" }
        { -trans_task_ids "" }
        { -package_type_id "" }
        { -object_id "" }
    } {
        Return the task_units for a freelance_package_id or bunch of trans_task_ids

        @param freelance_package_id Package for which we need the units
        @param trans_task_ids List of task_ids for which we calculate the units
        @param object_id Object for which we calculate the task_units. Might be the freelancer, the customer or the project
    } {
        if {$freelance_package_id ne ""} {
            set trans_task_ids [db_list trans_tasks "select trans_task_id from im_freelance_packages_trans_tasks where freelance_package_id = :freelance_package_id"]
            set package_type_id [db_string package_type "select package_type_id from im_freelance_packages where freelance_package_id = :freelance_package_id"]
        }

        if {$trans_task_ids eq ""} {
            return ""
        }

        set total_units 0
        
        if {$object_id eq "" || $package_type_id eq ""} {
            set total_units [db_string task_units "select sum(task_units) from im_trans_tasks where task_id in ([ns_dbquotelist $trans_task_ids])" -default 0]
        } else {
            set task_type [im_category_from_id -translate_p 0 $package_type_id]

            if {$object_id eq ""} {
                set object_id [im_company_freelance]
            }

            foreach trans_task_id $trans_task_ids {
                db_1row task_matrix "select match_x,match_rep,match100,match95,match85,match75,match50,match0,
						match_perf,match_cfr,match_f95,match_f85,match_f75,match_f50,locked, task_units
					from im_trans_tasks where task_id = :trans_task_id"
                    
		        set trans_task_units [im_trans_trados_matrix_calculate $object_id \
					$match_x $match_rep $match100 $match95 $match85 $match75 $match50 $match0 \
					$match_perf $match_cfr $match_f95 $match_f85 $match_f75 $match_f50 $locked $task_type]

                if {$trans_task_units < 1} {
                    set trans_task_units $task_units
                }
                set total_units [expr $total_units + $trans_task_units]
            }
        }
        return [format "%.0f" $total_units]
    }


    ad_proc -public create {
        { -trans_task_ids "" }
        { -target_language_id ""}
        { -project_id "" }
        { -package_type_id "" }
        { -package_name "" }
        { -user_id "" }
        -overwrite:boolean
    } {
        Generates one (or more) packages for the task_ids if not already assigned to a package.

        Assumes we want to generate one package per target language (for the time being)

        @param trans_task_ids List of trans task_ids which we want to generate a package
        @param target_language_id Target language for which we need to generate the pacakge in case of no trans_task_ids
        @param project_id Project in which we want to add the packge. Might be deferred from the task_ids
        @param package_type_id Task Type for which we want to generate the package. If empty, generate packages for all types of the project
        @param overwrite if set, we will overwrite existing package assignments (if possible). Otherwise we only generate a package for the not already assigned tasks.
        @param user_id Who is generating the packages

        @return package_ids List of package_ids generated
    } {
        # We need to generate the freelance_package

        if {$project_id eq ""} {
        	set p_task_id [lindex $trans_task_ids 0]
	        set project_id [db_string project_id "select project_id from im_trans_tasks where task_id = :p_task_id" -default ""]
        }

        if {$project_id eq ""} {
            ns_log Error "We can't generate the packages for $trans_task_ids .. could not find a project"
            return ""
        }        

        if {$user_id eq ""} { set user_id [auth::get_user_id]}
        set ip_address [ad_conn peeraddr]

        set package_type_ids [webix::assignments::trans_task_type_ids -project_id $project_id -trans_task_ids $trans_task_ids]

        if {$package_type_id ne ""} {
            # Check if the package_type_id is valid
            if {[lsearch $package_type_ids $package_type_id]<0} {
                # not a valid package_type, return
                ns_log Error "Attempt to create a package of type $package_type_id for trans_task_id $trans_task_ids, which is not valid"
                return
            } else {
                set package_type_ids $package_type_id
            }
        }

        set package_ids [list]

        foreach package_type_id $package_type_ids {

            # Check that we can actually create a new package as we have tasks to create with left
            
            if {$overwrite_p} {
                # We need to prevent overwriting of packages with PO
                set freelance_packages_with_po  [db_list po_created "select fa.freelance_package_id
                    from im_freelance_assignments fa, im_freelance_packages fp
                    where fa.freelance_package_id = fp.freelance_package_id
                    and fp.project_id = :project_id
                    and fa.assignment_status_id <> 4230
                    and fa.purchase_order_id is not null"]
                
                set exclude_package_ids $freelance_packages_with_po
            } else {
                set project_freelance_packages [db_list project_freelance_packages "select freelance_package_id from im_freelance_packages 
                    where project_id = :project_id
                    and package_type_id = :package_type_id"]
                
                set exclude_package_ids $project_freelance_packages
            }

            if {[llength $trans_task_ids] >0 } {
                set assignable_task_sql "select task_id from im_trans_tasks where task_id in ([ns_dbquotelist $trans_task_ids])"
                if {$exclude_package_ids ne ""} {
                    append assignable_task_sql " and task_id not in (
                            select trans_task_id from im_freelance_packages_trans_tasks where freelance_package_id in ([ns_dbquotelist $exclude_package_ids]))"
                }
                set assignable_task_ids [db_list assignable_task_ids $assignable_task_sql]
            } else {
                set assignable_task_ids [list]
            }            

            if {$overwrite_p} {
                foreach trans_task_id $assignable_task_ids {
                    # Delete first so we don't get the same package_name back, as it would find 
                    # the package name (which exists) for the trans_task_ids
                    db_dml delete_mapping "delete from im_freelance_packages_trans_tasks 
                        where trans_task_id = :trans_task_id 
                        and freelance_package_id in (
                            select freelance_package_id from im_freelance_packages where package_type_id = :package_type_id and project_id = :project_id
                        )"
                }
            }


            #---------------------------------------------------------------
            # Assume one package per target language
            #---------------------------------------------------------------
            if {$target_language_id eq ""} {
                if {[llength $assignable_task_ids] >0} {
                    set target_language_ids [db_list target_languages "select distinct target_language_id from im_trans_tasks 
                        where task_id in ([ns_dbquotelist $assignable_task_ids])"]
                } else {
                    set target_language_ids [db_list target_languages "select language_id from im_target_languages where project_id = :project_id"]
                }
            } else {
                set target_language_ids $target_language_id
            }

            foreach target_language_id $target_language_ids {

                if {[llength $target_language_ids] >1} {
                    if {$package_name eq ""} {
                        set package_name [cog::trans::packages::package_name -project_id $project_id -package_type_id $package_type_id -target_language_id $target_language_id]
                    } else {
                        set package_name "${package_name}"
                    }
                } else {
                    if {$package_name eq ""} {
                        set package_name [cog::trans::packages::package_name -project_id $project_id -package_type_id $package_type_id]
                    }
                }

                set freelance_package_id [db_string exists "select freelance_package_id from im_freelance_packages 
                    where package_type_id = :package_type_id
                    and project_id = :project_id
                    and freelance_package_name = :package_name 
                    and target_language_id = :target_language_id limit 1" -default ""]

                if {$freelance_package_id eq ""} {
                    set freelance_package_id [db_string new_package "select im_freelance_package__new(
                        :user_id,		-- creation_user
                        :ip_address,		-- creation_ip
                        :package_type_id,
                        null,
                        :project_id,
                        :package_name,
                        :target_language_id)"]
                }

                if {[llength $assignable_task_ids]>0} {
                    set package_task_ids [db_list target_languages "select task_id from im_trans_tasks 
                        where task_id in ([ns_dbquotelist $assignable_task_ids])
                        and target_language_id = :target_language_id"]


                    foreach trans_task_id $package_task_ids {
                        # Delete first if overwrite
                        if {$overwrite_p} {
                            db_dml delete_mapping "delete from im_freelance_packages_trans_tasks 
                                where trans_task_id = :trans_task_id 
                                and freelance_package_id in (
                                    select freelance_package_id from im_freelance_packages where package_type_id = :package_type_id and project_id = :project_id
                                )"
                        }

                        db_dml add_mapping "insert into im_freelance_packages_trans_tasks (freelance_package_id,trans_task_id) values (:freelance_package_id, :trans_task_id)"
                    }
                }

                catch {
                    cog::callback::invoke cog::trans::packages::freelance_after_create -trans_task_ids $package_task_ids -freelance_package_id $freelance_package_id
                }

                lappend package_ids $freelance_package_id
            }
        }
        return $package_ids
    }

    ad_proc -public package_name {
        -project_id:required
        { -target_language_id ""}
        -package_type_id:required
    } {
        Return the package name for the task_type_ids and the task_type_id

        @param project_id Project for which we create the name
        @param target_language_id Language for which we want to create the package, if any
        @param package_type_id Package types for which to return the name
    } {
        # Check if we already have package_names for the tasks.
        set counter [db_string counter "select count(*) from im_freelance_packages where package_type_id = :package_type_id and project_id = :project_id and target_language_id = :target_language_id" -default 0]

        if {$counter eq 1} {
            set package_name [db_string package_names "select distinct freelance_package_name from im_freelance_packages fp 
            where fp.project_id = :project_id and package_type_id = :package_type_id and target_language_id = :target_language_id"]
        } else {
            # We have either no package_name, then we need to generate it.
            # Alternative we have more than one, then we need a new one if we want to assign the tasks in a package

            if {![db_0or1row prefix "select aux_string1 as type_prefix, category from im_categories where category_id = :package_type_id and category_type = 'Intranet Trans Task Type'"]} {
                # Invalid package_type
                ns_log Error "Invalid category for trans tasks $package_type_id"
                return ""
            }
            if {$type_prefix eq ""} {
                db_1row project_name_and_language "select project_name from im_projects p where project_id = :project_id"
                set type_prefix "${project_name}_$category"
            }
            
            if {$target_language_id ne ""} {
                set type_prefix "${type_prefix}_[im_name_from_id $target_language_id]"
            }

            set exists_p 1
            while {$exists_p > 0 } {
                incr counter
                set package_name "${type_prefix}_$counter"
                set exists_p [db_string package_name_exists "select 1 from im_freelance_packages where freelance_package_name = :package_name and package_type_id = :package_type_id and project_id = :project_id limit 1" -default 0]
            }
        }
        return $package_name
    }

    ad_proc -public nuke {
        -freelance_package_id:required 
    } {
        Utterly removes a package and it's assignments from the database
    } {
        foreach assignment_id [db_list assignments "select assignment_id from im_freelance_assignments where freelance_package_id = :freelance_package_id"] {
            webix::assignments::nuke -assignment_id $assignment_id
        }
        db_1row delete_package "select im_freelance_package__delete(:freelance_package_id)"
    }
}