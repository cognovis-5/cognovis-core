
ad_library {
    Support Procedures for handling json using chilkat
    @author malte.sussdorff@cognovis.de
}

namespace eval cog::json {
    ad_proc -public object {
        -elements:required
        { -user_id "" }
    } {
        Returns a formated JSON line for the documented return json "Object"

        @param elements List of lists with name, value, type of the element.
        @param user_id User to whom the object is returned. Defaults to currently logged in user.

    	@return Returns the formatted json string for the object
    } {
        set jsonObj [new_CkJsonObject]
        if {$user_id eq ""} {
            set user_id [auth::get_user_id]
        }

        foreach element $elements {
            set name [lindex $element 0]
            set type [lindex $element 1]
            set value [lindex $element 2]

            regsub -all {\n} $value {} value
            regsub -all {\r} $value {} value
            if {$value eq ""} {
                switch $type {
                    string {
                        CkJsonObject_AddStringAt $jsonObj -1 $name ""
                    }
                    default {
                        CkJsonObject_AddNullAt $jsonObj -1 $name
                    }
                }
                continue
            } 

            switch $type {
                integer {
                    CkJsonObject_AddIntAt $jsonObj -1 $name $value
                }

                number {
                    # We might need to do a conversion integer vs. numeric....
                    CkJsonObject_AddNumberAt $jsonObj -1 $name $value
                }
                boolean {
                    if {[string is true $value]} {
                        CkJsonObject_AddBoolAt $jsonObj -1 $name 1
                    } else {
                        CkJsonObject_AddBoolAt $jsonObj -1 $name 0						
                    }
                }
                string {
                    CkJsonObject_AddStringAt $jsonObj -1 $name $value
                }
                category {
                    # We only deal with a single category, so only return the first value
                    set category_id [lindex $value  0]

                    set catObj [new_CkJsonObject]
                    CkJsonObject_AddIntAt $catObj -1 "id" $category_id
                    CkJsonObject_AddStringAt $catObj -1 "name" [im_category_from_id -current_user_id $user_id $category_id]
                    CkJsonObject_AddStringAt $catObj -1 "icon_or_color" [im_category_icon_or_color -category_id $category_id]
                    CkJsonObject_AddObjectCopyAt $jsonObj -1 $name $catObj
                    delete_CkJsonObject $catObj
                }
                category_array {
                    # we return an object for the category
                    CkJsonObject_AddArrayAt $jsonObj -1 $name
                    set catArr [CkJsonObject_ArrayAt $jsonObj [expr [CkJsonObject_get_Size $jsonObj] - 1]]
                    
                    foreach category_id $value  {
                        CkJsonArray_AddObjectAt $catArr -1
                        set catObj [CkJsonArray_ObjectAt $catArr [expr [CkJsonArray_get_Size $catArr] - 1]]

                        CkJsonObject_AddIntAt $catObj -1 "id" $category_id
                        CkJsonObject_AddStringAt $catObj -1 "name" [im_category_from_id -current_user_id $user_id $category_id]
                        CkJsonObject_AddStringAt $catObj -1 "icon_or_color" [im_category_icon_or_color -category_id $category_id]
                        
                        delete_CkJsonObject $catObj
                    }
                    delete_CkJsonArray $catArr
                }
                named_id {
                    if { [string is integer -strict $value] } {                    
                        set deref [im_name_from_id $value]
                    } {
                        set deref $value
                        set value ""
                    }
                    set objObj [new_CkJsonObject]
                    CkJsonObject_AddIntAt $objObj -1 "id" $value
                    CkJsonObject_AddStringAt $objObj -1 "name" $deref

                    CkJsonObject_AddObjectCopyAt $jsonObj -1 $name $objObj
                    delete_CkJsonObject $objObj
                }
                object {
                    set objObj [new_CkJsonObject]
                    CkJsonObject_Load $objObj $value
                    CkJsonObject_AddObjectCopyAt $jsonObj -1 $name $objObj
                    delete_CkJsonObject $objObj
                }
                object_array {		
                    CkJsonObject_AddArrayAt $jsonObj -1 $name
                    set objArr [CkJsonObject_ArrayAt $jsonObj [expr [CkJsonObject_get_Size $jsonObj] - 1]]
                    foreach object_id $value  { 
                        CkJsonArray_AddObjectAt $objArr -1
                        set objObj [CkJsonArray_ObjectAt $objArr [expr [CkJsonArray_get_Size $objArr] - 1]]
                        CkJsonObject_AddIntAt $objObj -1 "id" $object_id
                        CkJsonObject_AddStringAt $objObj -1 "name" [im_name_from_id $object_id]
                        delete_CkJsonObject $objObj
                    }
                    delete_CkJsonArray $objArr
                }
                json_array {
                    # Build the array if it is properly define
                    # The calling environment should have a list named so it behaves just like return_array.
                    regsub -all {\\n} $value {} value
                    CkJsonObject_AddArrayAt $jsonObj -1 $name
                    set objArr [CkJsonObject_ArrayAt $jsonObj [expr [CkJsonObject_get_Size $jsonObj] - 1]]
                    foreach return_object $value {
                        set objObj [new_CkJsonObject]
                        CkJsonObject_Load $objObj $return_object
                        CkJsonArray_AddObjectCopyAt $objArr -1 $objObj
                        delete_CkJsonObject $objObj
                    }
                    delete_CkJsonArray $objArr
                }
                json_object {
                    # Don't quote
                    set Obj [new_CkJsonObject]
                    CkJsonObject_Load $Obj $value
                    CkJsonObject_AddObjectCopyAt $jsonObj -1 $name $Obj
                    delete_CkJsonObject $Obj
                }
                cr_file {
                    # Get the cr_file info. 
                    set item_id [content::revision::item_id -revision_id $value]
                    if {$item_id eq ""} {
                        set revision_id $value
                    } else {
                        set revision_id [content::item::get_best_revision -item_id $item_id]
                    }

                    db_1row revision_info "select title, content_length as size, content as cr_path  from cr_revisions where revision_id = :revision_id"

                    set crObj [new_CkJsonObject]
                    CkJsonObject_AddIntAt $crObj -1 "id" $revision_id
                    CkJsonObject_AddStringAt $crObj -1 "name" $title
                    CkJsonObject_AddStringAt $crObj -1 "sname" $cr_path
                    CkJsonObject_AddStringAt $crObj -1 "status" "server"
                    CkJsonObject_AddStringAt $crObj -1 "sizetext" "[expr $size/1024] Kb"
                    CkJsonObject_AddObjectCopyAt $jsonObj -1 $name $crObj
                    delete_CkJsonObject $crObj
                }
                cr_file_array {
                    CkJsonObject_AddArrayAt $jsonObj -1 $name
                    set crArr [CkJsonObject_ArrayAt $jsonObj [expr [CkJsonObject_get_Size $jsonObj] - 1]]
                    foreach id $value  {
                        # Get the cr_file info. 
                        set item_id [content::revision::item_id -revision_id $id]
                        if {$item_id eq ""} {
                            set revision_id $id
                        } else {
                            set revision_id [content::item::get_best_revision -item_id $item_id]
                        }

                        db_1row revision_info "select title, content_length as size  from cr_revisions where revision_id = :revision_id"
                        
                        CkJsonArray_AddObjectAt $crArr -1
                        set crObj [CkJsonArray_ObjectAt $crArr [expr [CkJsonArray_get_Size $crArr] - 1]]
                        CkJsonObject_AddIntAt $crObj -1 "id" $revision_id
                        CkJsonObject_AddStringAt $crObj -1 "name" $title
                        CkJsonObject_AddStringAt $crObj -1 "sname" $revision_id
                        CkJsonObject_AddStringAt $crObj -1 "status" "server"
                        CkJsonObject_AddNumberAt $crObj -1 "size" $size
                        delete_CkJsonObject $crObj
                    }
                }
                default {
                    # Quote by default 
                    CkJsonObject_AddStringAt $jsonObj -1 $name $value
                }
            }
        }        
    	return 	[CkJsonObject_emit $jsonObj]
    }
}
