
ad_library {
    Components moved from other packages for the cognovis-core package
    @author malte.sussdorff@cognovis.de
}

ad_proc cog_project_finance_component {
    {-show_details_p 1}
    {-show_summary_p 1}
    {-show_admin_links_p 1}
    {-no_timesheet_p 0}
    {-view_name ""}
    {-disable_view_standard_p 0}
    {-disable_view_finance_p 0}
    {-user_id ""}
    {-return_url ""}
    -project_id
} {
    Returns a HTML table containing a detailed summary of all
    financial activities of the project. <p>

    ToDo:
    - Add DynView dynamic columns and remove parameters

    The complexity of this component comes from several sources:
    <ul>
    <li>We need to sum up the financial items and sort them into
        several
        "buckets" that correspond to the different cost types
        such as "customer invoices", "provider purchase orders",
        internal "timesheet costs" etc.
    <li>We can have costs and financial documents (see doc.) in
        several currencies, so we can't just add these together.
        Instead, we need to maintain separate sums per cost type
        and currency.
        Also, costs may have NULL cost values (timesheet costs
        from persons whithout the "hourly cost" defined).
    </ul>

} {
    if {$user_id eq ""} {
        set user_id [ad_conn user_id]
    }


    if {$return_url eq ""} {
        set return_url [cog::url_with_query]
    }
    set params [list [list user_id $user_id] [list project_id $project_id] [list show_details_p $show_details_p] [list show_summary_p $show_summary_p] [list show_admin_links_p $show_admin_links_p] [list no_timesheet_p $no_timesheet_p] [list view_name $view_name] [list disable_view_standard_p $disable_view_standard_p] [list disable_view_finance_p $disable_view_finance_p] [list return_url $return_url]]
    set result [ad_parse_template -params $params "/packages/cognovis-core/lib/project-finance"]
    return [string trim $result]
}
