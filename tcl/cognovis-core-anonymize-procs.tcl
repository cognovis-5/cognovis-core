ad_library {

    Anything to anonymize the system

    @author Malte Sussdorff ( malte.sussdorff@cognovis.de )
    @creation-date 2022-01-30
}
package require textutil


namespace eval cog::anonymize {
    ad_proc -public all {} {
        Anonymize the whole dataset
    } {
        cog::anonymize::cleanup
        cog::anonymize::notes
        cog::anonymize::persons
        cog::anonymize::offices
        cog::anonymize::companies
        cog::anonymize::projects
        cog::anonymize::finance
    }
    
    ad_proc -public cleanup {
    } {
        Cleanup data before anonymizing
    } {
        set current_user_id [auth::get_user_id]
        if {![acs_user::site_wide_admin_p -user_id $current_user_id]} {
            return ""
        }
        
        # Reset parties for offices and companies
        db_dml reset_parties "update parties set email = null, signature = null, url = null where party_id not in (select person_id from persons)"
        db_dml nuke_no_person_users "delete from users where user_id not in (select person_id from persons)"

        # Delete non main offices
        foreach office_id [db_list no_main_offices "select office_id from im_offices where office_id not in (select main_office_id from im_companies)"] {
            cog::office::nuke -current_user_id $current_user_id -office_id $office_id
        } 

        db_dml delete_fs_actions "delete from im_fs_actions"

        db_dml parameters {
            update apm_parameter_values set attr_value = null where parameter_id in (select parameter_id from apm_parameters where parameter_name like 'Office365%');
            update apm_parameter_values set attr_value = null where parameter_id in (select parameter_id from apm_parameters where parameter_name like '%Password');
        }

        db_dml delete_contacts "delete from users_contact where user_id not in (select user_id from users)"

        # ---------------------- im_trans_prices -------------------------------
        if {[im_table_exists users_office365]} {
            db_dml delete_tokens "delete from users_office365"
        }

        db_dml im_cost_centers_update {
            update im_cost_centers set
                cost_center_name = translate(cost_center_name, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC0123456789', 'xa2zzbwiwiqihhasdgk7bbkjhhLQIKIIJJUUQQOOIIPSSPZZFFTZZ4578906123'),
                cost_center_label = translate(cost_center_label, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC0123456789', 'xa2zzbwiwiqihhasdgk7bbkjhhLQIKIIJJUUQQOOIIPSSPZZFFTZZ4578906123'),
                note = translate(note, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC0123456789', 'xa2zzbwiwiqihhasdgk7bbkjhhLQIKIIJJUUQQOOIIPSSPZZFFTZZ4578906123'),
                description = translate(description, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC0123456789', 'xa2zzbwiwiqihhasdgk7bbkjhhLQIKIIJJUUQQOOIIPSSPZZFFTZZ4578906123')
        }

        db_dml anonymize {
            update acs_events set description =  translate(description, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC', 'xaabbawiwiqihhasdgkksskjhhLLKKJJJJUUUUOOOOPPPPZZZZZZZ'), name = translate(name, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC', 'xaaaaawiwiqihhasdgkkkkkjhhLLKKJJJJUUUUOOOOPPPPZZZZZZZ'), related_link_text = translate(related_link_text, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC', 'xaaaaawiwiqihhasdgkkkkkjhhLLKKJJJJUUUUOOOOPPPPZZZZZZZ');
            update acs_mail_log set subject = translate(subject, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC', 'xaaaaawiwiqihhasdgkkkkkjhhLLKKJJJJUUUUOOIIPPPPZZTTTZZ');
            update acs_mail_log set body = translate(body, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC', 'xaaaaawiwiqihhasdgkkkkkjhhLLKKJJJJUUUUOOIIPPPPZZTTTZZ');
            update cr_revisions set content = translate(content, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC', 'xaaaaawiwiqihhasdgkkkkkjhhLLKKJJJJUUUUOOIIPPPPZZTTTZZ'), description = translate(description, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC', 'xaaaaawiwiqihhasdgkkkkkjhhLLKKJJJJUUUUOOIIPPPPZZTTTZZ'), title = translate(title, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC', 'xaaaaawiwiqihhasdgkkkkkjhhLLKKJJJJUUUUOOIIPPPPZZTTTZZ');
            update cr_items set name = item_id || ' - ' || translate(name, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC', 'xaaaaawiwiqihhasdgkkkkkjhhLLKKJJJJUUUUOOIIPPPPZZTTTZZ');
            update acs_objects set title = translate(title, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC', 'xaaaaawiwiqihhasdgkkkkkjhhLLKKJJJJUUUUOOIIPPPPZZTTTZZ');
        }

        db_dml freelancers {delete from im_freelancers}
        db_dml collmex {
            delete from collmex_accdoc;
            delete from collmex_accbal;
            delete from collmex_accounts;
        }

        foreach portrait_rel_id [db_list rel_ids "select rel_id
            from acs_rels a, cr_items c
            where a.object_id_two = c.item_id
            and a.rel_type = 'user_portrait_rel'"] {
                db_1row delete_portrait_rel "select acs_rel__delete(:portrait_rel_id)"
            }

        db_dml templates {
            delete from im_categories where enabled_p = 'f' and category_type = 'Intranet Cost Template';
        }
    }

    ad_proc -public notes {} {
        Anonymize notes and descriptions
    } {
        set quantity [db_string notes "select count(*) from im_notes" -default 0]
        set lorems [cog::randommer::LoremIpsum -quantity $quantity]
        set ctr 0
        foreach note_id [db_list notes "select note_id from im_notes where note is not null"] {
            set note [lindex $lorems $ctr]
            db_dml update_note "update im_notes set note = :note where note_id = :note_id"
            incr ctr
        }

        set sentences [list]
        foreach paragraph $lorems {
            foreach sentence [split $paragraph {.}] {
                if {[string trim $sentence] ne ""} {
                    lappend sentences [string trim $sentence]
                }
            }
        }

        set ctr 0
        foreach topic_id [db_list forums "select topic_id from im_forum_topics where message is not null or subject is not null"] {
            set sentence [lindex $sentences $ctr]
            db_dml update_topic "update im_forum_topics set subject = :sentence, message=:sentence where topic_id = :topic_id"
            incr ctr
        }

        foreach hour_id [db_list forums "select hour_id from im_hours where note is not null"] {
            set sentence [lindex $sentences $ctr]
            db_dml update_topic "update im_hours set note = :sentence where hour_id = :hour_id"
            incr ctr
        }

        foreach user_id [db_list forums "select user_id from users_contact where note is not null"] {
            set sentence [lindex $sentences $ctr]
            db_dml update_topic "update users_contact set note = :sentence where user_id = :user_id"
            incr ctr
        }

        foreach office_id [db_list forums "select office_id from im_offices where note is not null"] {
            set sentence [lindex $sentences $ctr]
            db_dml update_topic "update users_contact set note = :sentence where office_id = :office_id"
            incr ctr
        }

        foreach absence_id [db_list forums "select absence_id from im_user_absences where description is not null"] {
            set sentence [lindex $sentences $ctr]
            db_dml update_topic "update im_user_absences set description = :sentence where absence_id = :absence_id"
            incr ctr
        }

        foreach project_id [db_list forums "select project_id from im_projects where note is not null or description is not null"] {
            set sentence [lindex $sentences $ctr]
            db_dml update_topic "update im_projects set note = :sentence, description=:sentence where project_id = :project_id"
            incr ctr
        }

        foreach assignment_id [db_list forums "select assignment_id from im_freelance_assignments where assignment_comment is not null"] {
            set sentence [lindex $sentences $ctr]
            db_dml update_topic "update im_freelance_assignments set assignment_comment = :sentence where assignment_id = :assignment_id"
            incr ctr
        }

        if {[im_table_exists im_assignment_quality_reports]} {
            foreach report_id [db_list forums "select report_id from im_assignment_quality_reports where comment is not null"] {
                set sentence [lindex $sentences $ctr]
                db_dml update_topic "update im_assignment_quality_reports set comment = :sentence where report_id = :report_id"
                incr ctr
            }
        }

        foreach item_id [db_list forums "select item_id from im_invoice_items where description is not null"] {
            set sentence [lindex $sentences $ctr]
            db_dml update_topic "update im_invoice_items set description = :sentence where item_id = :item_id"
            incr ctr
        }

        foreach price_id [db_list forums "select price_id from im_prices where note is not null"] {
            set sentence [lindex $sentences $ctr]
            db_dml update_topic "update im_prices set note = :sentence where price_id = :price_id"
            incr ctr
        }

        foreach payment_id [db_list forums "select payment_id from im_payments where note is not null"] {
            set sentence [lindex $sentences $ctr]
            db_dml update_topic "update im_payments set note = :sentence where payment_id = :payment_id"
            incr ctr
        }

        foreach company_id [db_list forums "select company_id from im_companies where note is not null"] {
            set sentence [lindex $sentences $ctr]
            db_dml update_topic "update im_companies set note = :sentence where company_id = :company_id"
            incr ctr
        }

        foreach cost_id [db_list forums "select cost_id from im_costs where note is not null or description is not null"] {
            set sentence [lindex $sentences $ctr]
            db_dml update_topic "update im_costs set note = :sentence, description=:sentence where cost_id = :cost_id"
            incr ctr
        }

    }

    ad_proc -public persons {} {
        Anonymize persons 
    } {
        set current_user_id [auth::get_user_id]
        if {![acs_user::site_wide_admin_p -user_id $current_user_id]} {
            return ""
        }

        set bounce_domain [parameter::get_from_package_key -package_key "acs-mail-lite" -parameter "BounceDomain"]
        
        set ctr 0
        db_dml disable_person_trigger "alter table persons disable trigger ALL"

        # Reset persons
        set person_ids [db_list persons "select person_id from persons where person_id != :current_user_id"]
        db_dml update_users "update users set username = user_id, screen_name = user_id where user_id != :current_user_id"
        db_dml update_users "update parties set email = party_id, signature=null where party_id != :current_user_id"
        set new_names_list [cog::randommer::Name -quantity [llength $person_ids] -nameType "fullname"]

        foreach person_id $person_ids {
            set fullname [lindex $new_names_list $ctr]
            set first_names [lindex $fullname 0]
            set last_name [lindex $fullname 1]
            set username [string tolower "${first_names}.$last_name"]
            set screen_name "[string range $first_names 0 1][string range $last_name 0 1]"
            set email "${username}@$bounce_domain"
            db_dml update_persons "update persons set first_names = :first_names, last_name = :last_name where person_id = :person_id"
            db_dml update_emails "update parties set email = :email where party_id = :person_id"
            db_dml update_users "update users set username = :username, screen_name = :username where user_id = :person_id"
            catch {db_dml update_users "update users set screen_name = :screen_name where user_id = :person_id"}
            ad_change_password $person_id $screen_name
            
            db_dml update_object "update acs_objects set title = :fullname where object_id = :person_id"
            set http [new_CkHttp]
            incr ctr
        }

        set persons_with_bio_ids [db_list bio "select person_id from persons where bio is not null"]
        set new_bios [cog::randommer::LoremIpsum -quantity [llength $persons_with_bio_ids]]
        set ctr 0
        foreach person_id $persons_with_bio_ids {
            set bio [lindex $new_bios $ctr]
            db_dml update_person "update persons set bio = :bio where person_id = :person_id"
            incr ctr
        }

        if {[db_column_exists persons position]} {
            db_dml anonymize "update persons set position = translate(position, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC', 'xaaabbwiwiqihhasdgkkddkjhhLLKKJJJJUUKIOOIIPLLPZZTTTZZ')"
        }

        if {[db_column_exists persons job_title]} {
            db_dml anonymize "update persons set job_title = translate(job_title, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC', 'xaaabbwiwiqihhasdgkkddkjhhLLKKJJJJUUKIOOIIPLLPZZTTTZZ')"
        }

        db_dml enable_person_trigger "alter table persons enable trigger ALL"

        if {[db_column_exists users_email smtpuser]} {
            db_dml users_email_update "update users_email set smtpuser = null, smtppassword = null, smtphost = null, imaphost = null, imapuser = null, imappassword = null"
        }


        db_dml users_contact_update {
            update users_contact set
                home_phone = null, skype_screen_name=null,
                work_phone = translate(work_phone, '0123456789', '4578906123'),
                cell_phone =translate(cell_phone, '0123456789', '4578806122'),
                pager = null, fax = null, aim_screen_name = null, icq_number = null,
                wa_line1 = null, wa_line2 = null, wa_city = null, wa_state = null, wa_postal_code = null,
                current_information = null
        }
        
        db_dml update_country "update users_contact set ha_country_code = wa_country_code where ha_country_code is null"
        
        db_foreach users_contact "select ha_country_code, user_id from users_contact" {
            if {$ha_country_code eq ""} {
                set company_id [cog::user::main_company_id -user_id $user_id]
                set ha_country_code [db_string ha_country_code "select address_country_code from im_offices o, im_companies c 
                    where c.main_office_id = o.office_id and c.company_id = :company_id" -default ""]
                db_dml update "update users_contact set ha_country_code = :ha_country_code where user_id = :user_id"
            }
        }

        db_foreach ha_country_code "select ha_country_code, count(ha_country_code) as num from users_contact group by ha_country_code" {
            set addresses_list($ha_country_code) [cog::randommer::RandomAddress -quantity $num -country_code $ha_country_code]
            set addresses_counter($ha_country_code) 0
            set phone_nums_list($ha_country_code)  [cog::randommer::Phone -quantity [expr $num*2] -country_code $ha_country_code]
            set phone_nums_counter($ha_country_code) 0
        }

        db_foreach office "select user_id, ha_country_code from users_contact" {
            set ha_list [lindex $addresses_list($ha_country_code) $addresses_counter($ha_country_code)]
            set ha_line1 [lindex $ha_list 0]
            set ha_line2 [lindex $ha_list 1]
            set ha_postal_code [lindex $ha_list 2]
            set ha_city [lindex $ha_list 3]
            set ha_state [lindex $ha_list 4]
            set work_phone [lindex $phone_nums_list($ha_country_code) $phone_nums_counter($ha_country_code)]
            incr phone_nums_counter($ha_country_code)
            set cell_phone [lindex $phone_nums_list($ha_country_code) $phone_nums_counter($ha_country_code)]
            incr phone_nums_counter($ha_country_code)

            db_dml update_contacts "update users_contact set ha_line1 = :ha_line1, ha_line2 = :ha_line2, ha_postal_code = :ha_postal_code,
                ha_city = :ha_city, ha_state = :ha_state, work_phone = :work_phone, cell_phone=:cell_phone
                where user_id = :user_id"
            
            incr addresses_counter($ha_country_code)
        }

        if {[db_column_exists persons sworn_court]} {
            db_dml anonymize "update persons set sworn_court = (select ha_city from users_contact where person_id = user_id) where sworn_court is not null"
        }


        db_dml im_user_absences_update {
            update im_user_absences set
                absence_name = im_name_from_id(absence_type_id),
                contact_info = im_name_from_id(vacation_replacement_id);
            update im_user_leave_entitlements set leave_entitlement_name = im_name_from_id (leave_entitlement_type_id);
        }


        db_dml im_employees_update {
            update im_employees set
                personnel_number = null, ss_number = null, educational_history = null,
                termination_reason = null, last_degree_completed = null, skills = null,
                job_description = null, job_title = null, salary=null
        } 
    }

    ad_proc -public offices {} {
        Anonymize offices
    } {
        set current_user_id [auth::get_user_id]
        if {![acs_user::site_wide_admin_p -user_id $current_user_id]} {
            return ""
        }

        # Prepare office addresses
        db_foreach office_countries "select address_country_code, count(address_country_code) as num from im_offices group by address_country_code" {
            set addresses_list($address_country_code) [cog::randommer::RandomAddress -quantity $num -country_code $address_country_code]
            set addresses_counter($address_country_code) 0
            set phone_nums_list($address_country_code)  [cog::randommer::Phone -quantity [expr $num*2] -country_code $address_country_code]
            set phone_nums_counter($address_country_code) 0
        }

        db_foreach office "select office_id, address_country_code from im_offices" {
            set address_list [lindex $addresses_list($address_country_code) $addresses_counter($address_country_code)]
            set address_line1 [lindex $address_list 0]
            set address_line2 [lindex $address_list 1]
            set address_postal_code [lindex $address_list 2]
            set address_city [lindex $address_list 3]
            set address_state [lindex $address_list 4]
            set phone [lindex $phone_nums_list($address_country_code) $phone_nums_counter($address_country_code)]
            incr phone_nums_counter($address_country_code)
            set fax [lindex $phone_nums_list($address_country_code) $phone_nums_counter($address_country_code)]
            incr phone_nums_counter($address_country_code)

            db_dml update_office "update im_offices set address_line1 = :address_line1, address_line2 = :address_line2,
                address_postal_code = :address_postal_code, address_city = :address_city, address_state= :address_state,
                phone = :phone, fax=:fax, landlord = null, security = null
                where office_id = :office_id"
            incr addresses_counter($address_country_code)
        }
    }

    ad_proc -public companies {} {
        Anonymize the companies in the system
    } {

        set current_user_id [auth::get_user_id]
        if {![acs_user::site_wide_admin_p -user_id $current_user_id]} {
            return ""
        }
        
        set internal_company_id [im_company_internal]
        set freelance_company_id [im_company_freelance]

        db_dml update_companies "update im_companies set company_path = company_id"
        set company_ids [db_list num "select company_id from im_companies where company_name not like 'Freelance%'"]
        set num_company_ids [llength $company_ids]
        set company_ctr 0
        for {set z 0} {$z <= [format "%.0f" [expr $num_company_ids / 100]]} {incr z} {
            # Initialize company infos
            set jsonArray [new_CkJsonArray]
            CkJsonArray_Load $jsonArray [cog::random_data_api::companies -size [llength $company_ids]]
            set arraySize [CkJsonArray_get_Size $jsonArray]
            for {set i 0} {$i <= [expr $arraySize - 1]} {incr i} {
                set company_id [lindex $company_ids $company_ctr]
                incr company_ctr
                if {$company_id ne ""} {
                    set compObj [CkJsonArray_ObjectAt $jsonArray $i]
                    set company_name_arr($company_id) [CkJsonObject_stringOf $compObj business_name]

                    delete_CkJsonObject $compObj
                }
            }
            delete_CkJsonArray $jsonArray
        }
         
        db_foreach freelancer "select company_id, first_names, last_name from im_companies c, persons p  where c.primary_contact_id = p.person_id and company_name like 'Freelance%'" {
            set company_name_arr($company_id) "Freelance $first_names $last_name"
        }

        set company_ids [db_list companies "select company_id from im_companies"]

        
        db_dml disable_trigger "alter table im_companies disable trigger ALL;"
        foreach company_id $company_ids {
            set company_name $company_name_arr($company_id)

            regsub -all {[^a-zA-Z0-9]} [string trim [string tolower $company_name]] "_" company_path

            set office_name "$company_name [lang::message::lookup "" intranet-core.Main_Office {Main Office}]"

            if {[db_string company_path_exists "select 1 from im_companies where company_path = :company_path" -default 0]} {
                set company_path "${company_path}_$company_id"
                set office_name "$company_name [lang::message::lookup "" intranet-core.Main_Office {Main Office}] ($company_id)"
            }
            db_dml update_company "update im_companies set company_name = :company_name, company_path = :company_path,
                referral_source = null, site_concept = null,
                vat_number=translate(vat_number, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC', 'xaazzbwiwiqihhasdgkkbbkjhhLLKKIIJJUUQQOOIIPSSPZZFFTZZ')
                where company_id = :company_id"
            
            set office_path "${company_path}_main_office"

            db_dml update_office "update im_offices set office_path = :office_path, office_name = :office_name where office_id = (select main_office_id from im_companies where company_id = :company_id)"
        }
        if [ im_column_exists im_companies bank_account_nr] {
            db_dml company_bank {
                update im_companies set
                    bank_account_nr=null,
                    bank_routing_nr=null,
                    bank_name=null,
                    bic=null
            }
        }

        if [ im_column_exists im_companies iban] {
            db_foreach iban_company "select c.company_id, address_country_code from im_companies c, im_offices o 
                where o.office_id = c.main_office_id and c.iban is not null" {
                    set iban [cog::randommer::iban -country_code $address_country_code]
                    db_dml update_company "update im_companies set iban = :iban where company_id = :company_id"
                }
        }

        if [ im_column_exists im_companies paypal_email] {
            db_dml company_bank "update im_companies set paypal_email = null"
        }

        if [ im_column_exists im_companies skrill_email] {
            db_dml company_bank "update im_companies set skrill_email=null"
        }

        db_dml update_internal "update im_companies set company_path = 'internal' where company_id = :internal_company_id"
        if {$internal_company_id ne $freelance_company_id} {
            db_dml update_internal "update im_companies set company_path = 'default_freelance' where company_id = :freelance_company_id"
        }

        db_dml disable_trigger "alter table im_companies enable trigger ALL;"

    }

    ad_proc -public projects {} {
        Anonymize projects - recreate the names
    } {
        db_dml projects "alter table im_projects disable trigger ALL"

        db_dml null_projects "
            update im_projects set company_project_nr =  null
        "

        if {[db_column_exists im_projects final_company]} {
            db_dml anonymize_final_company {
                update im_projects set final_company = null
            }
        }

        db_foreach project "select project_id, company_id, im_name_from_id(project_type_id) as project_type from im_projects where project_type_id not in (100,101)" {
            set project_nr [im_next_project_nr -customer_id $company_id]
            set project_name "$project_nr ($project_type)"
            if {[im_column_exists im_projects source_language_id]} {
                set source_language [db_string source "select im_name_from_id(source_language_id) from im_projects where project_id = :project_id" -default ""]
                if {$source_language ne ""} {
                    set target_languages [im_target_languages $project_id]
                    set project_name "${project_type}: $source_language => [join $target_languages ","]"
                }
            }
            regsub -all {[^a-zA-Z0-9]} [string trim [string tolower $project_nr]] "_" project_path
            db_dml update_project_nr_name "update im_projects set project_path = :project_path, project_nr = :project_nr, project_name = :project_name where project_id = :project_id"
        }

        set quantity [db_string tasks "select count(*) from im_projects where project_type_id = 100"]
        set reviews [cog::randommer::review -product "ERP4translation" -quantity $quantity]
        set ctr 0
        
        db_foreach project "select project_id, company_id, im_name_from_id(project_type_id) as project_type from im_projects where project_type_id = 100" {
            set task_name [lindex $reviews $ctr]
            incr ctr
            db_dml update_project_nr_name "update im_projects set project_path = project_id, project_nr = project_id, project_name = :task_name where project_id = :project_id"
        }

        db_dml projects "alter table im_projects enable trigger ALL"


        if {[im_table_exists im_trans_tasks]} {
            db_dml anonymize_translation {
                alter table im_trans_tasks disable trigger ALL;
                update im_trans_tasks set task_name = im_name_from_id(task_type_id) || ' ' || im_name_from_id(source_language_id) || '->' || im_name_from_id(target_language_id) || ' (' || task_id || ')',
                    description = translate(description, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC', 'xaaaaawiwiqihhasdgkkkkkjhhLLKKJJJJUUUUOOIIPPPPZZTTTZZ'),
                    task_filename = translate(task_filename, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC', 'xaaaaawiwiqihhasdgkkkkkjhhLLKKJJJJUUUUOOIIPPPPZZTTTZZ');
                alter table im_trans_tasks enable trigger ALL;
            }
        }

        if {[im_table_exists im_freelance_packages]} {
            db_foreach package "select project_id, target_language_id, package_type_id, freelance_package_id from im_freelance_packages" {
                # We have either no package_name, then we need to generate it.
                # Alternative we have more than one, then we need a new one if we want to assign the tasks in a package

                if {![db_0or1row prefix "select aux_string1 as type_prefix, category from im_categories where category_id = :package_type_id and category_type = 'Intranet Trans Task Type'"]} {
                    # Invalid package_type
                    set package_name ""
                }
                if {$type_prefix eq ""} {
                    db_1row project_name_and_language "select project_name from im_projects p where project_id = :project_id"
                    set type_prefix "${project_name}_$category"
                }
                
                if {$target_language_id ne ""} {
                    set type_prefix "${type_prefix}_[im_name_from_id $target_language_id]"
                }

                set counter 0
                set exists_p 1
                while {$exists_p > 0 } {
                    incr counter
                    set package_name "${type_prefix}_$counter"
                    set exists_p [db_string package_name_exists "select 1 from im_freelance_packages where freelance_package_name = :package_name and package_type_id = :package_type_id and project_id = :project_id limit 1" -default 0]
                }
                db_dml update_name "update im_freelance_packages set freelance_package_name = :package_name where freelance_package_id = :freelance_package_id"
            }
        }

        if {[im_table_exists im_freelance_assignments]} {
            db_foreach assignment "select assignment_id, project_nr, freelance_package_name, material_id, assignment_type_id 
                from im_freelance_assignments a, im_freelance_packages fp, im_projects p
                where a.freelance_package_id = fp.freelance_package_id
                and fp.project_id = p.project_id" {
                set assignment_name "$project_nr - $freelance_package_name - [im_material_name -material_id $material_id] - [im_category_from_id $assignment_type_id]"
                db_dml update "update im_freelance_assignments set assignment_name = :assignment_name where assignment_id = :assignment_id"
            }
        }
        if {[im_table_exists im_freelance_assignments]} {
            db_dml anonymize_package_files "update im_freelance_package_files set freelance_package_file_name =  translate(freelance_package_file_name, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC', 'xaaaaawiwiqihhasdgkkkkkjhhLLKKJJJJUUUUOOIIPPPPZZTTTZZ'),  file_path = translate(file_path, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC', 'xaaaaawiwiqihhasdgkkkkkjhhLLKKJJJJUUUUOOIIPPPPZZTTTZZ');"
        }
    }

    ad_proc -public finance {} {
        Anonymize the finance part
    } {
        db_dml anoymize_non_standard_cost_names "update im_costs set cost_name = translate(cost_name, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZC', 'xaaaaawiwiqihhasdgkkkkkjhhLLKKJJJJUUUUOOIIPPPPZZTTTZZ') where cost_name not like '_2%'"

        # ---------------------- im_invoice_items -------------------------------

        db_dml update_items "update im_invoice_items set item_name = item_id"
        foreach invoice_id [db_list invoices "select invoice_id from im_invoices"] {
            set im_invoice_items_sql "
                select item_id, material_name
                from	im_invoice_items i, im_materials m
                where m.material_id = i.item_material_id
                and invoice_id = :invoice_id
            "
            set sort_order 1
            db_foreach im_invoice_items_select $im_invoice_items_sql {
                set new_price "0.[expr round(100*rand())]"
                db_dml im_invoice_items_update "
                update im_invoice_items set
                    item_name = :material_name,
                    sort_order = :sort_order,
                    price_per_unit = :new_price
                where item_id = :item_id
                "
                incr sort_order
            }
            cog::invoice::update_rounded_amount -invoice_id $invoice_id
        }

        foreach price_id [db_list prices "select price_id from im_prices"] {
            set new_price [expr round(100*rand()) / 100]
            db_dml update_prices "update im_prices set price = :new_price where price_id = :price_id"
        }

        # ---------------------- im_trans_prices -------------------------------
        if {[im_table_exists im_trans_prices]} {
            db_dml delete_trans_prices "delete from im_trans_prices"
        }

        if {[im_table_exists im_timesheet_prices]} {
            db_dml delete_trans_prices "delete from im_timesheet_prices"
        }

    }


    ad_proc -public portrait {
        -user_id:required
    } {
        Add new portrait to user. Won't check for gender. uses https://thispersondoesnotexist.com

        @param user_id User who should get a new portrait
    } {
	return ""
	
        while {[nsv_exists coy_anonymize portrait]} {
            after 500 
        }

        nsv_set cog_anonymize portrait 1

        set http [new_CkHttp]
        set portrait_path [ns_mktemp]
        set success [CkHttp_Download $http "https://thispersondoesnotexist.com/image" $portrait_path]
        if {$success != 1} then {
            cog_log Error [CkHttp_lastErrorText $http]
            set file_revision_id ""
            set file_item_id ""
        } else {
            set file_revision_id [cog::file::import_fs_file -context_id $user_id -user_id $user_id -file_path $portrait_path]
            set file_item_id [content::revision::item_id -revision_id $file_revision_id]
            cog_rest::post::relationship -object_id_one $user_id -object_id_two $file_item_id -rel_type "user_portrait_rel" -rest_user_id $user_id
        }

        file delete $portrait_path
        delete_CkHttp $http
        after 1000
        nsv_unset cog_anonymize portrait

        return $file_item_id
    }

}


namespace eval cog::random_data_api {
    ad_proc -public companies {
        {-size 2}
    } {
        Returns a JSON array of company objects with
        id, uid, business_name, suffix, industry, catch_phrase,
        actuating, bs_company_statement, employee_identification_number,
        duns_number, logo, type, phone_number, full_address, latitude, longitude 

        @param size How many records (max 100)
    } {
        set http [new_CkHttp]
        if {$size >100} {
            set size 100
        }
        set jsonString [CkHttp_quickGetStr $http [export_vars -base "https://random-data-api.com/api/company/random_company" -url {size}]]
        delete_CkHttp $http
        return $jsonString
    }
}

namespace eval cog::randommer {

    ad_proc -public get {
        -type:required
        {-url_params ""}
    } {
        Get Call to the API of Randommer and return a JSON 

        @param type What is the type we want to Retrieve
        @param url_params Any parameters to pass through using export_vars

        @return JSON Array string
    } {
        set http [new_CkHttp]
        set api_key [parameter::get_from_package_key -package_key "cognovis-core" -parameter "RandommerAPIKey"]
        CkHttp_SetRequestHeader $http "X-Api-Key" $api_key
        if {$url_params eq ""} {
            set jsonString [CkHttp_quickGetStr $http "https://randommer.io/api/${type}"]
        } else {
            set jsonString [CkHttp_quickGetStr $http "https://randommer.io/api/${type}?$url_params"]
        }
        if {[CkHttp_get_LastMethodSuccess $http] == 0} then {
           cog_log Error [CkHttp_lastErrorText $http]
        }
        delete_CkHttp $http
        return $jsonString
    }

    ad_proc -public post {
        -type:required
        {-url_params ""}
    } {
        Get Call to the API of Randommer and return a JSON 

        @param type What is the type we want to Retrieve
        @param url_params_list Any parameters to pass through using a key value list of lists

        @return JSON Array string
    } {
        set http [new_CkHttp]
        set api_key [parameter::get_from_package_key -package_key "cognovis-core" -parameter "RandommerAPIKey"]
        CkHttp_SetRequestHeader $http "X-Api-Key" $api_key

        set resp [CkHttp_PText $http "POST" "https://randommer.io/api/Text/Review?$url_params" "" "" "" 0 0]

        if {[CkHttp_get_LastMethodSuccess $http] == 0} then {
           cog_log Error [CkHttp_lastErrorText $http]
        }
        set jsonString [CkHttpResponse_bodyStr $resp]
        delete_CkHttpResponse $resp
        delete_CkHttp $http
        
        return $jsonString
    }

    ad_proc -public Name {
        { -nameType "firstname" }
        { -quantity "1"}
    } {
        Returns a list of names of the type

        @param nameType Any of firstname, surname and fullname
        @param quantity How many do we need to return

        @return names list of names
    } {
        set jsonArray [new_CkJsonArray]
        CkJsonArray_Load $jsonArray [cog::randommer::get -type "Name" -url_params [export_vars {nameType quantity}]]

        set names [list]
        set arraySize [CkJsonArray_get_Size $jsonArray]
        for {set i 0} {$i <= [expr $arraySize - 1]} {incr i} {
            lappend names [CkJsonArray_stringAt $jsonArray $i]
        }

        delete_CkJsonArray $jsonArray

        return $names
    }

    ad_proc -public LoremIpsum {
        { -loremType "normal"}
        { -type "paragraphs"}
        { -quantity "1"}
    } {
        Returns a list of product reviewws

    } {
        if {$quantity eq 0} { return ""}

        set lorem_ipsum [cog::randommer::get -type "Text/LoremIpsum" -url_params [export_vars {loremType type {number $quantity}}]]
        regsub -all {"} $lorem_ipsum {} lorem_ipsum
        return [textutil::splitx $lorem_ipsum "<br>"]
    }

    ad_proc -public Phone {
        {-quantity "1"}
        {-country_code "en"}
    } {
        Return random phone numbers for a country_code

        @quantity how many do we need
        @country_code code for which to generate phone numbers
    } {
        if {$quantity eq 0} { return ""}

        set jsonArray [new_CkJsonArray]

        switch $country_code {
            uk {
                set country_code gb
            }
        }

        CkJsonArray_Load $jsonArray [cog::randommer::get -type "Phone/Generate" -url_params [export_vars {{CountryCode $country_code} {Quantity $quantity}}]]
        set arraySize [CkJsonArray_get_Size $jsonArray]
        if {$arraySize <1} {
            CkJsonArray_Load $jsonArray [cog::randommer::get -type "Phone/Generate" -url_params [export_vars {{CountryCode "de"} {Quantity $quantity}}]]
            set arraySize [CkJsonArray_get_Size $jsonArray]
        }
        set phones [list]
        
        for {set i 0} {$i <= [expr $arraySize - 1]} {incr i} {
            lappend phones [CkJsonArray_stringAt $jsonArray $i]
        }

        delete_CkJsonArray $jsonArray

        return $phones

    }

    ad_proc -public RandomAddress {
        {-quantity "1"}
        {-country_code "en"}
    } {
        Returns random addresses

        @return List of Lists with "address_line1 address_line2 ZIP City State Country"
    } {
        if {$quantity eq 0} { return ""}

        switch $country_code {
            de - fr - es - it - tr - sk {
                set culture $country_code
            }
            at {
                set culture "de_AT"
            }
            za {
                set culture "af_ZA"
            }
            no {
                set culture "nb_NO"
            }
            se {
                set culture "sv"
            }
            ie {
                set culture "en_IE"
            }
            pt {
                set culture "pt_PT"
            }
            ch {
                set culture "fr_CH"
            }
            uk - gb {
                set culture "en_GB"
            }
            default {
                set culture "en"
            }
        }
        
        set jsonArray [new_CkJsonArray]
        CkJsonArray_Load $jsonArray [cog::randommer::get -type "Misc/Random-Address" -url_params [export_vars {culture {number $quantity}}]]

        set addresses [list]
        set arraySize [CkJsonArray_get_Size $jsonArray]
        for {set i 0} {$i <= [expr $arraySize - 1]} {incr i} {
            set address_string [CkJsonArray_stringAt $jsonArray $i]
            regsub -all {, } $address_string {,} address_string
            lappend addresses [split $address_string ","]
        }

        delete_CkJsonArray $jsonArray

        return $addresses
    }

    ad_proc -public iban {
        -country_code:required
    } {
        Return an IBAN for the country 

    } {
        switch $country_code {
            al - ad - at - az - bh - be - ba - br - bg - cr - hr - cy - cz - dk {
                set countryCode $country_code
            }
            do - ee - fo - fi - fr - ge - de - gi - gr - gl - gt - hu - is - ie - il - it - jo - kz {
                set countryCode $country_code
            }                
            kw - lv - lb - li - lt - lu - mk - mt - mr - mu - md - mc - nl - no - pk - ps - pl - pt - qa - ro - sm - sa - rs - sk - si - es - se - ch - tl - tn - tr - ae - gb - vg {
                set countryCode $country_code
            }
            uk {
                set countryCode gb
            }
            default {
                set countryCode de
            }
        }
        return [string trim [cog::randommer::get -type "Finance/Iban/$countryCode"] \"]
    }

    ad_proc -public review {
        -product:required
        -quantity:required
    } {
        Gets reviews for the product

        @param product The product we want to get reviews for
        @param quantity How many do we want
    } {
        set jsonArray [new_CkJsonArray]
        set counter $quantity
        if {$quantity > 500} {set quantity 500} ;# Hard limit on randommer site
        CkJsonArray_Load $jsonArray [cog::randommer::post -type "Text/Review" -url_params [export_vars {product quantity}]]

        set reviews [list]
        while {$counter >0} {
            set arraySize [CkJsonArray_get_Size $jsonArray]
            for {set i 0} {$i <= [expr $arraySize - 1]} {incr i} {
                if {$counter >0} {
                    lappend reviews [CkJsonArray_stringAt $jsonArray $i]
                    set counter [expr $counter -1]
                } else {
                    break
                }
            }
        }

        delete_CkJsonArray $jsonArray

        return $reviews
    }
}
