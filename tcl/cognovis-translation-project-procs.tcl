ad_library {
    Procedures to handle translation projects
    @author malte.sussdorff@cognovis.de
}

namespace eval cog::trans::project {

    ad_proc -public new { 
        -company_id:required
        -source_language_id:required
        -target_language_ids:required
        -creation_user:required
        { -project_type_id "" }
        { -project_status_id "" }
        { -subject_area_id ""}
        { -final_company_id ""}
        { -customer_contact_id ""}
        { -project_id ""}
        { -project_name ""}
        { -project_nr ""}
        { -processing_time "" }
        { -company_project_nr ""}
        { -project_lead_id ""}
        { -final_company_id ""}
        { -subject_area_id ""}
        { -project_source_id ""}
        { -complexity_type_id ""}
        { -parent_id ""}
        { -description ""}
        { -start_date "" }
        { -end_date ""}
        -no_callback:boolean
    } {
        Create a new translation project
        
        @param project_status_id Status of the project, defaults to potential
        @param project_type_id Type of the project, defaults to trans + edit
    }  {
        # Auto get the project_nr if missing
        if {$project_nr eq ""} {
            set project_nr [im_next_project_nr -customer_id $company_id]
        }
        
        # Auto set the project_name
        if {$project_name eq ""} {
            set project_name $project_nr
        }

        if {$customer_contact_id eq ""} {
            set customer_contact_id [db_string company_contact "select coalesce(primary_contact_id,accounting_contact_id) from im_companies where company_id = :company_id" -default ""]
        }

        set project_id [db_string project_id "select project_id from im_projects where
            (   upper(trim(project_name)) = upper(trim(:project_name)) OR
                upper(trim(project_nr)) = upper(trim(:project_nr)) OR
                upper(trim(project_path)) = upper(trim(:project_name))
            )" -default 0]

        # Try to create the project
        if {$project_id eq "0"} {
            set project_path [string tolower [string trim $project_nr]]

            # Use sensible defaults
            if {$project_type_id eq ""} {set project_type_id [im_project_type_trans_edit]}
            if {$project_status_id eq ""} {set project_status_id [im_project_status_potential]}

            set creation_ip [ns_conn peeraddr]
            set project_id [db_exec_plsql create_new_project {
                select im_project__new (
                    NULL,         
                    'im_project',
                    now(),
                    :creation_user,
                    :creation_ip,
                    null,
                    :project_name,
                    :project_nr,
                    :project_path,
                    null,
                    :company_id,
                    :project_type_id,
                    :project_status_id
                );

            }]
        }

        if {$project_id eq 0} {
            ns_log Error "Creation of project $project_name , $project_nr for [im_name_from_id $company_id] failed"
        } else {

            # Add the project Manager
            if {$project_lead_id eq ""} {
                set project_lead_id $creation_user
            }

            set role_id [im_biz_object_role_project_manager]
            im_biz_object_add_role $project_lead_id $project_id $role_id
            
            # Project source id
            if {$project_source_id eq ""} {
                set project_source_id [im_project_source_normal]
            }
            db_dml update_project "update im_projects set company_contact_id = :customer_contact_id, source_language_id = :source_language_id, 
                project_lead_id = :project_lead_id, final_company_id = :final_company_id, processing_time =:processing_time, subject_area_id = :subject_area_id, 
                project_source_id =:project_source_id, company_project_nr = :company_project_nr, description =:description, parent_id =:parent_id
                where project_id = :project_id"
        
            if {$complexity_type_id ne ""} {
                db_dml update_complexity "update im_projects set complexity_type_id = :complexity_type_id where project_id = :project_id"
            }
            # Save the information about the project target languages
            # in the im_target_languages table
            db_dml delete_im_target_language "delete from im_target_languages where project_id=:project_id"
    
            foreach lang $target_language_ids {
                set sql "insert into im_target_languages values ($project_id, $lang)"
                db_dml insert_im_target_language $sql
                im_freelance_add_required_skills -object_id $project_id -skill_type_id [im_freelance_skill_type_target_language] -skill_ids $lang
            }
            
            set update_dates_query [list]
            if {$start_date ne ""} {
                lappend update_dates_query "start_date = to_date(:start_date, 'YYYY-MM-DD HH24:MI')" 
            } else {
                # Set start date to now if the project is open
                if {[lsearch [im_sub_categories [im_project_status_open]] $project_status_id]>-1} {
                    lappend update_dates_query "start_date = now()"
                }
            }
            if {$end_date ne ""} {
                lappend update_dates_query "end_date = to_date(:end_date, 'YYYY-MM-DD HH24:MI')"
            }
            
            # We run query if update_dates_query has at least one element
            if {[llength $update_dates_query] > 0} {
                set update_dates_query_sql [join $update_dates_query ","]
                db_dml update_project_dates "update im_projects set $update_dates_query_sql where project_id = :project_id" 
            }

            # Add the source language as a skill
            im_freelance_add_required_skills -object_id $project_id -skill_type_id [im_freelance_skill_type_source_language] -skill_ids $source_language_id
            im_freelance_add_required_skills -object_id $project_id -skill_type_id [im_freelance_skill_type_subject_area] -skill_ids $subject_area_id
            intranet_fs::create_project_folder -project_id $project_id -user_id $creation_user

            if {!$no_callback_p} {
                cog::callback::invoke cog::trans::project::after_create -project_id $project_id -user_id $creation_user
            }
        
        }
                
        return $project_id
    }

    ad_proc -public clone {
        -project_id:required
        {-company_id ""}
        {-company_contact_id ""}
        {-project_lead_id ""}
        {-project_name ""}
    } {
        Creates a base clone for the trans project with a new (or the same) project name.

        Will automatically get a new project_nr and retain the language combination.
    } {
        db_1row original_project_info "select project_name as original_project_name,
            company_id as original_company_id,
            company_contact_id as original_company_contact_id,
            project_lead_id as original_project_lead_id,
            processing_time, project_type_id, source_language_id,
            subject_area_id, final_company_id, project_source_id,
            complexity_type_id, description
            from im_projects where project_id = :project_id"
        
        if {$company_id eq ""} {set company_id $original_company_id} 
        if {$project_name eq ""} {set project_name $original_project_name}
        if {$company_contact_id eq ""} {set company_contact_id $original_company_contact_id}
        if {$project_lead_id eq ""} {set project_lead_id $original_project_lead_id}

        set target_language_ids [cog::trans::project_valid_language_ids -project_id $project_id -type "target"]

        set project_id [cog::trans::project::new \
            -company_id $company_id \
            -project_name $project_name \
            -processing_time $processing_time \
            -project_type_id $project_type_id \
            -project_lead_id $project_lead_id \
            -source_language_id $source_language_id \
            -target_language_ids $target_language_ids \
            -subject_area_id $subject_area_id \
            -final_company_id $final_company_id \
            -creation_user $project_lead_id \
            -customer_contact_id $company_contact_id \
            -complexity_type_id $complexity_type_id \
            -description $description]
    
        # Calculate the end date
        set start_timestamp [db_string start_date "select to_char(now(),'YYYY-MM-DD HH24:MI') from dual"]
        im_translation_update_project_dates -project_id $project_id -start_timestamp $start_timestamp

        return $project_id
    }

    ad_proc -public trans_task_types {
        -project_id:required
    } {
        Returns list of category_ids which are valid task_type ids. (from Intranet Project Type)

        @param project_id object im_project::read Project in which to look for possible task Types
        @return task_type_ids List of task types
    } {
        
        set task_type_ids [list]
        
        set project_type_id [db_string type_id "select project_type_id from im_projects where project_id = :project_id"]
        lappend task_type_ids $project_type_id

        switch $project_type_id {
            86 - 87 {
                lappend task_type_ids 93 ; # trans only
                lappend task_type_ids 2505 ; # cert only
                lappend task_type_ids 95 ; # proof only
            }
            89 {
                lappend task_type_ids 93 ; # trans only
                lappend task_type_ids 2505 ; # cert only
                lappend task_type_ids 95 ; # proof only
                lappend task_type_ids 88 ; # edit only
            }
            2505 {
                lappend task_type_ids 93 ; # trans only
            }
        }

        return $task_type_ids
    }

    ad_proc -public change_status {
        -project_id:required
        -project_status_id:required
        {-modifying_user_id ""}
        {-return_url ""}
    } {
        Quicker version than the full update call to facilitate status changes in a project

        @return return_url The URL to return to after the status change
    } {

        if {[nsv_exists cog_trans_project_update $project_id]} {
            cog_log Error "Your project is $project_id currently being processed. Please be patient and try again in 5 seconds <a href='[export_vars -base "/intranet/projects/view" -url {project_id}]'>here</a>"
        }

        nsv_set cog_trans_project_update $project_id 1

        if {$modifying_user_id eq ""} {
            set modifying_user_id [auth::get_user_id]
        }

        set old_project_status_id [db_string previous_status "select project_status_id from im_projects where project_id = :project_id"]

        if {$old_project_status_id ne $project_status_id} {
            db_dml update_project_status "update im_projects set project_status_id = :project_status_id where project_id = :project_id"

            cog::callback::invoke cog::trans::project::after_update -project_id $project_id -user_id $modifying_user_id -project_status_id $project_status_id -old_project_status_id $old_project_status_id -return_url_var "return_url"
        }

        nsv_unset cog_trans_project_update $project_id
        return $return_url
    }

    ad_proc -public update {
        -project_id:required
        -parent_id
        -company_id
        -source_language_id
        { -target_language_ids ""}
        -project_type_id
        -project_status_id
        -subject_area_id
        -final_company_id
        -customer_contact_id
        -project_lead_id
        -project_name
        -project_nr
        -processing_time
        -company_project_nr
        -project_source_id 
        { -start_date ""}
        { -end_date ""}
        -language_experience_level_id
        -contact_again_date
        -complexity_type_id
        -description
        { -modifying_user_id ""}
        {-return_url ""}
    } {
        Modify a translation project. In comparison to normal project creation this supports source and target languages as well as skills.
        Might be used for "normal" project creation as well though.

        Handler for PUT calls on the project.

        For variables starting with "skill_", these will be added as freelancer skills to the project. So are source_language_id, target_language_ids, subject_area_id and
        language_experience_level_id (the level for the skills in languages).

        @param trans_project_body_put request_body Single project which can be created
        @param project_id object im_project::write Project which we want to update
    } {

        if {[nsv_exists cog_trans_project_update $project_id]} {
            cog_log Error "Your project is $project_id currently being processed. Please be patient and try again in 5 seconds <a href='[export_vars -base "/intranet/projects/view" -url {project_id}]'>here</a>"
        }

        nsv_set cog_trans_project_update $project_id 1

        if {$modifying_user_id eq ""} {
            set modifying_user_id [auth::get_user_id]
        }
        set old_project_status_id [db_string previous_status "select project_status_id from im_projects where project_id = :project_id"]

        set sql "update im_projects set"

        if {[info exists customer_contact_id]} {
            set company_contact_id $customer_contact_id
        }
        
        set vars_to_retrieve [list]
        foreach mandatory_var [list company_contact_id company_id project_type_id project_status_id project_name project_nr source_language_id complexity_type_id] {
            if {[info exists $mandatory_var] && [set $mandatory_var] ne ""} {
                append sql " $mandatory_var=:$mandatory_var,\n"
            } else {
                lappend vars_to_retrieve $mandatory_var
            }
        }

        foreach optional_var [list final_company_id description processing_time company_project_nr parent_id contact_again_date subject_area_id] {
            if {[info exists $optional_var]} {
                append sql " $optional_var = :$optional_var, \n"
            } else {
                lappend vars_to_retrieve $optional_var
            }
        }

        foreach date_var [list start_date end_date] {
            if {[info exists $date_var]} {
                set value [set $date_var]
                if {$value eq ""} {
                    append sql " $date_var = null,\n"
                } else {
                    append sql " $date_var= '$value'::timestamp,\n"
                }
            } else {
                lappend vars_to_retrieve $date_var
            }
        }
            
        append sql " project_id=:project_id where project_id=:project_id"

        # Update the project
        db_dml update_im_projects $sql
        
        if {$target_language_ids ne ""} {
            db_dml delete_im_target_language "delete from im_target_languages where project_id=:project_id"
            foreach target_language_id $target_language_ids {
                db_dml insert_im_target_language "insert into im_target_languages values ($project_id, $target_language_id)"
            }
        } else {
            set target_language_ids [cog::trans::project_valid_language_ids -project_id $project_id -type "target"]
        }

        #---------------------------------------------------------------
        # Retrieve vars for the skills
        #---------------------------------------------------------------
        if {[llength $vars_to_retrieve]>0} {
            db_1row retrieve_vars "select [join $vars_to_retrieve ","] from im_projects where project_id = :project_id"
        }

        db_dml delete_exising_skills "delete from im_object_freelance_skill_map where object_id =:project_id"
        
        im_freelance_add_required_skills -object_id $project_id -skill_type_id [im_freelance_skill_type_source_language] -skill_ids $source_language_id
        im_freelance_add_required_skills -object_id $project_id -skill_type_id [im_freelance_skill_type_subject_area] -skill_ids $subject_area_id
        foreach target_language_id $target_language_ids {
            im_freelance_add_required_skills -object_id $project_id -skill_type_id [im_freelance_skill_type_target_language] -skill_ids $target_language_id
        }

        cog::callback::invoke cog::trans::project::after_update -project_id $project_id -user_id $modifying_user_id -project_status_id $project_status_id -old_project_status_id $old_project_status_id -return_url_var "return_url"
        
        nsv_unset cog_trans_project_update $project_id
        return $return_url
    }

    ad_proc -public nuke {
        -project_id:required
        { -user_id "" }
    } {
        if {$user_id ne ""} {
            set user_id [auth::get_user_id]
        }

        im_project_permissions $user_id $project_id view read write admin

        # delete the packages first
        foreach package_id [db_list package_id "select freelance_package_id from im_freelance_packages where project_id = :project_id"] {
            foreach assignment_id [db_list assignments "select assignment_id from im_freelance_assignments where freelance_package_id = :package_id"] {
                db_dml delete_reports "delete from im_assignment_quality_reports where assignment_id = :assignment_id"
                db_string delete_assignment "select im_freelance_assignment__delete(:assignment_id) from dual"
            }
            db_string delete_package "select im_freelance_package__delete(:package_id) from dual"                
        }
            
        # Delete trans_price_history
        db_dml delete_quality "delete from im_trans_freelancer_quality where project_id = :project_id"
        return [cog::project::nuke -current_user_id $user_id $project_id]
    }
}