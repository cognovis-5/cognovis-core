
ad_library {
    APM callback procedures.

    @creation-date 2021-08-06
    @author malte.sussdorff@cognovis.de
}

namespace eval cog::apm {}

ad_proc -public cog::apm::after_upgrade {
    {-from_version_name:required}
    {-to_version_name:required}
} {
    After upgrade callback.
} {
    apm_upgrade_logic \
        -from_version_name $from_version_name \
        -to_version_name $to_version_name \
        -spec {
            5.0.2.4.9 5.0.2.5.0 {
                # Remove richtext from notes.
                # Make it all html
                db_dml index "drop index im_notes_object_note_idx"
                db_foreach notes {select note_id, note from im_notes} {
                    catch {set note [template::util::richtext::get_property html_value $note]}
                    regsub -all {\n} $note {<br />} note
                    if {$note eq ""} {
                        im_note_nuke $note_id
                    } else {
                        db_dml update_note "update im_notes set note = :note where note_id = :note_id"
                    }
                }
                db_dml index_create "create unique index im_notes_object_note_idx on im_notes(object_id,md5(note))"
            }
            5.0.2.5.5 5.0.2.5.6 {
                db_foreach companies "select company_id, primary_contact_id from im_companies where primary_contact_id is not null" {
                    if {![im_biz_object_member_p $primary_contact_id $company_id]} {
                        set user_p [db_string user_p "select 1 from users where user_id = :primary_contact_id" -default 0]
                        if {$user_p} {
                            im_biz_object_add_role $primary_contact_id $company_id 1300
                        } else {
                            db_dml update "update im_companies set primary_contact_id = null where company_id = :company_id"
                        }
                    }
                }
                db_dml remove_constraint "alter table im_companies drop constraint im_companies_prim_cont_fk"
                db_dml add_constraing "alter table im_companies add constraint im_companies_prim_cont_fk foreign key (primary_contact_id) references users on delete set null"
                db_foreach companies "select company_id, accounting_contact_id from im_companies where accounting_contact_id is not null" {
                    if {![im_biz_object_member_p $accounting_contact_id $company_id]} {
                        set user_p [db_string user_p "select 1 from users where user_id = :accounting_contact_id" -default 0]
                        if {$user_p} {
                            im_biz_object_add_role $accounting_contact_id $company_id 1300
                        } else {
                            db_dml update "update im_companies set accounting_contact_id = null where company_id = :company_id"
                        }
                    }
                }
                db_dml add_constraing "alter table im_companies add constraint im_companies_acc_cont_fk foreign key (accounting_contact_id) references users on delete set null"
            }
            5.0.2.5.7 5.0.2.5.8 {
                db_foreach invoice_without_delivery "select cost_id,end_date from im_costs c, im_projects p where cost_type_id in (3700,3725,3740) and delivery_date is null and c.project_id = p.project_id" {
                    db_dml update_invoice_delivery_date "update im_costs set delivery_date = :end_date where cost_id = :cost_id"
                }
                if {[im_column_exists im_invoices delivery_date]} {
                    db_foreach invoice_delivery_date "select invoice_id, i.delivery_date from im_invoices i, im_costs c where i.invoice_id = c.cost_id and c.delivery_date is null and i.delivery_date is not null" {
                        db_dml update_invoice_delivery_date "update im_costs set delivery_date = :delivery_date where cost_id = :invoice_id"
                    }
                }
                if {[im_column_exists im_invoices delivery_date]} {
                    db_dml drop "alter table im_invoices drop column delivery_date"
                }
                db_foreach provider_bill "select cost_id, a.end_date from im_freelance_assignments a, im_costs c, acs_rels r
                    where r.object_id_two = c.cost_id and c.cost_type_id in (3704,3735,3741) and c.delivery_date is null
                    and r.object_id_one = a.purchase_order_id and a.end_date is not null" {
                        db_dml update_po_delivery "update im_costs set delivery_date = :end_date where cost_id = :cost_id"
                }
            }
            5.0.2.5.8 5.0.2.6.0 {
                # Migrate timesheet prices
                db_foreach timesheet_price {
                    select distinct uom_id, task_type_id from im_timesheet_prices where material_id is null
                } {
                    set material_id [cog::material::get_material_id -material_uom_id $uom_id -task_type_id $task_type_id]
                    if {$material_id eq ""} {
                        # need to generate it.
                        set material_id [cog::material::create -material_uom_id $uom_id -task_type_id $task_type_id -description "Auto generated from timesheet prices"]
                    }
                }
                db_foreach timesheet_price {
                    select uom_id, company_id, task_type_id, material_id, valid_from, valid_through, currency, price
                    from im_timesheet_prices
                } {
                    if {$material_id eq ""} {
                        set material_id [cog::material::get_material_id -material_uom_id $uom_id -task_type_id $task_type_id]
                    }
                    set price_id [cog::price::create -company_id $company_id -material_id $material_id \
                        -valid_from $valid_from -valid_through $valid_through -currency $currency -price $price]
                }

                if {[im_table_exists im_trans_prices]} {
                    # Migrate trans prices
                    db_foreach trans_price {
                        select uom_id, company_id, task_type_id, target_language_id, source_language_id, subject_area_id,
                        valid_from, valid_through, currency, price, note, file_type_id, min_price, complexity_type_id
                        from im_trans_prices
                    } {
                        set material_id [cog::material::get_material_id -material_uom_id $uom_id -task_type_id $task_type_id \
                            -file_type_id $file_type_id -subject_area_id $subject_area_id \
                            -source_language_id $source_language_id -target_language_id $target_language_id]
                        
                        if {$material_id eq ""} {
                            set material_id [cog::material::create -material_uom_id $uom_id -task_type_id $task_type_id \
                            -file_type_id $file_type_id -subject_area_id $subject_area_id \
                            -source_language_id $source_language_id -target_language_id $target_language_id \
                            -description "Auto generated from trans prices"]
                        }

                        set price_id [cog::price::create -company_id $company_id -material_id $material_id \
                            -valid_from $valid_from -valid_through $valid_through -currency $currency -price $price \
                            -complexity_type_id $complexity_type_id -min_price $min_price -note $note]
                    }
                }
            }
            5.0.2.6.2 5.0.2.6.3 {
                set user_ids [db_list users {select distinct object_id_one from
                    acs_rels r, users u
                    where r.object_id_one = u.user_id
                    and rel_type='user_portrait_rel'}]
                
                foreach user_id $user_ids {
                    set company_id [cog::user::main_company_id -user_id $user_id]
                    set freelance_company_p [db_string freelance_company "select 1 from im_companies where company_name like 'Freelance%' and company_id = :company_id" -default 0]
                    if {$freelance_company_p} {
                        set portrait_item_id [db_string revision "select max(object_id_two) from acs_rels where object_id_one = :user_id and rel_type = 'user_portrait_rel'"]
                        cog_rest::post::relationship -object_id_one $company_id -object_id_two $portrait_item_id -rel_type "im_company_logo_rel" -rest_user_id $user_id
                    } 
                }
            }
            5.0.2.6.3 5.0.2.6.4 {
                db_foreach price_to_be_created {
                    select material_id, assignee_id, rate from im_freelance_assignments where material_id in (select material_id from im_materials where material_type_id = 9014 and material_billable_p = 'f') and rate is not null
                } {
                    set company_id [cog::user::main_company_id -user_id $assignee_id]
                    set price_ids [cog::price::get_ids -company_id $company_id -material_id $material_id]
                    if {[llength $price_ids] eq 0} {
                        cog::price::create -company_id $company_id -material_id $material_id -price $rate
                    }
                }
                db_dml set_to_billable "update im_materials set material_billable_p = 't', material_status_id = 9100 where material_id in (select distinct material_id from im_prices)"
            }

            5.0.2.6.4 5.0.2.6.5 {
                db_foreach provider_material_id "select distinct m.material_id, task_type_id from im_materials m, \
                    im_prices p, im_companies c where p.company_id = c.company_id and m.material_id = p.material_id and \
                    c.company_type_id in ([ns_dbquotelist [im_sub_categories [im_company_type_provider]]])" {
                        if {$task_type_id ne ""} {
                            set new_type_id [db_string new_type "select category_id from im_categories where category = (select aux_string1 from im_categories where category_id = :task_type_id)" -default ""]
                            if {$new_type_id ne ""} {
                                db_dml update_trans_type "update im_materials set task_type_id = :new_type_id where material_id = :material_id"   
                            }
                        } 
                }
            }
            5.0.2.6.5 5.0.2.6.6 {
                ns_log Notice "Upgrading to 5.0.2.6.6"

                if {[im_table_exists im_termbases]} {
                    db_dml refresh_constraint {
                        alter table im_termbases drop constraint im_termbases_company_fk;
                        alter table im_termbases add constraint im_termbases_company_fk foreign key (company_id) references im_companies on delete cascade;
                    }
                }
                if {[im_table_exists im_timesheet_prices]} {
                    db_dml refresh_constraint {
                        alter table im_timesheet_prices drop constraint im_timesheet_prices_company_id;
                        alter table im_timesheet_prices add constraint im_timesheet_prices_company_id foreign key (company_id) references im_companies on delete cascade;
                    }
                }
                if {[im_table_exists im_trans_main_freelancer]} {
                    db_dml refresh_constraint {
                        alter table im_trans_main_freelancer drop constraint im_trans_main_fl_customer_fk;
                        alter table im_trans_main_freelancer add constraint im_trans_main_fl_customer_fk foreign key (customer_id) references im_companies on delete cascade;
                    }
                }
                if {[im_table_exists im_trans_memoq_resources]} {
                    db_dml refresh_constraint {
                        alter table im_trans_memoq_resources drop constraint im_trans_memoq_company_fk;
                        alter table im_trans_memoq_resources add constraint im_trans_memoq_company_fk foreign key (company_id) references im_companies on delete cascade;
                    }
                }

                if {[im_table_exists im_tb_languages]} {
                    db_dml refresh_constraint {
                        alter table im_tb_languages drop constraint im_tb_languages_termbase_fk;
                        alter table im_tb_languages add constraint im_tb_languages_termbase_fk foreign key (termbase_id) references im_termbases on delete cascade;
                    }
                }
                
                if {[im_table_exists im_tb_concepts]} {
                    db_dml refresh_constraint {
                        alter table im_tb_concepts drop constraint im_tb_concepts_termbase_fk;
                        alter table im_tb_concepts add constraint im_tb_concepts_termbase_fk foreign key (termbase_id) references im_termbases on delete cascade;
                    }
                }

                if {[im_table_exists im_tb_terms]} {
                    db_dml refresh_constraint {
                        alter table im_tb_terms drop constraint im_tb_terms_concept_fk;
                        alter table im_tb_terms add constraint im_tb_terms_concept_fk foreign key (concept_id) references im_tb_concepts on delete cascade;
                    }
                }


                if {[im_table_exists im_tb_descriptions]} {
                    db_dml refresh_constraint {
                        alter table im_tb_descriptions drop constraint im_tb_descriptions_concept_fk;
                        alter table im_tb_descriptions add constraint im_tb_descriptions_concept_fk foreign key (concept_id) references im_tb_concepts on delete cascade;
                        alter table im_tb_descriptions drop constraint im_tb_descriptions_terms_fk;
                        alter table im_tb_descriptions add constraint im_tb_descriptions_terms_fk foreign key (term_id) references im_tb_terms on delete cascade;
                    }
                }
            }
            5.0.2.6.9 5.0.2.6.10 {
                ns_log Notice "Upgrading to 5.0.2.6.10"

                # Remove the activities from the system as we don't need them anymore
                foreach activity_id [db_list cal_items "
                    select activity_id
                    from acs_events 
                    where related_object_type in ('im_project', 'im_trans_task')
                    or related_object_type is null"] {
                    db_string delete_activity "select acs_activity__delete(:activity_id)"
                    ns_log Notice "Deleting activity $activity_id"
                }

                foreach cal_item_id [db_list cal_items "
                    select event_id 
                    from acs_events 
                    where related_object_type in ('im_project', 'im_trans_task')
                    or related_object_type is null"] {

                    db_string delete_event "select acs_event__delete(:cal_item_id)"
                        
                    db_string delete_cal "select cal_item__delete(:cal_item_id)"
                    ns_log Notice "Deleting event $cal_item_id"

                }
            }
            5.0.2.7.1 5.0.2.7.2 {
                db_foreach notes "select note_id, n.object_id, context_id from im_notes n, acs_objects o where n.note_id = o.object_id and context_id is null" {
                    db_dml update_object_context "update acs_objects set context_id = :object_id where object_id = :note_id"
                }
            }
            5.0.2.8.4 5.0.2.8.5 {
                # Update the payment_terms for existing invoices 
                db_foreach cost "select cost_id, category_id from im_costs c, im_categories ca where ca.aux_int1 = c.payment_days and payment_term_id is null and ca.category_type = 'Intranet Payment Term'" {
                    db_dml update_payment_term "update im_costs set payment_term_id = :category_id where cost_id = :cost_id"
                }
            }
        }
}

ad_proc -public cog::apm::after_install {} {
    Setup cognovis core
} {

    # Remove richtext from notes.
    # Make it all html

    cog::run_update_scripts -package_key "cognovis-core" -ignore_db_errors
    cog::apm::after_upgrade -from_version_name 5.0.2.4.0 -to_version_name 5.0.3.0.0

}
