ad_library {
    Procedures to handle translation callbacks
    @author malte.sussdorff@cognovis.de
}

ad_proc -public -callback cog::trans::project::after_create {
    {-project_id:required}
    { -user_id "" }
} {
    After creation of a translation project

    @param project_id Project which was just created
    @param user_id User which is responsible for the project_creation
} -

ad_proc -public -callback cog::trans::packages::freelance_after_create {
    {-trans_task_ids:required}
    {-freelance_package_id:required}
} {
    Callback that is executed after freelance packages are generated for trans_task_ids
} -

ad_proc -public -callback cog::trans::packages::after_deadline_calculation {
    -freelance_package_id:required
    -package_deadline:required
} {
    Callback to ammend the deadline after it was auto calculated
} -

ad_proc -public -callback cog::trans::project::after_update {
    -project_id:required
    -project_status_id:required
    -old_project_status_id:required
    -user_id:required
    {-return_url_var ""}
} {
    After update (primarily status change) callback for translation projects
} - 


ad_proc -public -callback cog::trans::project::after_update -impl "aa_status_changed" {
    -project_id:required
    -project_status_id:required
    -old_project_status_id:required
    -user_id:required
    {-return_url_var "return_url"}
} {
    Set the modification date and handle overall status changes.
} {
    # Set the modification date
    db_dml update_project_object "update acs_objects set last_modified = now(), modifying_user = :user_id where object_id = :project_id"
    if {$old_project_status_id ne $project_status_id} {
        #---------------------------------------------------------------
        # Project Cleanup maintenance
        #---------------------------------------------------------------

        # If the project is closed, we set the end date to now
        if {[lsearch [im_sub_categories [im_project_status_closed]] $project_status_id]>-1} {
            db_dml update_closed_projects_end_date "update im_projects set end_date = now() where project_id = :project_id and end_date is null"
        }
        
        # Update the cost cache in case of a status change
        cog::project::update_cost_cache -project_id $project_id
    }
}

ad_proc -public -callback cog::trans::project::after_update -impl "cog-potential-to-open" {
    -project_id:required
    -project_status_id:required
    -old_project_status_id:required
    -user_id:required
    {-return_url_var "return_url"}
} {
    After run if the status changed to open from potential

    Will send out created assignments and setting them to requested
} {
    if {[im_category_is_a $old_project_status_id [im_project_status_potential]] && [im_category_is_a $project_status_id [im_project_status_open]]} {


        # Set the assignment status to requested
        # set assignment_created_status_id [cog::trans::assignment::status_created]
        # set created_assignment_ids [db_list assignments "select assignment_id from im_freelance_assignments fa, im_freelance_packages fp 
        #     where fp.project_id = :project_id
        #     and fp.freelance_package_id = fa.freelance_package_id
        #     and fa.assignment_status_id = :assignment_created_status_id"]
        
        # foreach assignment_id $created_assignment_ids {
        #     cog_rest::put::translation_assignments -assignment_id $assignment_id -rest_user_id $user_id -assignment_status_id [cog::trans::assignment::status_requested]
        # }

        db_1row project_info "
            select p.processing_time,
                   c.company_id,
                   c.company_status_id
            from im_projects p,
                 im_companies c
            where p.project_id = :project_id
              and c.company_id = p.company_id"

        # Update the processing time
        if {$processing_time eq 0} {
            im_translation_update_project_dates -project_id $project_id
        } else {
            im_translation_update_project_dates -project_id $project_id -customer_processing_days $processing_time
        }

        # Set the company status active if the project is open
        if {[im_company_status_active] != $company_status_id} {
    	    db_dml make_active "update im_companies set company_status_id = [im_company_status_active] where company_id = :company_id"
	    }
    }
}

ad_proc -public -callback cog::trans::project::after_update -impl "cog-open-to-delivered" {
    -project_id:required
    -project_status_id:required
    -old_project_status_id:required
    -user_id:required
    {-return_url_var "return_url"}
} {
    After run if the status changed to delivered from open

    Close all assignments where work was done or we waited for a reaction.
} {
    if {$old_project_status_id eq [im_project_status_open] && $project_status_id eq [im_project_status_delivered]} {
        # set assignment_ids [db_list assignments "select assignment_id from im_freelance_assignments fa, im_freelance_packages fp 
        #     where fp.project_id = :project_id
        #     and fp.freelance_package_id = fa.freelance_package_id
        #     and fa.assignment_status_id in (4220, 4221,4222, 4224, 4225,4226,4229)"]
        
        # foreach assignment_id $assignment_ids {
        #     cog_rest::put::translation_assignments -assignment_id $assignment_id -rest_user_id $user_id -assignment_status_id 4231
        # }
    }
}