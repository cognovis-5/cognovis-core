
ad_library {
    Procedures for supporting with companies 
    @author malte.sussdorff@cognovis.de
}

namespace eval cog::company {
    ad_proc -public key_account_id {
        -user_id:required 
    } {
        Returns the key account for the user

        @param user_id User whos key account we look for

        @return key_account_id
    } {
        # Check if the key accounter is an employee and return the supervisor
        set key_account_id [db_string get_key_account_id "select supervisor_id as key_account_id from im_employees where employee_id =:user_id" -default ""]

        # Check for key accounter for the company
        if {$key_account_id eq ""} {
            set company_id [cog::company::users_company_id -user_id $user_id]
            set key_account_id [company_key_account_id -company_id $company_id]
        }

        # Try with the freelancers contact mail
        if {$key_account_id eq ""} {
            set freelancers_contact_email [parameter::get_from_package_key -package_key "webix-portal" -parameter "GeneralFreelancerContact" -default ""]
            if {$freelancers_contact_email ne ""} {
                set key_account_id [db_string get_key_account_id "select user_id as key_account_id from cc_users where email =:freelancers_contact_email" -default ""]
            }        
        }

        # Hack to display some information if no key account is present.
        if {$key_account_id eq "" } { 
            set key_account_id $user_id 
        }

        return $key_account_id

    }


	ad_proc -public users_company_id {
		-user_id:required
	} {
		Returns the company_id of a user. for employees it is the internal company

		@param user_id UserId for which we look up the company
	} {

		# First we check if user is not primary_contact_id for 
    	set company_id [db_string company_id_sql "select company_id from im_companies where primary_contact_id =:user_id limit 1" -default ""]
    
		if {$company_id eq ""} {
			if {[im_user_is_employee_p $user_id]} {
				set company_id [im_company_internal]
			}
		}
		# If company_id is still null, we keep on searching, this time with acs_rels
    	if {$company_id eq ""} {
        	set company_id [db_string company_id_sql "select object_id_one as company_id from acs_rels where object_id_two =:user_id and rel_type = 'im_company_employee_rel' limit 1" -default ""]
    	}
    	return $company_id
	}


    ad_proc -public company_key_account_id {
        -company_id
    } {

        Return the key account id for given company_id
        This should return single id or empty string
    
        @return key_account_id

    } {
        # First we check if user is not manager_id
        set key_account_id [db_string key_account_id_sql "select manager_id from im_companies where company_id =:company_id" -default ""]
        # If company id is null, we check acs_rels
        if {$key_account_id eq ""} {
            set key_account_id [db_string key_account_id_sql "select object_id_two as key_account_id from acs_rels where object_id_one =:company_id and rel_type = 'im_key_account_rel' limit 1" -default ""]
        }
        
        return $key_account_id
    }

    ad_proc -public new {
        -company_name:required
        {-main_office_id ""}
        {-company_path ""}
        {-company_type_id ""}
        {-company_status_id ""}
        {-creation_user ""}
        { -vat_type_id ""}
        { -vat_number ""}
        { -url ""}
        { -referral_source_id ""}
        { -payment_term_id ""}
        { -default_invoice_template_id ""}
        { -default_quote_template_id ""}    
        { -default_po_template_id ""}
        { -default_bill_template_id ""}
        { -primary_contact_id ""} 
        { -accounting_contact_id ""}
    } {
        
        Create a new company. This is done WITHOUT a callback. You need to cover that in the calling environment
        
        @param company_name Name of the company
        @param main_office_id Main office for the company - Will be created if missing
        @param comapny_path Internal name for the company. Useful if you have multiple companies with the same name. Needs to be unique! If empty, it's derived from company_name
        @param company_type_id category "Intranet Company Type" Type of company (Intranet Company Type). Defaults to "Customer"
        @param company_status_id category "Intranet Company Status" Status of company (Intranet Company Status). Defaults to "Active"
        @param creation_user Who is creating the company. Defaults to current user
        @param vat_type_id category "Intranet VAT Type" VAT Classification of the company. Defaults to "" (Not set)
        @param vat_number company vat number. Defaults to "" (Not set)
        @param url Website for the company. Defaults to "" (Not set)
        @param referral_source_id category "Referral source" Where did this company come from. Defaults to "" (Not set)
        @param payment_term_id category "Intranet Payment Term" Terms for the payment (aka how fast has the payment to be made). Defaults to "" (Not set)
        @param default_invoice_template_id category "Intranet Cost Template" Template we use for new invoices for this customer. Defaults to "" (Not set)
        @param default_quote_template_id category "Intranet Cost Template" Template we use for new quotes for this customer. Defaults to "" (Not set)
        @param default_bill_template_id category "Intranet Cost Template" Template we use for new provider bills. Defaults to "" (Not set)
        @param default_po_template_id category "Intranet Cost Template" Template we use for new purchase orders for this provider. Defaults to "" (Not set)
        @param primary_contact object person Primary contact for the company. Defaults to "" (Not set)
        @param accounting_contact object person Accounting contact for the company. Defaults to "" (Not set)

    } {
        if {$company_name eq ""} {
            cog_log Error "Supplied an empty company_name"
            return ""
        }

	    # -----------------------------------------------------------

        set creation_date [db_string get_sysdate "select sysdate from dual"]
        set creation_ip [ns_conn peeraddr]
        set context_id ""

        if {$company_path eq ""} {
            regsub -all {[^a-zA-Z0-9]} [string trim [string tolower $company_name]] "_" company_path
        }

        if {$creation_user eq ""} {
            set creation_user [auth::get_user_id]
        }

        if {$company_type_id eq ""} {
            set company_type_id [im_company_type_customer]
        }

        if {$company_status_id eq ""} {
            set company_status_id [im_company_status_active]
        }

        if {$main_office_id eq ""} {
            set office_path "${company_path}_main_office"
            set office_name "$company_name [cog::message::lookup -key intranet-core.Main_Office -default "Main Office"]"
            set main_office_id [db_string office_id "select office_id from im_offices where office_name = :office_name or office_path = :office_path" -default ""]
            if {$main_office_id eq ""} {            
                set main_office_id [cog::office::new \
                    -creation_user $creation_user \
                    -office_path $office_path \
                    -office_name $office_name]
            }
        }

    	set company_id [db_exec_plsql create_new_company {
            select im_company__new (
                null,
                'im_company',
                :creation_date,
                :creation_user,
                :creation_ip,
                :context_id,
                :company_name,
                :company_path,
                :main_office_id,
                :company_type_id,
                :company_status_id
            )
        }]
        
        db_dml company_update "update im_companies set vat_number = :vat_number, vat_type_id = :vat_type_id,
            site_concept = :url,  default_referral_source_id = :referral_source_id, payment_term_id = :payment_term_id,
            primary_contact_id = :primary_contact_id, accounting_contact_id = :accounting_contact_id
         where company_id = :company_id"    

        # ---------------------------------------------------------------
        # Add the default templates
        # ---------------------------------------------------------------
        if {$default_invoice_template_id eq ""} {
            set default_invoice_template_id [db_string invoice_template "select category_id from im_categories
                where category_type = 'Intranet Cost Template'
                and aux_int1 = 3700
                and enabled_p = 't'
                order by sort_order asc, category_id desc
                limit 1" -default ""]
        }

        if {$default_bill_template_id eq ""} {
            set default_bill_template_id [db_string invoice_template "select category_id from im_categories
                where category_type = 'Intranet Cost Template'
                and aux_int1 = 3704
                and enabled_p = 't'
                order by sort_order asc, category_id desc
                limit 1" -default ""]
        }

        if {$default_po_template_id eq ""} {
            set default_po_template_id [db_string invoice_template "select category_id from im_categories
                where category_type = 'Intranet Cost Template'
                and aux_int1 = 3706
                and enabled_p = 't'
                order by sort_order asc, category_id desc
                limit 1" -default ""]
        }

        if {$default_quote_template_id eq ""} {
            set default_quote_template_id [db_string invoice_template "select category_id from im_categories
                where category_type = 'Intranet Cost Template'
                and aux_int1 = 3702
                and enabled_p = 't'
                order by sort_order asc, category_id desc
                limit 1" -default ""]
        }

        db_dml update_default_templates "update im_companies
            set default_invoice_template_id = :default_invoice_template_id,
                default_bill_template_id = :default_bill_template_id,
                default_po_template_id = :default_po_template_id,
                default_quote_template_id = :default_quote_template_id
        where company_id = :company_id"
            
        
        set role_id [im_company_role_key_account]
        im_biz_object_add_role $creation_user $company_id $role_id

        if {$primary_contact_id ne ""} {
            set role_id [im_company_role_member]
            im_biz_object_add_role $primary_contact_id $company_id $role_id
        }


        if {$accounting_contact_id ne ""} {
            set role_id [im_company_role_member]
            im_biz_object_add_role $accounting_contact_id $company_id $role_id
        }

        db_dml update_manager "update im_companies set manager_id = :creation_user where company_id = :company_id and manager_id is null"        

    	return $company_id
    }

    ad_proc -public update {
        -company_id:required
        {-company_name ""}
        {-company_path ""}
        {-company_type_id ""}
        {-company_status_id ""}
        -modifying_user:required
        { -vat_type_id ""}
        { -vat_number ""}
        { -url ""}
        { -referral_source_id ""}
        { -payment_term_id ""}
        { -default_invoice_template_id ""}
        { -default_quote_template_id ""}    
        { -default_po_template_id ""}
        { -default_bill_template_id ""}
        { -primary_contact_id ""} 
        { -accounting_contact_id ""}
    } {
        Update company. 
        
        @param company_id ID of the company to update
        @param company_name Name of the company
        @param comapny_path Internal name for the company. Useful if you have multiple companies with the same name. Needs to be unique!
        @param company_type_id category "Intranet Company Type" Type of company (Intranet Company Type)
        @param company_status_id category "Intranet Company Status" Status of company (Intranet Company Status)
        @param modifying_user Who is updating the company
        @param vat_type_id category "Intranet VAT Type" VAT Classification of the company
        @param vat_number company vat number
        @param url Website for the company
        @param referral_source_id category "Referral source" Where did this company come from
        @param payment_term_id category "Intranet Payment Term" Terms for the payment (aka how fast has the payment to be made)
        @param default_invoice_template_id category "Intranet Cost Template" Template we use for new invoices for this customer
        @param default_quote_template_id category "Intranet Cost Template" Template we use for new quotes for this customer
        @param default_bill_template_id category "Intranet Cost Template" Template we use for new provider bills
        @param default_po_template_id category "Intranet Cost Template" Template we use for new purchase orders for this provider
        @param primary_contact object person Primary contact for the company
        @param accounting_contact object person Accounting contact for the company

    } {

        if {$referral_source_id ne ""} {
            set referral_source [im_name_from_id $referral_source_id]
        } else {
            set referral_source ""
        }

        set update_list [list]
        foreach company_var [list company_name company_path accounting_contact_id primary_contact_id company_type_id company_status_id vat_type_id vat_number referral_source payment_term_id default_invoice_template_id default_quote_template_id default_po_template_id default_bill_template_id] {
            if {[set $company_var] ne ""} {
                lappend update_list "$company_var = :$company_var"
            }
        }

        if {$url ne ""} {
            lappend update_list "site_concept = :url"
        }

        if {$primary_contact_id ne ""} {
            set role_id [im_company_role_member]
            im_biz_object_add_role $primary_contact_id $company_id $role_id
        }


        if {$accounting_contact_id ne ""} {
            set role_id [im_company_role_member]
            im_biz_object_add_role $accounting_contact_id $company_id $role_id
        }
        
        if {[llength $update_list]>0} {
            db_dml update_company "update im_companies set [join $update_list ", "] where company_id = :company_id"
            db_dml update_object "update acs_objects set last_modified = now(), modifying_user = :modifying_user where object_id = :company_id"

            cog::callback::invoke -object_type "im_company" -object_id $company_id -action after_update
        }
        
        return $company_id
    }

    ad_proc -public nuke {
        -company_id:required
        -current_user_id:required
    } {
        Nukes the company from the system if there are no dependencies on it (otherwise will fail)

        @param company_id Company to nuke
        @param current_user_id User who is trying to do that
    } {
        set company_exists_p [db_string exists "select count(*) from im_companies where company_id = :company_id"]
        if {!$company_exists_p} { return }

        if {![acs_user::site_wide_admin_p -user_id $current_user_id]} {
            im_company_permissions $current_user_id $company_id view read write admin
            if {!$admin} { return }
        }
        
        set folder_id [cog::file::get_company_folder_id -company_id $company_id]
        if {$folder_id ne ""} {
            content::folder::delete -folder_id $folder_id -cascade_p 1
        }

        cog::fs::nuke -object_id $company_id
        cog::rel::remove -object_id $company_id -current_user_id $current_user_id
        cog::mail::nuke_log -object_id $company_id -current_user_id $current_user_id

        # Delete the offices for this company
        set companies_offices_sql "
            select	office_id
            from	im_offices o,
                acs_rels r
            where	r.object_id_one = o.office_id
                and r.object_id_one = :company_id
            UNION
            select	office_id
            from	im_offices o
            where	company_id = :company_id
        "

        db_foreach delete_offices $companies_offices_sql {
            db_dml unlink_offices "update im_companies set main_office_id = (select min(office_id) from im_offices) where main_office_id = :office_id"
            cog::office::nuke -current_user_id $current_user_id -office_id $office_id
        }

        im_exec_dml del_company_$company_id "im_company__delete($company_id)"
    }

    ad_proc -public info_component {
        -company_id:required
        -return_url:required
    } {
        Old company info component due to lack of intranet-core access
    } {
        set params [list [list base_url "cognovis-core"] [list company_id $company_id] [list return_url $return_url]]
        set result [ad_parse_template -params $params "/packages/cognovis-core/lib/company-info"]
        return [string trim $result]
    }

    ad_proc -public path {
        -company_id:required 
    } {
        Returns the path for a company

        @param company_id Company we need the path for

        @return UNIX path for the company
    } {
        set path_proc [parameter::get_from_package_key -package_key "cognovis-core" -parameter "CompanyPathProc"]
        if {$path_proc eq "" || $path_proc eq "im_filestorage_company_path"} {
            return [util_memoize [list im_filestorage_company_path_helper $company_id]]
        }
        return [$path_proc -company_id $company_id]
    }

    ad_proc -public filestorage_final_company_path {
        -company_id:required
        -final_company_id:required
    } {
        Returns the filestorage based path for companies
    } {
        return [util_memoize [list im_filestorage_company_path_helper $final_company_id]]
    }


    ad_proc -public final_company_path {
        -company_id:required 
        -final_company_id:required
    } {
        Returns the path for a final company
    
        @param company_id Company we need the final company for
        @param final_company_id Final company we want to create within the company

        @return UNIX path for the final company
    } {
        set path_proc [parameter::get_from_package_key -package_key "cognovis-core" -parameter "FinalCompanyPathProc"]
        return [$path_proc -company_id $company_id -final_company_id $final_company_id]
    }

    ad_proc -public provider_p {
        -company_id:required
    } {
        Returns 1 if the company is a provider

        @param company_id Company which we need to know whether it is a provider

        @return 1 if the company is a provider 
    } {
        set provider_type_ids [im_sub_categories [im_company_type_provider]]
        set provider_p [db_string provider_p "select 1 from im_companies where company_id = :company_id and company_type_id in ([ns_dbquotelist $provider_type_ids])" -default 0]
        return $provider_p
    }

}

namespace eval cog::office {
    ad_proc -public new {
        -creation_user:required
        {-company_id ""}
        {-office_name ""}
        {-office_path ""}
        { -office_type_id "" }
        { -office_status_id "" }
        { -address_country_code ""}
        { -address_line1 ""}
        { -address_line2 ""}
        { -address_city ""}
        { -address_state ""}
        { -address_postal_code ""}
        { -phone ""}
    } {
        create a new office for a company

        @param office_name Name of the office, derived from the company and office_type
        @param office_type_id Type of the office, defaults to Main office
        @param office_status_id Status - defaults to active
        @param creation_user Who is creating the office
        @param company_id For which company are we adding the office
        @param address_country_code string Country code of company main_office_id
        @param address_line1 string Street (address_line_1) of company main office
        @param address_line2 string Second address line (e.g. PO box or appartment)
        @param address_city string city of company main office
        @param address_state string state of company main office
        @param address_postal_code string zip code / postal code of company main office
        @param phone string phone number of the main office

    } {
        if {$office_type_id eq ""} {
            set office_type_id [im_office_type_main]
        }
        if {$office_status_id eq ""} {
            set office_status_id [im_office_status_active]
        }

        if {$company_id ne ""} {
            db_1row company_info "select company_name, company_path from im_companies where company_id = :company_id"
            if {$office_name eq "" } {
                set office_name "$company_name [im_name_from_id $office_type_id] Office"
            }

            if {$office_path eq ""} {
                set office_type [db_string category "select category from im_categories where category_id = :office_type_id"]
                regsub -all {[^a-zA-Z0-9]} [string trim [string tolower $office_type]] "_" office_type
                set office_path "${company_path}_$office_type"
            }
        }

        set creation_date [db_string get_sysdate "select sysdate from dual"]
        set creation_ip [ns_conn peeraddr]
        set context_id "$company_id"

        set office_id [db_exec_plsql create_new_office {
            select im_office__new (
                null::integer,
                'im_office'::varchar,
                :creation_date::timestamptz,
                :creation_user::integer,
                :creation_ip::varchar,
                :context_id::integer,
                :office_name::varchar,
                :office_path::varchar,
                :office_type_id::integer,
                :office_status_id::integer,
                :company_id::integer
            )
        }]

        db_dml office_update "update im_offices set address_line1 = :address_line1, address_line2 = :address_line2, address_city=:address_city, address_postal_code = :address_postal_code, address_state = :address_state, address_country_code = :address_country_code, phone = :phone where office_id = :office_id"

        return $office_id
    }

    ad_proc -public update {
        -modifying_user:required
        {-office_id ""}
        {-office_name ""}
        {-office_path ""}
        { -office_type_id ""}
        { -office_status_id "" }
        { -address_country_code ""}
        { -address_line1 ""}
        { -address_line2 ""}
        { -address_city ""}
        { -address_postal_code ""}
        { -phone ""}
    } {
        create a new office for a company

        @param office_name Name of the office, derived from the company and office_type
        @param office_type_id Type of the office, defaults to Main office
        @param office_status_id Status - defaults to active
        @param modifying_user Who is updating the office
        @param company_id For which company are we adding the office
        @param address_country_code string Country code of company main_office_id
        @param address_line1 string Street (address_line_1) of company main office
        @param address_line2 string Second address line (e.g. PO box or appartment)
        @param address_city string city of company main office
        @param address_postal_code string zip code / postal code of company main office
        @param phone string phone number of the main office

    } {
        # db_1row office_info "select office_name,office_path,office_type_id,office_status_id,address_country_code,address_line1,address_line2,address_city,address_postal_code,phone from im_offices where office_id = :office_id" -column_arry old_office

        set update_list [list]
        foreach office_var [list office_name office_path office_type_id office_status_id address_country_code address_line1 address_line2 address_city address_postal_code phone] {
            if {[set $office_var] ne ""} {
                lappend update_list "$office_var = :$office_var"
            }
        }

        if {[llength $update_list]>0} {
            db_dml update_office "update im_offices set [join $update_list ", "] where office_id = :office_id"
            db_dml update_object "update acs_objects set last_modified = now(), modifying_user = :modifying_user where object_id = :office_id"

            cog::callback::invoke -object_type "im_office" -object_id $office_id -action after_update
        }
        return $office_id
    }

     ad_proc -public nuke {
        -office_id:required
        -current_user_id:required
    } {
        Nukes the office from the system if there are no dependencies on it (otherwise will fail)

        @param office_id Office to nuke
        @param current_user_id User who is trying to do that
    } {
        set office_exists_p [db_string exists "select count(*) from im_offices where office_id = :office_id"]
        if {!$office_exists_p} { return }

        if {![acs_user::site_wide_admin_p -user_id $current_user_id]} {
            im_office_permissions $current_user_id $office_id view read write admin
            if {!$admin} { return }
        }
        set company_p [db_string company_p "select count(*) from im_companies where main_office_id = :office_id"]
        if {$company_p} { return }

        set invoice_p [db_string invoice "select count(*) from im_invoices where invoice_office_id = :office_id"]
        if {$invoice_p} { return }

        cog::fs::nuke -object_id $office_id

        cog::rel::remove -object_id $office_id -current_user_id $current_user_id
        cog::mail::nuke_log -object_id $office_id -current_user_id $current_user_id

        im_exec_dml del_office_$office_id "im_office__delete($office_id)"

    }

}

namespace eval cog::company_contact {
    ad_proc -public new {
        -company_id:required
        -first_names:required
        -last_name:required
        -email:required
        { -locale "" }
        { -salutation_id "" }
        { -home_phone "" }
        { -cell_phone "" }
        { -work_phone "" }
        { -position ""}
        { -password ""}
        { -screen_name ""}
        {-creation_user_id ""}
    } {
        Create a contact for a company

        @param company_id object im_company::write Company for which we want to add a contact
        @param first_names string First names of the contact
        @param last_name string Last name of the contact
        @param email string Email of the contact
        @param locale string Locale of the contact
        @param salutation_id string Salutation of the contact
        @param home_phone string Home phone of the contact
        @param cell_phone string Cell phone of the contact
        @param work_phone string Work phone of the contact
        @param position string Position of the contact
        @param password string Password of the contact
        @param screen_name string Screenname of the contact
        @return contact_id integer ID of the created contact
    } {
        
        if {$creation_user_id eq ""} {
            set creation_user_id [auth::get_user_id]
        }

        set contact_id [db_string first_last_name_exists_p "
            select  user_id
            from    cc_users
            where   lower(trim(email)) = lower(trim(:email))
            " -default ""]

        if {$contact_id eq ""} {

            if {$password ne ""} {
                set password_confirm $password
            } else {
                set password_confirm ""
            }

            #---------------------------------------------------------------
            # Generate Screenname
            #---------------------------------------------------------------
            if {$screen_name eq ""} {
                set screen_name "$first_names $last_name"
                set screen_name_exists_p [db_string check_screen_name "select 1 from cc_users where screen_name =:screen_name" -default 0]
                if {$screen_name_exists_p} {
                    set screen_name_second "$first_names $last_name ([im_name_from_id $company_id])"
                    set screen_name_exists_p [db_string check_screen_name "select 1 from cc_users where screen_name =:screen_name_second" -default 0]
                    if {$screen_name_exists_p} {
                        set count_same_screen_names [db_string count_screen_names "select count(*) from cc_users where screen_name =:screen_name" -default 0]
                        incr count_same_screen_names
                        set screen_name_third "$first_names $last_name $count_same_screen_names"
                        set screen_name $screen_name_third
                    } else {
                        set screen_name $screen_name_second
                    }
                }
            }

            # Create the user
            array set creation_info [auth::create_user  -screen_name $screen_name -username $email  -email $email  -first_names $first_names  -last_name $last_name  -password $password  -password_confirm $password_confirm ]
        
            # Extract the contact_id from the creation info
            if {[info exists creation_info(user_id)]} {
                set contact_id $creation_info(user_id)
                set role_id [im_biz_object_role_full_member]
            } else {
                cog_log Error "Could not create contact: [lindex $creation_info(element_messages) 0]"
                return ""
            }
        
            # Update creation user to allow the creator to admin the user
            db_dml update_creation_user_id "
                update acs_objects
                set creation_user = :creation_user_id
                where object_id = :contact_id
            "

            # Add the user to the "Registered Users" group, because
            # (s)he would get strange problems otherwise
            set registered_users [db_string registered_users "select object_id from acs_magic_objects where name='registered_users'"]
            set reg_users_rel_exists_p [db_string member_of_reg_users "
                    select  count(*)
                    from    group_member_map m, membership_rels mr
                    where   m.member_id = :contact_id
                        and m.group_id = :registered_users
                        and m.rel_id = mr.rel_id
                        and m.container_id = m.group_id
                        and m.rel_type::text = 'membership_rel'::text
            "]
            if {!$reg_users_rel_exists_p} {
                relation_add -member_state "approved" "membership_rel" $registered_users $contact_id
            }

            set company_type_id [db_string company_info "select company_type_id from im_companies where company_id = :company_id" -default [im_company_type_customer]]
            
            set profile [im_profile_customers]
            if {$company_type_id == [im_company_type_customer]} {set profile [im_profile_customers]}
            if {$company_type_id == [im_company_type_partner]} {set profile [im_profile_partners]}
            if {$company_type_id == [im_company_type_provider]} {set profile [im_profile_freelancers]}
            if {$company_type_id == [im_company_type_internal]} {set profile [im_profile_employees]}

            # Check whether the creation_user_id has the right to add the guy to the group:
            set managable_profiles [im_profile::profile_options_managable_for_user $creation_user_id]
            foreach profile_tuple $managable_profiles {
                set profile_name [lindex $profile_tuple 0]
                set profile_id [lindex $profile_tuple 1]
                if {$profile == $profile_id} { im_profile::add_member -profile_id $profile -user_id $contact_id }
            }

            db_dml add_users_contact "insert into users_contact (user_id) values (:contact_id)"
            db_dml update_contact "update users_contact set cell_phone = :cell_phone, work_phone = :work_phone, home_phone = :home_phone where user_id = :contact_id"
            db_dml update_person "update persons set position = :position, salutation_id = :salutation_id where person_id = :contact_id"
            if {$locale ne ""} {
                lang::user::set_locale -user_id $contact_id $locale
            }
        } 
        db_dml add_primary_contact "update im_companies set primary_contact_id = :contact_id where company_id = :company_id"
        set role_id [im_biz_object_role_full_member]
        im_biz_object_add_role $contact_id $company_id $role_id      

        return $contact_id
    }

    # Update contact
    # 

    ad_proc -public update {
        -contact_id:required
        -first_names:required
        -last_name:required
        -email:required
        -company_id:required
        {-cell_phone ""}
        {-work_phone ""}
        {-home_phone ""}
        {-position ""}
        {-salutation_id ""}
    } {
        Update contact
        @param contact_id ID of the contact
        @param first_names First names of the contact
        @param last_name Last name of the contact
        @param email Email of the contact
        @param salutation_id Salutation of the contact
        @param home_phone Home phone of the contact
        @param cell_phone Cell phone of the contact
        @param work_phone Work phone of the contact
        @param position Position of the contact

    } {

        party::update  -party_id $contact_id  -email $email

        # Make sure the "person" exists.
        # This may be not the case when creating a user from a party.
        set person_exists_p [db_string person_exists "select count(*) from persons where person_id = :contact_id"]
        if {!$person_exists_p} {
            db_dml insert_person "
                insert into persons (
                    person_id, first_names, last_name
                ) values (
                    :contact_id, :first_names, :last_name
                )
            "
            # Convert the party into a person
            db_dml person2party "
                update acs_objects
                set object_type = 'person'
                where object_id = :contact_id
            "
        }

        person::update  -person_id $contact_id  -first_names $first_names  -last_name $last_name
        db_dml update_person "update persons set position = :position, salutation_id = :salutation_id where person_id = :contact_id"

        set user_exists_p [db_string user_exists "select count(*) from users where user_id = :contact_id"]
        if {!$user_exists_p} {
            if {"" == $username} { set username $email}
            db_dml insert_user "
                insert into users (
                user_id, username
                ) values (
                :contact_id, :username
                )
            "
            # Convert the person into a user
            db_dml party2user "
                update acs_objects
                set object_type = 'user'
                where object_id = :contact_id
            "
        }

        acs_user::update  -user_id $contact_id -username $email
        
        if {![db_string exists_p "select 1 from users_contact where user_id = :contact_id" -default 0]} {
           db_dml add_users_contact "insert into users_contact (user_id) values (:contact_id)"
        }

        db_dml update_contact "update users_contact set cell_phone = :cell_phone, work_phone = :work_phone, home_phone = :home_phone where user_id = :contact_id"

        db_dml add_primary_contact "update im_companies set primary_contact_id = :contact_id where company_id = :company_id"
        set role_id [im_biz_object_role_full_member]
        im_biz_object_add_role $contact_id $company_id $role_id      

        return $contact_id
    }
}
