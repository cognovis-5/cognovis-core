#
# Copyright (C) 2007 ]project-open[
#
# This program is free software. You can redistribute it
# and/or modify it under the terms of the GNU General
# Public License as published by the Free Software Foundation;
# either version 2 of the License, or (at your option)
# any later version. This program is distributed in the
# hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

ad_library {
    Stubs for object callbacks.

    @author frank.bergmann@project-open.com
    @author malte.sussdorff@cognovis.de
}

namespace eval cog::callback {

    ad_proc -public invoke {
        {-object_id "" }
        {-object_type "" }
        {-status_id "" }
        {-type_id "" }
        {-action "after_update" }
        {-impl *}
        {callback ""}
        {args ""}
    } {
        Invoke the registered callback implementations for the given
        callback.  The callbacks terminate on error unless -catch
        is provided.  The value returned by the callback function is
        determined by the return codes from the callback implementations.
        <p>
        The callbacks are executed one level below the calling function
        so passing arrays to a callback can be done normally via
        <pre>upvar arrayname $arrayref</pre>
        <p>
        The return codes returned from the implmentation are treated
        as follows:
        <dl>
        <dt>return -code ok or "<b>return</b>"</dt>
        <dd>With a plain return, a non-empty return value will be lappended to
        the list of returns from the callback function</dd>

        <dt>return -code error or "<b>error</b>"</dt>
        <dd>errors will simply propigate (and no value returned) unless -catch
        is specified in which case the callback processing will continue but
        no value will be appended to the return list for the implementation
        which returned an error.
        </dd>

        <dt>return -code return</dt>
        <dd>Takes the return value if the implementation returning -code return
        and returns a one element list with that return value.  Note that this means
        if you have code which returns <code>return -code return {x y}</code>,
        you will get {{x y}} as the return value from the callback.  This is
        done in order to unambiguously distinguish a pair of callbacks returning
        x and y respectively from this single callback.
        </dd>

        <dt>return -code break</dt>
        <dd>return the current list of returned values including this implementations
        return value if non-empty</dd>

        <dt>return -code continue</dt>
        <dd>Continue processing, ignore the return value from this implementation</dd>

        </dl>

        @param callback the callback name without leading or trailing ::

        @param impl invoke a specific implemenation rather than all implementations
            of the given callback

        @param args pass the set of arguments on to each callback

        @param object_id The object that may have changed
        @param object_type We can save one SQL statement if the calling side already knows the type of the object
        @param action One of {before|after} + '_' + {create|update|delete} or {view}:
            Create represent object creation.
            Update is the default.
            Delete refers to a "soft" delete, marking the object as deleted
            Nuke represents complete object deletion - should only be used for demo data.
            Before_update represents checks before the update of important objects im_costs,
            im_project. This way the system can detect changes from outside the system.
        
        @return list of the returns from each callback that does a normal (non-empty) return

        @see callback
    
    } {

        if {$callback eq ""} {
            if {"" == $object_type || "" == $status_id || "" == $type_id} {

                set ref_status_id ""
                set ref_type_id ""
                db_0or1row audit_object_info "
                    select	o.object_type,
                        im_biz_object__get_status_id(o.object_id) as ref_status_id,
                        im_biz_object__get_type_id(o.object_id) as ref_type_id
                    from	acs_objects o
                    where	o.object_id = :object_id
                "

                if {"" == $status_id && "" != $ref_status_id} { set status_id $ref_status_id }
                if {"" == $type_id && "" != $ref_type_id} { set type_id $ref_type_id }
            }
            set callback "${object_type}_${action}"
            set args "-object_id $object_id -status_id $status_id -type_id $type_id"
        }

       	set fo [open "[acs_root_dir]/log/callback.execution.log" a]
        set now [clock format [clock seconds] -format "%Y-%m-%d %H:%M:%S"]
        set proc_name_up [lindex [info level -1] 0]

        puts $fo "$now [im_name_from_id [auth::get_user_id]] - $proc_name_up :: $callback - $args"
        close $fo

        # see that the contract exists and call the contract for
        # arg validation -- ::callback::${callback}::contract is an
        # empty function that only runs the ad_proc generated arg parser.

        if {[info commands ::callback::${callback}::contract] eq ""} {
            return ""
        }
        
        if {[catch {::callback::${callback}::contract {*}$args}]} {
            cog_log Error "callback '::callback::${callback}::contract $args' does not match contract"
            return ""
        }

        set returns {}

        set base ::callback::${callback}::impl
        foreach procname [lsort [info commands ${base}::$impl]] {
            set c [catch {::uplevel 1 [::list $procname {*}$args]} ret]
            switch -exact $c {
                0 { # code ok
                    if { $ret ne "" } {
                        lappend returns $ret
    					set fo [open "[acs_root_dir]/log/callback.execution.log" a]
                        puts $fo "** [lindex [split $procname "::"] end] $args => $ret"					
                        close $fo
                    }
                }
                1 { # code error - either rethrow the current error or log
                    cog_log Error "callback $callback error invoking $procname: $ret\n[ad_print_stack_trace]\n"
                }
                2 { # code return -- end processing and return what we got back.
                    return [list $ret]
                }
                3 { # code break -- terminate return current list of results.
                    if { $ret ne "" } {
                        lappend returns $ret
                    }
                    return $returns
                }
                4 { # code continue -- just skip this one
                }
                default {
                    cog_log Error "Callback return code unknown: $c when invoking $procname"
                }
            }
        }

        if {$impl ne "*" && ![info exists c] && !$catch_p} {
            cog_log Error "callback $callback implementation $impl does not exist"
        }
        return $returns
    }
}