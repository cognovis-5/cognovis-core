#!/bin/zsh

# Pfad zum Unterordner "upgrade"
upgrade_dir="./upgrade"

# Zieldatei, in die die Inhalte geschrieben werden sollen
target_file="cognovis-create.sql"

# Temporäre Datei zum Speichern der sortierten Dateinamen
temp_file=$(mktemp)

# Alle SQL-Dateien im Unterordner "upgrade" finden und nach Versionsnummern sortieren
ls -1 "$upgrade_dir"/upgrade-*.sql | sort -t- -k2,2 -V > "$temp_file"

# Inhalte der sortierten Dateien in die Zieldatei schreiben
while read -r file; do
  echo "-- Inhalt von $file:" >> "$target_file"
  cat "$file" >> "$target_file"
  echo "" >> "$target_file"  # Leerzeile hinzufügen
done < "$temp_file"

# Temporäre Datei löschen
rm "$temp_file"

echo "Die Inhalte der SQL-Dateien wurden erfolgreich in $target_file zusammengeführt."