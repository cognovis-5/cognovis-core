SELECT acs_log__debug('/packages/cognovis-core/sql/postgresql/upgrade/upgrade-5.0.2.6.7-5.0.2.6.8.sql','');

alter table im_payments  drop constraint if exists im_payments_cost;
alter table im_payments add constraint im_payments_cost foreign key (cost_id) references im_costs on delete cascade;

CREATE OR REPLACE FUNCTION inline_0 ()
RETURNS INTEGER AS
$$
declare
	v_count integer;
begin
	select count(*) into v_count
	from         user_tab_columns
where lower(table_name) = 'im_object_freelance_skill_map';
	IF 0 = v_count THEN return 0; END IF;
                alter table im_object_freelance_skill_map  drop constraint if exists im_o_skills_user_fk;
                alter table im_object_freelance_skill_map add constraint im_o_skills_user_fk foreign key (object_id) references acs_objects on delete cascade;
	return 1;
end;
$$ LANGUAGE 'plpgsql';
	
SELECT inline_0 ();
DROP FUNCTION inline_0 ();

CREATE OR REPLACE FUNCTION inline_0 ()
RETURNS INTEGER AS
$$
declare
	v_count integer;
begin
	select count(*) into v_count
	from         user_tab_columns
where lower(table_name) = 'im_freelancer_quality';
	IF 0 = v_count THEN return 0; END IF;
                alter table im_freelancer_quality  drop constraint if exists im_trans_fl_quality_project_fk;
                alter table im_freelancer_quality add constraint im_trans_fl_quality_project_fk foreign key (project_id) references im_projects on delete cascade;
	return 1;
end;
$$ LANGUAGE 'plpgsql';
	
SELECT inline_0 ();
DROP FUNCTION inline_0 ();

alter table im_payments  drop constraint if exists im_payments_provider;
alter table im_payments add constraint im_payments_provider foreign key (provider_id) references acs_objects on delete cascade;

alter table images  drop constraint if exists images_image_id_fk;
alter table images add constraint images_image_id_fk foreign key (image_id) references cr_revisions on delete cascade;

-- Delete a single cost (if we know its ID...)
create or replace function im_cost__delete (integer)
returns integer as $$
DECLARE
        p_cost_id alias for $1;
        v_rel_val record;
begin
        -- Update im_hours relationship
        update  im_hours
        set     cost_id = null
        where   cost_id = p_cost_id;

        -- Erase the im_cost
        delete from im_costs
        where cost_id = p_cost_id;

        -- Erase the im_cost
        delete from im_biz_objects
        where object_id = p_cost_id;

        -- Erase the acs_rels entries pointing to this cost item
        FOR v_rel_val in
            select  rel_id
            from    acs_rels
            where   object_id_two = p_cost_id or object_id_one = p_cost_id
        LOOP
            PERFORM acs_rel__delete(v_rel_val.rel_id);
        end loop;
        
        delete from acs_permissions
        where object_id = p_cost_id;

        -- Erase the object
        PERFORM acs_object__delete(p_cost_id);
        return 0;
end;$$ language 'plpgsql';

create or replace function acs_mail_log__delete (integer)
returns integer as $$
DECLARE
        p_log_id alias for $1;
        
begin
        delete from acs_mail_log where log_id = p_log_id;
        -- Erase the object
        PERFORM acs_object__delete(p_log_id);
        return 0;
end;$$ language 'plpgsql';