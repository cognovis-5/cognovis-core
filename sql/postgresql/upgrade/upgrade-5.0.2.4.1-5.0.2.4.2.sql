--
--
-- Copyright (c) 2011, cognovís GmbH, Hamburg, Germany
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-- @author Malte Sussdorff (malte.sussdorff@cognovis.de)
-- @creation-date 2020-11-09
-- @cvs-id $Id$
--

SELECT acs_log__debug('/packages/cognovis-core/sql/postgresql/upgrade/upgrade-5.0.2.4.1-5.0.2.4.2.sql','');

-- Add symbols
update currency_codes set symbol = '€' where iso = 'EUR';
update currency_codes set symbol = '$' where iso = 'USD';

-- Remove unique constraint on im_companies name
create or replace function inline_0 ()
returns integer as $body$
declare
        v_count                 integer;
begin
    SELECT count(*) into v_count
       FROM pg_catalog.pg_constraint con
            INNER JOIN pg_catalog.pg_class rel
                       ON rel.oid = con.conrelid
            INNER JOIN pg_catalog.pg_namespace nsp
                       ON nsp.oid = connamespace
       WHERE rel.relname = 'im_companies' and con.conname = 'im_companies_name_un';
    if v_count = 1 then
        alter table im_companies  drop constraint if exists im_companies_name_un; 
    end if;
    return 0;
end;$body$ language 'plpgsql';
select inline_0 ();
drop function inline_0 ();    
