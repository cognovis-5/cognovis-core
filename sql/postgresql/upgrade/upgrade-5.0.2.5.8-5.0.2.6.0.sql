SELECT acs_log__debug('/packages/cognovis-core/sql/postgresql/upgrade/upgrade-5.0.2.5.8-5.0.2.6.0.sql','');

select acs_object_type__create_type (
	'im_price',		-- object_type
	'Price',		-- pretty_name
	'Prices',		-- pretty_plural
	'',			-- supertype
	'im_prices',		-- table_name
	'price_id',			-- id_column
	'',		-- package_name
	'f',				-- abstract_p
	null,				-- type_extension_table
	'im_price__name'	-- name_method
);

CREATE OR REPLACE FUNCTION inline_0 ()
RETURNS INTEGER AS
$$
declare
	v_count integer;
begin
	select count(*) into v_count
	from         user_tab_columns
where lower(table_name) = 'im_prices';
	IF 0 != v_count THEN drop table im_prices; END IF;

    create table im_prices (
	    price_id integer
			constraint im_prices_pk primary key
			references acs_objects on delete cascade,
        price_type_id integer
            constraint im_prices_type_fk references im_categories,
        price_status_id integer
            constraint im_prices_status_fk references im_categories,
        material_id integer
            constraint im_prices_material_fk references im_materials,
        company_id		integer not null 
            constraint im_prices_company_fk references im_companies,
        complexity_type_id integer not null
            constraint im_prices_complexiity references im_categories
            default 4290,
        valid_from		date,
	    valid_through	date,
            -- make sure the end date is after start date
            constraint im_prices_date_const
            check(valid_through >= valid_from),
    	currency char(3) references currency_codes(ISO)
            constraint im_prices_currency_nn not null,
    	price numeric(12,4)
			constraint im_prices_price_nn not null,
	    min_price numeric(12,4),
	    note text
    );

	return 1;
end;
$$ LANGUAGE 'plpgsql';

SELECT inline_0();
DROP FUNCTION inline_0();

update acs_object_types set
	status_type_table = 'im_prices',
	status_column = 'price_status_id',
	type_column = 'price_type_id'
where object_type = 'im_price';

select im_category_new(3900, 'Active', 'Intranet Price Status');
select im_category_new(3902, 'Inactive', 'Intranet Price Status');

select im_category_new(4290, 'Default', 'Intranet Price Complexity');
select im_category_new(4291, 'Premium', 'Intranet Price Complexity');

update im_categories set category_type = 'Intranet Price Complexity' where category_type = 'Intranet Translation Complexity';

select im_category_new(3910, 'Default', 'Intranet Price Type');
select im_category_new(3911, 'Timesheet', 'Intranet Price Type');
select im_category_new(3912, 'Translation', 'Intranet Price Type');

-- Table to store the history of prices paid in projects.
CREATE OR REPLACE FUNCTION inline_0 ()
RETURNS INTEGER AS
$$
declare
	v_count integer;
begin
	select count(*) into v_count
	from         user_tab_columns
where lower(table_name) = 'im_price_history';
	IF 0 != v_count THEN return 0; END IF;
	create table im_price_history (
		project_id	integer not null 
					constraint im_price_history_project_nn
					references im_projects,
        material_id integer not null
            constraint im_price_history_material_nn references im_materials,
		company_id	integer not null
					constraint im_price_history_company_nn
					references im_companies,
		currency	char(3) references currency_codes(ISO)
					constraint im_price_history_currency_nn
					not null,
		price		numeric(12,4)
					constraint im_price_history_price_nn
					not null,
        complexity_type_id integer not null
            constraint im_prices_complexiity_nn references im_categories,
		creation_date		timestamptz
	);

	-- make sure the same price doesn't get defined twice
	create unique index im_price_history_idx on im_price_history (
		project_id, material_id, company_id, currency, complexity_type_id
	);
	
	return 1;
end;
$$ LANGUAGE 'plpgsql';
	
SELECT inline_0 ();
DROP FUNCTION inline_0 ();

CREATE OR REPLACE FUNCTION im_price__new(
	p_price_type_id integer,
	p_price_status_id integer,
	p_material_id integer,
	p_company_id integer,
	p_complexity_type_id integer,
	p_valid_from date,
	p_valid_through date,
	p_currency varchar,
	p_price numeric,
	p_min_price numeric,
	p_note text,
   	p_creation_user integer,     -- default null
   	p_creation_ip varchar      -- default null

) RETURNS integer AS $$
DECLARE
    v_price_id im_prices.price_id%TYPE;
BEGIN
        v_price_id := acs_object__new (
                null,              -- object_id
                'im_price',          -- object_type
                now(),        -- creation_date
                p_creation_user,        -- creation_user
                p_creation_ip,          -- creation_ip
                p_company_id            -- context_id
        );

        insert into im_prices (price_id,price_type_id,price_status_id,material_id,company_id,complexity_type_id,valid_through,valid_from,currency,price,min_price,note)
        values (v_price_id,p_price_type_id,p_price_status_id,p_material_id,p_company_id,p_complexity_type_id,p_valid_through,p_valid_from,p_currency,p_price,p_min_price,p_note);

        return v_price_id;
END;
$$ LANGUAGE plpgsql;

delete from im_timesheet_prices where uom_id in (324,325);

CREATE OR REPLACE FUNCTION inline_0 ()
RETURNS INTEGER AS
$$
declare
	v_count integer;
begin
	select count(*) into v_count
	from         user_tab_columns
where lower(table_name) = 'im_materials' and lower(column_name) = 'source_language_id';
	IF 0 = v_count THEN return 0; END IF;
		delete from im_timesheet_prices where material_id in (select material_id from im_materials where source_language_id is not null or target_language_id is not null);
	return 1;
end;
$$ LANGUAGE 'plpgsql';
	
SELECT inline_0 ();
DROP FUNCTION inline_0 ();
