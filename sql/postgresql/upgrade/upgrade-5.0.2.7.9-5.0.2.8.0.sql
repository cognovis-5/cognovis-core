SELECT acs_log__debug('/packages/cognovis-core/sql/postgresql/upgrade/upgrade-5.0.2.7.9-5.0.2.8.0.sql','');

select acs_object_type__create_type (
	'im_report_column',		-- object_type
	'Report Column',		-- pretty_name
	'Report Columns',		-- pretty_plural
	'',			-- supertype
	'im_report_columns',		-- table_name
	'column_id',			-- id_column
	'',		-- package_name
	'f',				-- abstract_p
	null,				-- type_extension_table
	'im_report_column__name'	-- name_method
);

CREATE OR REPLACE FUNCTION im_report_column__name(p_column_id integer) 
RETURNS text AS $$
DECLARE
    v_column_name text;
BEGIN
    
    select column_name into v_column_name from im_report_columns where column_id = p_column_id;
    return v_column_name;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION inline_0 ()
RETURNS INTEGER AS
$$
declare
	v_count integer;
begin
	select count(*) into v_count
	from         user_tab_columns
where lower(table_name) = 'im_report_columns';
	IF 0 != v_count THEN drop table im_report_columns; END IF;

    create table im_report_columns (
	    column_id integer
			constraint im_report_columns_pk primary key
			references acs_objects on delete cascade,
        report_id integer
			constraint im_report_column_report_fk 
			references im_reports on delete cascade,    
        column_type_id integer
            constraint im_report_columns_type_fk references im_categories,
        column_status_id integer
            constraint im_report_columns_status_fk references im_categories,
        column_name text,
        sort_order integer,
        variable_name text,
	    column_description text
    );

	return 1;
end;
$$ LANGUAGE 'plpgsql';

SELECT inline_0();
DROP FUNCTION inline_0();

update acs_object_types set
	status_type_table = 'im_report_columns',
	status_column = 'column_status_id',
	type_column = 'column_type_id'
where object_type = 'im_report_column';

select im_category_new(15050, 'Active', 'Intranet Report Column Status');
select im_category_new(15051, 'Inactive', 'Intranet Report Column Status');

select im_category_new(15150, 'NamedID', 'Intranet Report Column Type');
select im_category_new(15151, 'String', 'Intranet Report Column Type');
select im_category_new(15152, 'Category', 'Intranet Report Column Type');
select im_category_new(15153, 'Date', 'Intranet Report Column Type');
select im_category_new(15154, 'Price', 'Intranet Report Column Type');
select im_category_new(15155, 'Numeric', 'Intranet Report Column Type');

update im_categories set aux_string1 = 'im_name_from_id' where category_id = 15150;
update im_categories set aux_string1 = 'im_category_from_id' where category_id = 15152;
update im_categories set aux_string1 = 'im_numeric_from_id' where category_id = 15155;


select im_category_new(15160, 'NamedIDArray', 'Intranet Report Column Type');
select im_category_new(15161, 'StringArray', 'Intranet Report Column Type');
select im_category_new(15162, 'CategoryArray', 'Intranet Report Column Type');

update im_categories set aux_string1 = 'im_name_from_id' where category_id = 15160;
update im_categories set aux_string1 = 'im_category_from_id' where category_id = 15162;

CREATE OR REPLACE FUNCTION im_report_column__new(
	p_report_id integer,
    p_column_name text,
	p_column_status_id integer,
    p_column_type_id integer,
    p_sort_order integer,
    p_variable_name text,
    p_column_description text,
   	p_creation_user integer,     -- default null
   	p_creation_ip varchar      -- default null

) RETURNS integer AS $$
DECLARE
    v_column_id integer;
BEGIN
        v_column_id := acs_object__new (
                null,              -- object_id
                'im_report_column',          -- object_type
                now(),        -- creation_date
                p_creation_user,        -- creation_user
                p_creation_ip,          -- creation_ip
                p_report_id            -- context_id
        );

        insert into im_report_columns (column_id,column_name,report_id,column_status_id,column_type_id,sort_order,variable_name,column_description)
        values (v_column_id,p_column_name,p_report_id,p_column_status_id,p_column_type_id,p_sort_order,p_variable_name,p_column_description);

        return v_column_id;
END;
$$ LANGUAGE plpgsql;
