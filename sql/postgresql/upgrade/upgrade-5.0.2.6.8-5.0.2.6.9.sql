SELECT acs_log__debug('/packages/cognovis-core/sql/postgresql/upgrade/upgrade-5.0.2.6.8-5.0.2.6.9.sql','');

-- Delete an object_type
create or replace function cog_object_type__delete (varchar)
returns integer as $$
DECLARE
        p_object_type alias for $1;
        v_rest_object_type record;
begin

        -- Erase the acs_rels entries pointing to this cost item
        FOR v_rest_object_type in
            select  object_type_id
            from    im_rest_object_types
            where   object_type = p_object_type
        LOOP
            PERFORM im_rest_object_type__delete(v_rest_object_type.object_type_id);
        end loop;
        
        delete from acs_object_type_tables where object_type = p_object_type;

        -- Erase the object
        PERFORM acs_object_type__drop_type (p_object_type,'1','1','1');
        return 0;
end;$$ language 'plpgsql';

create or replace function im_rest_object_type__delete (integer)
returns integer as $$
DECLARE
        p_object_type_id alias for $1;
begin

        delete from im_rest_object_types where object_type_id = p_object_type_id;

        -- Erase the object
        PERFORM acs_object__delete(p_object_type_id);
        return 0;
end;$$ language 'plpgsql';

create or replace function cog_rel_type__delete (varchar)
returns integer as $$
DECLARE
        p_rel_type alias for $1;
        v_rest_object_type record;
begin

        -- Erase the acs_rels entries pointing to this cost item
        FOR v_rest_object_type in
            select  object_type_id
            from    im_rest_object_types
            where   object_type = p_rel_type
        LOOP
            PERFORM im_rest_object_type__delete(v_rest_object_type.object_type_id);
        end loop;
        
        delete from acs_object_type_tables where object_type = p_rel_type;

        -- Erase the object
        PERFORM acs_rel_type__drop_type (p_rel_type,'1');
        return 0;
end;$$ language 'plpgsql';

create or replace function im_fs_file__delete (integer)
returns integer as $$
DECLARE
        p_fs_file_id alias for $1;
begin

        delete from im_fs_files where file_id = p_fs_file_id;

        -- Erase the object
        PERFORM acs_object__delete(p_fs_file_id);
        return 0;
end;$$ language 'plpgsql';

delete from user_preferences where user_id not in (select user_id from users);
alter table user_preferences add constraint user_pref_user_id_fk foreign key (user_id) references users on delete cascade;

alter table lang_messages  drop constraint if exists lang_messages_creation_u_fk;
alter table lang_messages add constraint lang_messages_creation_u_fk foreign key (creation_user) references users on delete set null;

alter table lang_messages_audit  drop constraint if exists lang_messages_audit_ou_fk;
alter table lang_messages_audit add constraint lang_messages_audit_ou_fk foreign key (overwrite_user) references users on delete set null;

alter table im_cost_centers  drop constraint if exists im_cost_centers_manager_fk;
alter table im_cost_centers add constraint im_cost_centers_manager_fk foreign key (manager_id) references users on delete set null;

delete from im_forum_topic_user_map where user_id not in (select user_id from users);
alter table im_forum_topic_user_map add constraint im_forum_topics_um_user_fk foreign key (user_id) references users on delete cascade;

alter table im_forum_topics  drop constraint if exists im_forum_topics_asignee_fk;
alter table im_forum_topics add constraint im_forum_topics_asignee_fk foreign key (asignee_id) references users on delete set null;

alter table im_user_absences  drop constraint if exists im_user_absences_vacation_replacement_fk;
alter table im_user_absences add constraint im_user_absences_vacation_replacement_fk foreign key (vacation_replacement_id) references parties on delete set null;

alter table im_companies  drop constraint if exists im_companies_manager_fk;
alter table im_companies add constraint im_companies_manager_fk foreign key (manager_id) references users on delete set null;

alter table im_projects add constraint im_project_company_contact_fk foreign key (company_contact_id) references users on delete set null;

CREATE OR REPLACE FUNCTION inline_0 ()
RETURNS INTEGER AS
$$
declare
	v_count integer;
begin
	select count(*) into v_count
	from         user_tab_columns
where lower(table_name) = 'im_projects' and lower(column_name) = 'acceptance_user_id';
	IF 0 = v_count THEN return 0; END IF;
                alter table im_projects  drop constraint if exists im_projects_acceptance_user_id_fkey
                alter table im_projects add constraint im_projects_acceptance_user_id_fkey foreign key (acceptance_user_id) references users on delete set null;
	return 1;
end;
$$ LANGUAGE 'plpgsql';
	
SELECT inline_0 ();
DROP FUNCTION inline_0 ();

alter table im_projects  drop constraint if exists im_projects_prj_lead_fk;
alter table im_projects add constraint im_projects_prj_lead_fk foreign key (project_lead_id) references users on delete set null;

alter table im_projects  drop constraint if exists im_projects_supervisor_fk;
alter table im_projects add constraint im_projects_supervisor_fk foreign key (supervisor_id) references users on delete set null;

alter table im_offices  drop constraint if exists im_offices_cont_per_fk;
alter table im_offices add constraint im_offices_cont_per_fk foreign key (contact_person_id) references users on delete set null;

CREATE OR REPLACE FUNCTION inline_0 ()
RETURNS INTEGER AS
$$
declare
	v_count integer;
begin
	select count(*) into v_count
	from         user_tab_columns
where lower(table_name) = 'im_freelance_skills' and lower(column_name) = 'confirmation_user_id';
	IF 0 = v_count THEN return 0; END IF;
                alter table im_freelance_skills  drop constraint if exists im_fl_skills_conf_user_fk;
                alter table im_freelance_skills add constraint im_fl_skills_conf_user_fk foreign key (confirmation_user_id) references users on delete set null;
	return 1;
end;
$$ LANGUAGE 'plpgsql';
	
SELECT inline_0 ();
DROP FUNCTION inline_0 ();
alter table im_conf_items  drop constraint if exists im_conf_items_owner_fk;
alter table im_conf_items add constraint im_conf_items_owner_fk foreign key (conf_item_owner_id) references persons on delete set null;

delete from party_approved_member_map where party_id not in (select party_id from parties);
alter table party_approved_member_map add constraint party_member_party_fk foreign key (party_id) references parties on delete cascade;

delete from im_employees where employee_id not in (select user_id from users);
alter table im_employees add constraint im_employees_employee_fk foreign key (employee_id) references users on delete cascade;

alter table im_employees  drop constraint if exists im_employees_supervisor_fk;
alter table im_employees add constraint im_employees_supervisor_fk foreign key (supervisor_id) references parties on delete set null;

delete from users where user_id not in (select party_id from parties);
alter table users add constraint users_user_fk foreign key (user_id) references parties on delete cascade;

alter table survsimp_responses  drop constraint if exists survsimp_responses_related_context_id_fkey;
alter table survsimp_responses add constraint survsimp_responses_related_context_id_fkey foreign key (related_context_id) references acs_objects on delete set null;

alter table survsimp_responses  drop constraint if exists survsimp_responses_related_object_id_fkey;
alter table survsimp_responses add constraint survsimp_responses_related_object_id_fkey foreign key (related_object_id) references acs_objects on delete set null;

CREATE OR REPLACE FUNCTION inline_0 ()
RETURNS INTEGER AS
$$
declare
	v_count integer;
begin
	select count(*) into v_count
	from         user_tab_columns
where lower(table_name) = 'im_trans_tasks';
	IF 0 = v_count THEN return 0; END IF;
                drop trigger im_trans_tasks_calendar_update_tr on im_trans_tasks;
	return 1;
end;
$$ LANGUAGE 'plpgsql';
	

drop trigger im_projects_calendar_update_tr on im_projects;
drop trigger im_forum_topics_calendar_update_tr on im_forum_topics;

delete from party_approved_member_map where member_id not in (select object_id from acs_objects);
alter table party_approved_member_map add constraint party_member_member_fk foreign key (member_id) references acs_objects on delete cascade;

alter table im_companies  drop constraint if exists im_cc_bill_cancellation_template_fk;
alter table im_companies add constraint im_cc_bill_cancellation_template_fk foreign key (default_bill_cancellation_template_id) references im_categories on delete set null;

alter table im_companies  drop constraint if exists im_cc_bill_correction_template_fk;
alter table im_companies add constraint im_cc_bill_correction_template_fk foreign key (default_bill_correction_template_id) references im_categories on delete set null;

alter table im_companies  drop constraint if exists im_cc_correction_template_fk;
alter table im_companies add constraint im_cc_correction_template_fk foreign key (default_correction_template_id) references im_categories on delete set null;

alter table im_companies  drop constraint if exists im_cc_invoice_cancellation_template_fk;
alter table im_companies add constraint im_cc_invoice_cancellation_template_fk foreign key (default_invoice_cancellation_template_id) references im_categories on delete set null;

alter table im_companies  drop constraint if exists im_companies_def_invoice_template_fk;
alter table im_companies add constraint im_companies_def_invoice_template_fk foreign key (default_invoice_template_id) references im_categories on delete set null;

alter table im_companies  drop constraint if exists im_companies_default_quote_fk;
alter table im_companies add constraint im_companies_default_quote_fk foreign key (default_quote_template_id) references im_categories on delete set null;

SELECT im_category_new ('9501', 'Projectmanagement', 'Intranet Gantt Task Type');
SELECT im_category_new ('9502', 'Software Development', 'Intranet Gantt Task Type');
update im_categories set category = 'Created' where category_id = 9600;
SELECT im_category_new ('9603', 'Active', 'Intranet Gantt Task Status');
update im_timesheet_tasks set task_status_id = 9600 where task_id in (select project_id from im_projects where project_type_id = 100 and project_status_id = 76);
update im_timesheet_tasks set task_status_id = 9601 where task_id in (select project_id from im_projects where project_type_id = 100 and project_status_id in (79,81,77,82));
update im_timesheet_tasks set task_status_id = 9603 where task_status_id = 9600 and task_id in (select project_id from im_hours);
update im_categories set aux_html2='fas fa-gear' where category_id = 9500;
update im_categories set aux_html2='fas fa-project-diagram' where category_id = 9501;
update im_categories set aux_html2='fas fa-code' where category_id = 9502;
update im_categories set aux_html2='#0080FF' where category_id = 9601;
update im_categories set aux_html2='#008000' where category_id = 9603;
update im_categories set aux_html2='#FFA500' where category_id = 9600;

alter table im_invoice_items  drop constraint if exists im_invoice_items_un;
alter table im_invoice_items add constraint im_invoice_items_un unique (item_name, invoice_id, project_id, sort_order, item_uom_id, item_material_id);
ALTER TABLE im_projects ALTER COLUMN company_project_nr TYPE text;
ALTER TABLE im_projects ALTER COLUMN project_risk TYPE text;

CREATE OR REPLACE FUNCTION inline_0 ()
RETURNS INTEGER AS
$$
declare
	v_count integer;
begin
	select count(*) into v_count
	from         user_tab_columns
where lower(table_name) = 'im_projects' and lower(column_name) = 'trans_size';
	IF 0 = v_count THEN return 0; END IF;
                ALTER TABLE im_projects ALTER COLUMN trans_size TYPE text;
	return 1;
end;
$$ LANGUAGE 'plpgsql';
	
SELECT inline_0 ();
DROP FUNCTION inline_0 ();

update im_component_plugins set component_tcl = 'cog::company::info_component -company_id $company_id -return_url $return_url' where component_tcl = 'im_company_info_component $company_id $return_url'