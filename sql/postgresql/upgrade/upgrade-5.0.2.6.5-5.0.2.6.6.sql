SELECT acs_log__debug('/packages/cognovis-core/sql/postgresql/upgrade/upgrade-5.0.2.6.5-5.0.2.6.6.sql','');
alter table im_price_history  drop constraint if exists im_price_history_company_nn;
alter table im_price_history add constraint im_price_history_company_nn foreign key (company_id) references im_companies on delete cascade;
alter table im_prices  drop constraint if exists im_prices_company_fk;
alter table im_prices add constraint im_prices_company_fk foreign key (company_id) references im_companies on delete cascade;
alter table im_notes  drop constraint if exists im_object_id_fk;
alter table im_notes add constraint im_notes_object_fk foreign key (object_id) references acs_objects on delete cascade;
alter table im_biz_object_members  drop constraint if exists im_biz_object_members_rel_fk;
alter table im_biz_object_members add constraint im_biz_object_members_rel_fk foreign key (rel_id) references acs_rels on delete cascade;
