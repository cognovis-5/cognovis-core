SELECT acs_log__debug('/packages/cognovis-core/sql/postgresql/upgrade/upgrade-5.0.2.6.0-5.0.2.6.1.sql','');

SELECT im_report_new (
	'Price List Export',
	'price_list_export',
	'cognovis-core',
	40,
	(select menu_id from im_menus where label = 'reporting-finance'),
	'
    select
		im_category_from_id(m.material_uom_id) as uom,
		c.company_path,
		im_category_from_id(m.task_type_id) as task_type,
		im_category_from_id(m.target_language_id) as target_language,
		im_category_from_id(m.source_language_id) as source_language,
		im_category_from_id(m.subject_area_id) as subject_area,
		im_category_from_id(m.file_type_id) as file_type,
		im_category_from_id(p.complexity_type_id) as complexity_type,
		p.valid_from,
		p.min_price,
		p.valid_through,
		replace(to_char(p.price, ''99999.99''), ''.'', '','') as price,
		p.currency
	from	im_prices p, im_materials m,
		im_companies c
	where	p.company_id = c.company_id
        and p.material_id = m.material_id
		and p.company_id = %company_id%
        and p.price_status_id = 3900
	order by
		im_category_from_id(m.material_uom_id),
		c.company_path,
		im_category_from_id(m.task_type_id),
		im_category_from_id(m.target_language_id),
		im_category_from_id(m.source_language_id),
		im_category_from_id(m.subject_area_id),
	    im_category_from_id(m.file_type_id)
'
);