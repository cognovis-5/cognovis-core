SELECT acs_log__debug('/packages/cognovis-core/sql/postgresql/upgrade/upgrade-5.0.2.6.6-5.0.2.6.7.sql','');
alter table user_portraits drop constraint if exists user_portraits_user_id_fk;
alter table user_portraits add constraint user_portraits_user_id_fk foreign key (user_id) references users on delete cascade;
alter table users_email drop constraint if exists users_email_pk_fk;
alter table users_email add constraint users_email_pk_fk foreign key (user_id) references users on delete cascade;
alter table users_office365 drop constraint if exists users_office365_pk_fk;
alter table users_office365 add constraint users_office365_pk_fk foreign key (user_id) references users on delete cascade;

alter table cr_item_rels drop constraint if exists cr_item_rels_rel_obj__fk;
alter table cr_item_rels add constraint cr_item_rels_rel_obj__fk foreign key (related_object_id) references acs_objects on delete cascade;

alter table parties drop constraint if exists parties_party_id_fk;
alter table parties add constraint parties_party_id_fk foreign key (party_id) references acs_objects on delete cascade;

alter table group_element_index drop constraint if exists group_element_index_rel_id_fk;
alter table group_element_index add constraint group_element_index_rel_id_fk foreign key (rel_id) references acs_rels on delete cascade;

alter table membership_rels drop constraint if exists membership_rel_rel_id_fk;
alter table membership_rels add constraint membership_rel_rel_id_fk foreign key (rel_id) references acs_rels on delete cascade;

alter table workflow_case_log_rev drop constraint if exists workflow_case_log_rev_entry_rev_id_fkey;
alter table workflow_case_log_rev add constraint workflow_case_log_rev_entry_rev_id_fkey foreign key (entry_rev_id) references cr_revisions on delete cascade;

CREATE OR REPLACE FUNCTION inline_0 ()
RETURNS INTEGER AS
$$
declare
	v_count integer;
begin
	select count(*) into v_count
	from         user_tab_columns
where lower(table_name) = 'im_freelance_assignments';
	IF 0 = v_count THEN return 0; END IF;
        alter table im_freelance_assignments drop constraint if exists im_freelance_assignment_id_fk;
        alter table im_freelance_assignments add constraint im_freelance_assignment_id_fk foreign key (assignment_id) references acs_objects on delete cascade;
	return 1;
end;
$$ LANGUAGE 'plpgsql';
	
SELECT inline_0 ();
DROP FUNCTION inline_0 ();

CREATE OR REPLACE FUNCTION inline_0 ()
RETURNS INTEGER AS
$$
declare
	v_count integer;
begin
	select count(*) into v_count
	from         user_tab_columns
where lower(table_name) = 'package_folder_ext';
	IF 0 = v_count THEN return 0; END IF;
        alter table package_folder_ext drop constraint if exists package_folder_ext_rel_id_fk;
        alter table package_folder_ext add constraint package_folder_ext_rel_id_fk foreign key (rel_id) references acs_rels on delete cascade;
		alter table project_folder_ext drop constraint if exists project_folder_ext_rel_id_fk;
		alter table project_folder_ext add constraint project_folder_ext_rel_id_fk foreign key (rel_id) references acs_rels on delete cascade;
	return 1;
end;
$$ LANGUAGE 'plpgsql';
	
SELECT inline_0 ();
DROP FUNCTION inline_0 ();

CREATE OR REPLACE FUNCTION inline_0 ()
RETURNS INTEGER AS
$$
declare
	v_count integer;
begin
	select count(*) into v_count
	from         user_tab_columns
where lower(table_name) = 'webix_notifications';
	IF 0 = v_count THEN return 0; END IF;
        alter table webix_notifications drop constraint if exists webix_notif_user_fk;
        alter table webix_notifications add constraint webix_notif_user_fk foreign key (recipient_id) references users on delete cascade;
	return 1;
end;
$$ LANGUAGE 'plpgsql';
	
SELECT inline_0 ();
DROP FUNCTION inline_0 ();

alter table notifications drop constraint if exists notif_user_id_fk;
alter table notifications add constraint notif_user_id_fk foreign key (notif_user) references users on delete cascade;

CREATE OR REPLACE FUNCTION inline_0 ()
RETURNS INTEGER AS
$$
declare
	v_count integer;
begin
	select count(*) into v_count
	from         user_tab_columns
where lower(table_name) = 'im_user_leave_entitlements';
	IF 0 = v_count THEN return 0; END IF;
        alter table im_user_leave_entitlements drop constraint if exists im_user_leave_entitlements_user_fk;
        alter table im_user_leave_entitlements add constraint im_user_leave_entitlements_user_fk foreign key (owner_id) references users on delete cascade;
	return 1;
end;
$$ LANGUAGE 'plpgsql';
	
SELECT inline_0 ();
DROP FUNCTION inline_0 ();


CREATE OR REPLACE FUNCTION inline_0 ()
RETURNS INTEGER AS
$$
declare
	v_count integer;
begin
	select count(*) into v_count
	from         user_tab_columns
where lower(table_name) = 'im_trans_tasks';
	IF 0 = v_count THEN return 0; END IF;
        alter table im_trans_tasks drop constraint if exists im_trans_tasks_cert_id_fkey;
        alter table im_trans_tasks add constraint im_trans_tasks_cert_id_fkey foreign key (cert_id) references users on delete set null;
        alter table im_trans_tasks drop constraint if exists im_trans_tasks_edit_fk;
        alter table im_trans_tasks add constraint im_trans_tasks_edit_fk foreign key (edit_id) references users on delete set null;
        alter table im_trans_tasks drop constraint if exists im_trans_tasks_other_fk;
        alter table im_trans_tasks add constraint im_trans_tasks_other_fk foreign key (other_id) references users on delete set null;
        alter table im_trans_tasks drop constraint if exists im_trans_tasks_proof_fk;
        alter table im_trans_tasks add constraint im_trans_tasks_proof_fk foreign key (proof_id) references users on delete set null;
        begin
            perform 1 from information_schema.columns where table_name = 'im_trans_tasks' and column_name = 'training_id';
        exception when undefined_table or undefined_column then
            null;
        end;
        if found then
            alter table im_trans_tasks drop constraint if exists im_trans_tasks_training_id_fkey;
            alter table im_trans_tasks add constraint im_trans_tasks_training_id_fkey foreign key (training_id) references users on delete set null;
        end if;
		alter table im_trans_tasks drop constraint if exists im_trans_tasks_trans_fk;
        alter table im_trans_tasks add constraint im_trans_tasks_trans_fk foreign key (trans_id) references users on delete set null;
	return 1;
end;
$$ LANGUAGE 'plpgsql';
	
SELECT inline_0 ();
DROP FUNCTION inline_0 ();


CREATE OR REPLACE FUNCTION inline_0 ()
RETURNS INTEGER AS
$$
declare
	v_count integer;
begin
	select count(*) into v_count
	from         user_tab_columns
where lower(table_name) = 'im_freelancer_quality';
	IF 0 = v_count THEN return 0; END IF;
        alter table im_freelancer_quality drop constraint if exists im_trans_fl_quality_user_fk;
        alter table im_freelancer_quality add constraint im_trans_fl_quality_user_fk foreign key (user_id) references users on delete cascade;
	return 1;
end;
$$ LANGUAGE 'plpgsql';
	
SELECT inline_0 ();
DROP FUNCTION inline_0 ();

CREATE OR REPLACE FUNCTION inline_0 ()
RETURNS INTEGER AS
$$
declare
	v_count integer;
begin
	select count(*) into v_count
	from         user_tab_columns
where lower(table_name) = 'im_freelance_notifications';
	IF 0 = v_count THEN return 0; END IF;
        alter table im_freelance_notifications drop constraint if exists im_freelance_notif_user_fk;
        alter table im_freelance_notifications add constraint im_freelance_notif_user_fk foreign key (user_id) references users on delete cascade;
	return 1;
end;
$$ LANGUAGE 'plpgsql';
	
SELECT inline_0 ();
DROP FUNCTION inline_0 ();

