SELECT acs_log__debug('/packages/cognovis-core/sql/postgresql/upgrade/upgrade-5.0.2.6.2-5.0.2.6.3.sql','');

CREATE OR REPLACE FUNCTION inline_0 ()
RETURNS integer as '
DECLARE
	v_count			integer;
BEGIN
	select count(*) into v_count from acs_object_types where object_type = ''im_company_logo_rel'';
	IF v_count = 0 THEN 

	PERFORM acs_object_type__create_type(
		''im_company_logo_rel'',
		''#cognovis-core.company_logo_rel#'',
		''#cognovis-core.company_logo_rel#'',
		''relationship'',
		''im_company_logo_rels'',
		''company_logo_rel_id'',
		''#cognovis-core.company_logo#'', 
		''f'',
		null,
		NULL
	);
	END IF;

    create table im_company_logo_rels (
		company_logo_rel_id	integer
			REFERENCES acs_rels
            ON DELETE CASCADE
		CONSTRAINT im_company_logo_rel_id_pk PRIMARY KEY
	);

	select count(*) into v_count from acs_rel_types where rel_type = ''im_company_logo_rel'';
	IF v_count = 0 THEN 

	insert into acs_rel_types (
		rel_type, object_type_one, role_one,
		min_n_rels_one, max_n_rels_one,
		object_type_two, role_two,min_n_rels_two, max_n_rels_two
	) values (
		''im_company_logo_rel'', ''im_company'', ''company'',
		''1'', NULL,
		''content_item'', NULL, ''1'', NULL
	);
    end if;

	RETURN 0;
end;' language 'plpgsql';
select inline_0();
drop function inline_0();