SELECT acs_log__debug('/packages/cognovis-core/sql/postgresql/upgrade/upgrade-5.0.2.6.3-5.0.2.6.4.sql','');
update im_dynfield_widgets set parameters = '{custom {category_type "Intranet Price Complexity" include_empty_p 0}}' where widget_name = 'category_complexity_type';
update im_materials set material_billable_p = 'f';
update im_materials set material_billable_p = 't' where material_id in (select distinct material_id from im_prices);
update im_materials set material_billable_p = 't' where material_billable_p = 'f' and material_id in (select distinct item_material_id from im_invoice_items);

CREATE OR REPLACE FUNCTION inline_0 ()
RETURNS INTEGER AS
$$
declare
	v_count integer;
begin
	select count(*) into v_count
	from         user_tab_columns
where lower(table_name) = 'im_materials' and lower(column_name) = 'source_language_id';
	IF 0 = v_count THEN return 0; END IF;
        update im_materials set material_type_id = 9014 where source_language_id is not null and target_language_id is not null and material_billable_p = 't';
	return 1;
end;
$$ LANGUAGE 'plpgsql';
	
SELECT inline_0 ();
DROP FUNCTION inline_0 ();

CREATE OR REPLACE FUNCTION inline_0 ()
RETURNS INTEGER AS
$$
declare
	v_count integer;
begin
	select count(*) into v_count
	from         user_tab_columns
where lower(table_name) = 'im_freelance_assignments';
	IF 0 = v_count THEN return 0; END IF;
        update im_materials set material_type_id = 9014 where material_id in (select distinct material_id from im_freelance_assignments ) and material_type_id = 9016;
	return 1;
end;
$$ LANGUAGE 'plpgsql';
	
SELECT inline_0 ();
DROP FUNCTION inline_0 ();

delete from im_materials where material_type_id = 9016 and material_billable_p = 'f';
update im_invoice_items set item_material_id = (select material_id from im_materials where material_nr = 'discount') where (lower(item_name) like '%discount%' or lower(item_name) like '%rabatt%') and item_material_id is null;
update im_invoice_items set item_material_id = (select material_id from im_materials where material_nr = 'surcharge') where (lower(item_name) like '%aufschlag%' or lower(item_name) like '%eilauftrag%' or lower(item_name) like '%surcharge%' or lower(item_name) like '%zuschlag%') and item_material_id is null;
update im_materials set material_status_id = 9102;
update im_materials set material_status_id = 9100 where material_id in (select material_id from im_prices);
update im_materials set material_status_id = 9100 where material_id in (select material_id from im_price_history);
update im_materials set material_status_id = 9100 where material_id in (select material_id from im_hours);
update im_materials set material_status_id = 9100 where material_id in (select material_id from im_timesheet_tasks);
update im_materials set material_status_id = 9100 where material_id in (select item_material_id from im_invoice_items);
