SELECT acs_log__debug('/packages/cognovis-core/sql/postgresql/upgrade/upgrade-5.0.2.8.9-5.0.2.9.0.sql','');

-- Add acceptance date
DO $$
BEGIN
    IF EXISTS (
        SELECT 1
        FROM information_schema.columns 
        WHERE table_name='im_projects' AND column_name='acceptance_date'
    ) THEN
        RAISE NOTICE 'Column "acceptance_date" already exists.';
    ELSE
        ALTER TABLE im_projects
        ADD COLUMN acceptance_date timestamptz;
    END IF;
END $$;

SELECT im_dynfield_attribute_new ('im_project', 'acceptance_date', 'Acceptance Date', 'jq_timestamp', 'timestamp', 't', 25, 't');

UPDATE im_dynfield_layout 
SET pos_y = '25', sort_key = '25' 
WHERE attribute_id = (
    SELECT attribute_id 
    FROM im_dynfield_attributes
    WHERE acs_attribute_id = (
        SELECT attribute_id 
        FROM acs_attributes 
        WHERE attribute_name = 'acceptance_date'
    )
);

-- Uncomment the following line if insert is required
-- INSERT INTO im_dynfield_layout (pos_y, sort_key, page_url, attribute_id) 
-- VALUES ('25', '25', '/intranet/projects/index', 
-- (SELECT attribute_id FROM im_dynfield_attributes WHERE acs_attribute_id = (SELECT attribute_id FROM acs_attributes WHERE attribute_name = 'acceptance_date')));

UPDATE im_dynfield_attributes 
SET also_hard_coded_p = 't' 
WHERE acs_attribute_id = (
    SELECT attribute_id 
    FROM acs_attributes 
    WHERE attribute_name = 'acceptance_date'
);

UPDATE im_dynfield_type_attribute_map 
SET display_mode = 'display' 
WHERE attribute_id = (
    SELECT attribute_id 
    FROM im_dynfield_attributes 
    WHERE acs_attribute_id = (
        SELECT attribute_id 
        FROM acs_attributes 
        WHERE attribute_name = 'acceptance_date'
    )
);

-- Add acceptance user
DO $$
BEGIN
    IF EXISTS (
        SELECT 1 
        FROM information_schema.columns 
        WHERE table_name='im_projects' AND column_name='acceptance_user_id'
    ) THEN
        RAISE NOTICE 'Column "acceptance_user_id" already exists.';
    ELSE
        ALTER TABLE im_projects
        ADD COLUMN acceptance_user_id INTEGER REFERENCES users;
    END IF;
END $$;

SELECT im_dynfield_attribute_new ('im_project', 'acceptance_user_id', 'Acceptance User', 'customer_contact', 'integer', 't', 26, 't');

UPDATE im_dynfield_layout 
SET pos_y = '26', sort_key = '26' 
WHERE attribute_id = (
    SELECT attribute_id 
    FROM im_dynfield_attributes
    WHERE acs_attribute_id = (
        SELECT attribute_id 
        FROM acs_attributes 
        WHERE attribute_name = 'acceptance_user_id'
    )
);

-- Uncomment the following line if insert is required
-- INSERT INTO im_dynfield_layout (pos_y, sort_key, page_url, attribute_id) 
-- VALUES ('26', '26', '/intranet/projects/index', 
-- (SELECT attribute_id FROM im_dynfield_attributes WHERE acs_attribute_id = (SELECT attribute_id FROM acs_attributes WHERE attribute_name = 'acceptance_user_id')));

UPDATE im_dynfield_attributes 
SET also_hard_coded_p = 't' 
WHERE acs_attribute_id = (
    SELECT attribute_id 
    FROM acs_attributes 
    WHERE attribute_name = 'acceptance_user_id'
);

UPDATE im_dynfield_type_attribute_map 
SET display_mode = 'display' 
WHERE attribute_id = (
    SELECT attribute_id 
    FROM im_dynfield_attributes 
    WHERE acs_attribute_id = (
        SELECT attribute_id 
        FROM acs_attributes 
        WHERE attribute_name = 'acceptance_user_id'
    )
);

-- change to cognovis handling of categories
update im_menus set url = '/cognovis/categories' where url = '/intranet/admin/categories/';