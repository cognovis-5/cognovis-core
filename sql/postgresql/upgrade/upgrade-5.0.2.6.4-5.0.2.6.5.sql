SELECT acs_log__debug('/packages/cognovis-core/sql/postgresql/upgrade/upgrade-5.0.2.6.4-5.0.2.6.5.sql','');

select im_category_new(331, 'MinPrice', 'Intranet UoM');
update im_categories set sort_order = 331 where category_id = 331;
update im_categories set sort_order = 330 where category_id = 330;
update im_materials set material_uom_id = 322 where material_uom_id is null;
alter table im_materials alter column material_uom_id set not null;