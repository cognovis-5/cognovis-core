SELECT acs_log__debug('/packages/cognovis-core/sql/postgresql/upgrade/upgrade-5.0.2.8.7-5.0.2.8.8.sql','');

-- Backwards compatability as acs-kernel did some significant changes and dropped the table
CREATE OR REPLACE VIEW acs_privilege_descendant_map AS
WITH RECURSIVE privilege_desc(parent, child) AS (
   SELECT child_privilege as parent, child_privilege as child FROM acs_privilege_hierarchy
UNION ALL
   SELECT privilege as parent, privilege as child FROM
   (SELECT privilege FROM acs_privilege_hierarchy
    EXCEPT
    SELECT child_privilege FROM acs_privilege_hierarchy) identity
UNION ALL
   SELECT h.privilege as parent, pd.child
   FROM acs_privilege_hierarchy h, privilege_desc pd
   WHERE pd.parent = h.child_privilege
) SELECT privilege_desc.parent, privilege_desc.child, privilege_desc.parent as privilege,  privilege_desc.child as descendant FROM privilege_desc;


-- Backwards compatability as acs-kernel did some significant changes and dropped the table
CREATE OR REPLACE FUNCTION im_search_update (
    p_object_id integer,
    p_object_type varchar,
    p_biz_object_id integer,
    p_text varchar
)
RETURNS integer AS $$
BEGIN
    RETURN 0; -- Funktion tut nichts mehr
END;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION im_trans_tasks_calendar_update_tr()
RETURNS trigger AS $$
DECLARE
    v_cal_item_id           integer;
    v_timespan_id           integer;
    v_interval_id           integer;
    v_calendar_id           integer;
    v_activity_id           integer;
    v_recurrence_id         integer;
    v_name                  varchar;
    v_project_name          varchar;
    v_project_nr            varchar;
BEGIN
    -- -------------- Skip if start or end date are null ------------
    IF new.end_date IS NULL THEN
        RETURN new;
    END IF;

    -- -------------- Check if the entry already exists ------------
    v_cal_item_id := NULL;
    
    SELECT event_id
    INTO v_cal_item_id
    FROM acs_events
    WHERE related_object_id = new.task_id
        AND related_object_type = 'im_trans_task';

    -- --------------------- Create entry if it isn't there -------------
    IF v_cal_item_id IS NULL THEN
        v_timespan_id := timespan__new(new.end_date, new.end_date);
        v_activity_id := acs_activity__new(
            null,
            new.task_name,
            ''::varchar,
            'f'::boolean,
            ''::varchar,
            'acs_activity'::varchar, 
            now(), 
            null::integer, 
            '0.0.0.0'::varchar, 
            null::integer
        );

        SELECT min(calendar_id)
        INTO v_calendar_id
        FROM calendars
        WHERE private_p = 'f';

        v_recurrence_id := NULL;
        v_cal_item_id := cal_item__new(
            null::integer,                  -- cal_item_id
            v_calendar_id,                  -- on_which_calendar
            new.task_name,                  -- name
            ''::varchar,                    -- description
            false::boolean,                 -- html_p
            ''::varchar,                    -- status_summary
            v_timespan_id,                  -- timespan_id
            v_activity_id,                  -- activity_id
            v_recurrence_id,                -- recurrence_id
            'cal_item'::varchar,            -- object_type
            null::integer,                  -- context_id
            now(),                          -- creation_date
            null::integer,                  -- creation_user
            '0.0.0.0'::varchar,             -- creation_ip
            null::integer                   -- package_id
        );
    END IF;

    -- --------------------- Update the entry --------------------
    SELECT activity_id INTO v_activity_id FROM acs_events WHERE event_id = v_cal_item_id;
    SELECT timespan_id INTO v_timespan_id FROM acs_events WHERE event_id = v_cal_item_id;
    SELECT recurrence_id INTO v_recurrence_id FROM acs_events WHERE event_id = v_cal_item_id;
    
    SELECT project_name INTO v_project_name FROM im_projects WHERE project_id = new.project_id;
    SELECT project_nr INTO v_project_nr FROM im_projects WHERE project_id = new.project_id;
    
    v_name := new.task_name || ' @ ' || v_project_nr || ' - ' || v_project_name;

    -- Update the event
    UPDATE acs_events
    SET name = v_name,
        description = '',
        related_object_id = new.task_id,
        related_object_type = 'im_trans_task',
        related_link_url = '/intranet-translation/trans-tasks/task-list?project_id='||new.project_id,
        related_link_text = v_name,
        redirect_to_rel_link_p = 't'
    WHERE event_id = v_cal_item_id;

    -- Update the activity - same as event
    UPDATE acs_activities
    SET name = v_name,
        description = ''
    WHERE activity_id = v_activity_id;

    -- Update the timespan. Make sure there is only one interval
    -- in this timespan (there may be multiples)
    SELECT interval_id INTO v_interval_id FROM timespans WHERE timespan_id = v_timespan_id;
    
    RAISE NOTICE 'cal_update_tr: cal_item:%, activity:%, timespan:%, recurrence:%, interval:%',
        v_cal_item_id, v_activity_id, v_timespan_id, v_recurrence_id, v_interval_id;

    UPDATE time_intervals
    SET start_date = new.end_date,
        end_date = new.end_date
    WHERE interval_id = v_interval_id;
    
    RETURN new;
END;
$$ LANGUAGE plpgsql;

DROP FUNCTION if exists public.to_tsvector(oid, text);
DROP FUNCTION if exists public.to_tsquery(oid, text);

update im_dynfield_attributes set also_hard_coded_p = 't' where widget_name in ('customer_contact','customers','parent_projects');
update im_dynfield_attributes set also_hard_coded_p = 't' where acs_attribute_id in (select attribute_id from acs_attributes where attribute_name in ('start_date','end_date'));