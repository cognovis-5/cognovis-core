SELECT acs_log__debug('/packages/cognovis-core/sql/postgresql/upgrade/upgrade-5.0.2.7.0-5.0.2.7.1.sql','');

alter table im_costs  drop constraint if exists im_costs_cause_fk;
alter table im_costs add constraint im_costs_cause_fk foreign key (cause_object_id) references acs_objects on delete set null;

alter table im_costs  drop constraint if exists im_cost_template_fk;
alter table im_costs add constraint im_cost_template_fk foreign key (template_id) references im_categories on delete set null;

alter table im_fs_folder_status  drop constraint if exists im_fs_folder_status_user_fk;
alter table im_fs_folder_status add constraint im_fs_folder_status_user_fk foreign key (user_id) references users on delete cascade;

alter table im_fs_folders  drop constraint if exists im_fs_folder_object_fk;
alter table im_fs_folders add constraint im_fs_folder_object_fk foreign key (object_id) references acs_objects on delete set null;